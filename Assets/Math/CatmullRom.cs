﻿using System;
using System.Collections.Generic;

namespace SGMath
{
	public class CatmullRom
	{
		public static UnityEngine.Vector3[] SmoothPath(UnityEngine.Vector3[] inCoordinates, int samples)
		{
			int length = inCoordinates.Length;
			
			if(length < 2)
			{
				return inCoordinates;	
			}
			
			UnityEngine.Vector3[] newCoordinates = new UnityEngine.Vector3[length + 2];
			
			newCoordinates[0] = inCoordinates[0] + (inCoordinates[0] - inCoordinates[1]).normalized;
			
			for(int i = 0; i < inCoordinates.Length; ++i)
			{
				newCoordinates[i + 1] = inCoordinates[i]; 
			}
			
			newCoordinates[newCoordinates.Length -1] = inCoordinates[length-1] + (inCoordinates[length-1] - inCoordinates[length-2]).normalized;
			
			
			List<UnityEngine.Vector3> results = new List<UnityEngine.Vector3>();
			
			for (int n = 1; n < newCoordinates.Length - 2; n++)
			{
				for (int i = 0; i < samples; i++)
				{
					results.Add(PointOnCurve(newCoordinates[n - 1], newCoordinates[n], newCoordinates[n + 1], newCoordinates[n + 2], (1f / samples) * i ));
				}
			}
			
			results.Add(newCoordinates[newCoordinates.Length - 2]);
			
			return results.ToArray();
		}


		public static SGPath.PathPoint[] SmoothPath(UnityEngine.Transform[] inCoordinates, int samples)
		{
			int length = inCoordinates.Length;

			SGPath.PathPoint[] newCoordinates = null;
			if(length < 2)
			{
				if(length > 0)
				{
					newCoordinates = new SGPath.PathPoint[length];
					for(int i = 0; i < length; ++i)
					{
						newCoordinates[i] = new SGPath.PathPoint(inCoordinates[i]);
					}
				}
				return newCoordinates;	
			}
			
			newCoordinates = new SGPath.PathPoint[length + 2];

			newCoordinates[0] = new SGPath.PathPoint(inCoordinates[0].position + (inCoordinates[0].position - inCoordinates[1].position).normalized);

			for(int i = 0; i < inCoordinates.Length; ++i)
			{
				newCoordinates[i + 1] = new SGPath.PathPoint(inCoordinates[i]); 
			}
			
			newCoordinates[newCoordinates.Length -1] = new SGPath.PathPoint(inCoordinates[length-1].position + (inCoordinates[length-1].position - inCoordinates[length-2].position).normalized);
			
			
			List<SGPath.PathPoint> results = new List<SGPath.PathPoint>();
			
			for (int n = 1; n < newCoordinates.Length - 2; n++)
			{
				for (int i = 0; i < samples; i++)
				{
					results.Add(PointOnCurve(newCoordinates[n - 1], newCoordinates[n], newCoordinates[n + 1], newCoordinates[n + 2], (1f / samples) * i ));
				}
			}
			
			results.Add(newCoordinates[newCoordinates.Length - 2]);
			
			return results.ToArray();
		}

		
		public static UnityEngine.Vector3[] SmoothLoop(UnityEngine.Vector3[] inCoordinates, int samples)
		{
			int length = inCoordinates.Length;
			
			if(length < 2)
			{
				return inCoordinates;	
			}
			
			
			
			List<UnityEngine.Vector3> results = new List<UnityEngine.Vector3>();
			
			for (int n = 0; n < inCoordinates.Length; n++)
			{
				for (int i = 0; i < samples; i++)
				{
					results.Add(PointOnCurve(inCoordinates[WrapIndex(n - 1, inCoordinates.Length)], inCoordinates[WrapIndex(n, inCoordinates.Length)], inCoordinates[WrapIndex(n + 1, inCoordinates.Length)], inCoordinates[WrapIndex(n + 2, inCoordinates.Length)], (1.0f / (float)samples) * i ));
				}
			}
			
			//results.Add(newCoordinates[newCoordinates.Length - 2]);
			
			return results.ToArray();
		}


		public static SGPath.PathPoint[] SmoothLoop(UnityEngine.Transform[] inCoordinates, int samples)
		{
			int length = inCoordinates.Length;
			

			if(length < 2)
			{
				SGPath.PathPoint[] newCoordinates = null;
				if(length > 0)
				{
					newCoordinates = new SGPath.PathPoint[length];
					for(int i = 0; i < length; ++i)
					{
						newCoordinates[i] = new SGPath.PathPoint(inCoordinates[i]);
					}
				}
				return newCoordinates;	
			}
			
			
			
			List<SGPath.PathPoint> results = new List<SGPath.PathPoint>();
			
			for (int n = 0; n < inCoordinates.Length; n++)
			{
				for (int i = 0; i < samples; i++)
				{
					results.Add(PointOnCurve(inCoordinates[WrapIndex(n - 1, inCoordinates.Length)], inCoordinates[WrapIndex(n, inCoordinates.Length)], inCoordinates[WrapIndex(n + 1, inCoordinates.Length)], inCoordinates[WrapIndex(n + 2, inCoordinates.Length)], (1.0f / (float)samples) * i ));
				}
			}
			
			//results.Add(newCoordinates[newCoordinates.Length - 2]);
			
			return results.ToArray();
		}


		
		public static int WrapIndex(int index, int length)
		{
			while(index < 0)
			{
				index += length;
			}
			
			return index % length;
		}



		public static SGPath.PathPoint PointOnCurve(UnityEngine.Transform p0, UnityEngine.Transform p1, UnityEngine.Transform p2, UnityEngine.Transform p3, float t)
		{
			SGPath.PathPoint result = new SGPath.PathPoint();
			
			float t0 = ((-t + 2f) * t - 1f) * t * 0.5f;
			float t1 = (((3f * t - 5f) * t) * t + 2f) * 0.5f;
			float t2 = ((-3f * t + 4f) * t + 1f) * t * 0.5f;
			float t3 = ((t - 1f) * t * t) * 0.5f;
			
			UnityEngine.Vector3 position = new UnityEngine.Vector3();
			
			position.x = p0.position.x * t0 + p1.position.x * t1 + p2.position.x * t2 + p3.position.x * t3;
			position.y = p0.position.y * t0 + p1.position.y * t1 + p2.position.y * t2 + p3.position.y * t3;
			position.z = p0.position.z * t0 + p1.position.z * t1 + p2.position.z * t2 + p3.position.z * t3;
			
			result.Position = position;
			result.Up = UnityEngine.Vector3.Lerp(p1.up, p2.up, t).normalized;
			
			return result;
		}



		public static SGPath.PathPoint PointOnCurve(SGPath.PathPoint p0, SGPath.PathPoint p1, SGPath.PathPoint p2, SGPath.PathPoint p3, float t)
		{
			SGPath.PathPoint result = new SGPath.PathPoint();
			
			float t0 = ((-t + 2f) * t - 1f) * t * 0.5f;
			float t1 = (((3f * t - 5f) * t) * t + 2f) * 0.5f;
			float t2 = ((-3f * t + 4f) * t + 1f) * t * 0.5f;
			float t3 = ((t - 1f) * t * t) * 0.5f;

			UnityEngine.Vector3 position = new UnityEngine.Vector3();
			
			position.x = p0.Position.x * t0 + p1.Position.x * t1 + p2.Position.x * t2 + p3.Position.x * t3;
			position.y = p0.Position.y * t0 + p1.Position.y * t1 + p2.Position.y * t2 + p3.Position.y * t3;
			position.z = p0.Position.z * t0 + p1.Position.z * t1 + p2.Position.z * t2 + p3.Position.z * t3;

			result.Position = position;
			result.Up = UnityEngine.Vector3.Lerp(p1.Up, p2.Up, t).normalized;
			
			return result;
		}

		
		public static UnityEngine.Vector3 PointOnCurve(UnityEngine.Vector3 p0, UnityEngine.Vector3 p1, UnityEngine.Vector3 p2, UnityEngine.Vector3 p3, float t)
		{
			UnityEngine.Vector3 result = new UnityEngine.Vector3();
			
			float t0 = ((-t + 2f) * t - 1f) * t * 0.5f;
			float t1 = (((3f * t - 5f) * t) * t + 2f) * 0.5f;
			float t2 = ((-3f * t + 4f) * t + 1f) * t * 0.5f;
			float t3 = ((t - 1f) * t * t) * 0.5f;
			
			result.x = p0.x * t0 + p1.x * t1 + p2.x * t2 + p3.x * t3;
			result.y = p0.y * t0 + p1.y * t1 + p2.y * t2 + p3.y * t3;
			result.z = p0.z * t0 + p1.z * t1 + p2.z * t2 + p3.z * t3;
			
			return result;
		}
	}
}

