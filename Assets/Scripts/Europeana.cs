﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class EuropeanaObject {
	private int _Index;
	public int Index {
		set { this._Index = value; }
		get { return this._Index; }
	}

	private string _Id;
	public string Id {
		set { this._Id = value; }
		get { return this._Id; }
	}

	private string _Type;
	public string Type {
		set { this._Type = value; }
		get { return this._Type; }
	}

	private string _Title;
	public string Title {
		set { this._Title = value; }
		get { return this._Title; }
	}

	private string _Description;
	public string Description {
		set {
			this._Description = value;
			if (this._Title == null) {
				this._Title = value;
			}
		}
		get { return this._Description; }
	}

	private string _Attribution;
	public string Attribution {
		set { this._Attribution = value; }
		get { return this._Attribution; }
	}

	private void SetAttribution() {
		this.Attribution = "";
		if (this.Credit != null) {
			this.Attribution = this.Credit;
			if (this.Provider != null) {
				this.Attribution += ", ";
			}
		}
		if (this.Provider != null) {
			this.Attribution += this.Provider;
		}
	}

	private string _Provider;
	public string Provider {
		set {
			this._Provider = value;
			this.SetAttribution ();
		}
		get { return this._Provider; }
	}

	private string _Credit;
	public string Credit {
		set {
			this._Credit = value;
			this.SetAttribution ();
		}
		get { return this._Credit; }
	}

	private Texture2D _TextureData;
	public Texture2D TextureData {
		set { this._TextureData = value; }
		get { return this._TextureData; }
	}

	public EuropeanaObject() {}

	public EuropeanaObject(string newId, string newType) {
		Id = newId;
		Type = newType;
	}
}

public class EuropeanaRequest {
	private bool _Verbose = false;
	public bool Verbose {
		set { this._Verbose = value; }
		get { return this._Verbose; }
	}

	private bool _Ended = false;
	public bool Ended {
		set { this._Ended = value; }
		get { return this._Ended; }
	}

	private EuropeanaRequest _Owner = null;
	public EuropeanaRequest Owner {
		set { this._Owner = value; }
		get { return this._Owner; }
	}

	private Dictionary<string, string> _Options;
	public Dictionary<string, string> Options {
		set { this._Options = value; }
		get { return this._Options; }
	}

	private List<EuropeanaRequest> _SubRequests = null;
	public List<EuropeanaRequest> SubRequests {
		set { this._SubRequests = value; }
		get { return this._SubRequests; }
	}

	private List<EuropeanaObject> _ArchiveObjects = null;
	public List<EuropeanaObject> ArchiveObjects {
		set { this._ArchiveObjects = value; }
		get { return this._ArchiveObjects; }
	}

	private EuropeanaObject _ArchiveObject = null;
	public EuropeanaObject ArchiveObject {
		set { this._ArchiveObject = value; }
		get { return this._ArchiveObject; }
	}

	private Action<EuropeanaObject> _SingleCallback = null;
	public Action<EuropeanaObject> SingleCallback {
		set { this._SingleCallback = value; }
		get { return this._SingleCallback; }
	}

	private Action<List<EuropeanaObject>> _TotalCallback = null;
	public Action<List<EuropeanaObject>> TotalCallback {
		set { this._TotalCallback = value; }
		get { return this._TotalCallback; }
	}

	private Action<EuropeanaObject> _RecordCallback = null;
	public Action<EuropeanaObject> RecordCallback {
		set { this._RecordCallback = value; }
		get { return this._RecordCallback; }
	}

	private Action<EuropeanaObject> _MediaCallback = null;
	public Action<EuropeanaObject> MediaCallback {
		set { this._MediaCallback = value; }
		get { return this._MediaCallback; }
	}

	private Action<int> _ErrorCallback = null;
	public Action<int> ErrorCallback {
		set { this._ErrorCallback = value; }
		get { return this._ErrorCallback; }
	}

	private WWW _Www = null;
	public WWW Www {
		set { this._Www = value; }
		get { return this._Www; }
	}

	private int _Error = 0;
	public int Error {
		set { this._Error = value; }
		get { return this._Error; }
	}

	public EuropeanaRequest() {
		Clear ();
	}
	~EuropeanaRequest() {
		if (Verbose) {
			Debug.Log("~EuropeanaRequest");
		}
		Dispose ();
	}

	public void Dispose() {
		Clear ();
	}

	protected void Clear() {
		EndSubRequests ();
		
		Error = 0;
		Www = null;
		Options = null;
		ArchiveObjects = null;
		ArchiveObject = null;
	}
	
	public void AddSubRequest(EuropeanaRequest subRequest) {
		if (SubRequests == null) {
			SubRequests = new List<EuropeanaRequest>();
		}

		subRequest.Owner = this;
		subRequest.Options = new Dictionary<string, string> (Options);
		subRequest.Verbose = Verbose;
		if (subRequest.SingleCallback == null) {
			subRequest.SingleCallback = SingleCallback;
		}
		if (subRequest.TotalCallback == null) {
			subRequest.TotalCallback = TotalCallback;
		}
		if (subRequest.RecordCallback == null) {
			subRequest.RecordCallback = RecordCallback;
		}
		if (subRequest.MediaCallback == null) {
			subRequest.MediaCallback = MediaCallback;
		}
		if (subRequest.ErrorCallback == null) {
			subRequest.ErrorCallback = ErrorCallback;
		}
		SubRequests.Add (subRequest);
	}

	public void Cancel() {
		if (Verbose) {
			Debug.Log("Europeana cancelling request");
		}

		CallErrorCallback (Europeana.ERROR_CANCELLED);

		End ();
	}

	public void End() {
		lock (this) {
			Ended = true;

			// Remove myself from the manager
			if (Owner != null) {
				Owner.RemoveSubRequest (this);
			}
			EndSubRequests ();
		}
	}
	
	public bool CheckEnded() {
		lock (this) {
			if (Ended) {
				Error = Europeana.ERROR_CANCELLED;
				if (ErrorCallback != null) {
					ErrorCallback (Error);
				}
				return true;
			}
			return false;
		}
	}

	public void CallSingleCallback(EuropeanaObject single) {
		lock (this) {
			if (!Ended && SingleCallback != null) {
				SingleCallback (single);
			}
		}
	}
	public void CallTotalCallback(List<EuropeanaObject> total) {
		lock (this) {
			if (!Ended && TotalCallback != null) {
				TotalCallback (total);
			}
		}
	}
	public void CallRecordCallback(EuropeanaObject record) {
		lock (this) {
			if (!Ended && RecordCallback != null) {
				RecordCallback (record);
			}
		}
	}
	public void CallMediaCallback(EuropeanaObject mediaRecord) {
		lock (this) {
			if (!Ended && MediaCallback != null) {
				MediaCallback (mediaRecord);
			}
		}
	}
	public void CallErrorCallback(int error) {
		lock (this) {
			if (!Ended && ErrorCallback != null) {
				ErrorCallback (error);
			}
		}
	}

	private void EndSubRequests() {
		if (SubRequests == null) {
			return;
		}

		List<EuropeanaRequest> requests = new List<EuropeanaRequest> (SubRequests);
		foreach (EuropeanaRequest subRequest in requests) {
			subRequest.End();
		}

		SubRequests = null;
	}


	public void RemoveSubRequest(EuropeanaRequest subRequest) {
		if (SubRequests == null) {
			return;
		}

		try {
			SubRequests.Remove (subRequest);
		} catch {
			// Do nothing, it's cool
		}
	}
}

public class Europeana : MonoBehaviour {
	public const string VALUE_TRUE = "true";
	public const string VALUE_FALSE = "false";

	public const string OPTION_ID = "id";
	public const string OPTION_SEARCH = "search";
	public const string OPTION_COUNT = "count";
	public const string OPTION_OFFSET = "offset";
	public const string OPTION_WIDTH = "width";
	public const string OPTION_HEIGHT = "height";
	public const string OPTION_MIN_WIDTH = "minwidth";
	public const string OPTION_MIN_HEIGHT = "minheight";
	public const string OPTION_KEEP_ASPECT = "aspect";
	public const string OPTION_POWER_OF_2 = "pow2";
	public const string OPTION_CROP_PERCENTAGE = "crop";
	public const string OPTION_TEXTURE_SIZE = "textureSize";
	public const string OPTION_VERBOSE_LOGGING = "verbose";
	
	public const int ERROR_NONE = 0;
	public const int ERROR_NO_RESULTS = 1;
	public const int ERROR_NONE_FOUND = 2;
	public const int ERROR_REQUEST = 3;
	public const int ERROR_BAD_DATA = 4;
	public const int ERROR_NO_TERMS = 5;
	public const int ERROR_NO_TYPE = 6;
	public const int ERROR_CANCELLED = 7;
	public const int ERROR_BAD_ARCHIVE = 8;
	public const int ERROR_RETRIEVING = 9;
	public const int ERROR_MEDIA_REQUEST = 10;

	private readonly string[] ERRORS = { "No error found",
										 "No results from specified parameters and search terms, offset may be too great",
										 "No results after filtering for available assets, offset may be too great",
										 "Problem sending request, search terms may include invalid characters",
										 "Malformed or empty data received from server, may be inaccessible",
										 "No or bad search terms",
										 "No media type specified, media type necessary to retrieve content",
									     "Cancelled by user",
										 "The content could not be retrieved from the archive that provides this asset",
										 "There was an error retrieving the asset from an archive entry page",
										 "Problem sending media request, catching an error with Unity's WWW request - use verbose logging to check URL being sent" };
	private const string UNKNOWN_ERROR = "Unknown error";

	private const string API_URL = "http://sgiserver.co.uk";
	private const string API_PORT = "9001";

	private const string PARAM_QUERY = "?";
	private const string PARAM_SEPERATOR = "&";
	private const string PARAM_EQUALS = "=";

	private const string PARAM_TYPE = "type";
	private const string PARAM_SEARCH = "search";
	private const string PARAM_FIRST_INDEX = "first";
	private const string PARAM_COUNT = "count";
	private const string PARAM_MEDIA = "media";
	private const string PARAM_ONLY_AVAILABLE = "availableOnly";
	private const string PARAM_ID = "id";
	private const string PARAM_WIDTH = "width";
	private const string PARAM_HEIGHT = "height";
	private const string PARAM_MIN_WIDTH = "minwidth";
	private const string PARAM_MIN_HEIGHT = "minheight";
	private const string PARAM_KEEP_ASPECT = "aspect";
	private const string PARAM_POWER_OF_2 = "pow2";
	private const string PARAM_CROP_PERCENTAGE = "crop";

	private const string TYPE_RECORDS = "records";
	private const string TYPE_RECORD = "record";
	private const string TYPE_IMAGE = "image";
//	private const string TYPE_AUDIO = "audio"; // Not ready for Unity
//	private const string TYPE_VIDEO = "video"; // Not ready for Unity

	private const string RESULT_ITEMS = "items";
	private const string RESULT_TITLE = "title";
	private const string RESULT_DESCRIPTION = "description";
	private const string RESULT_ATTRIBUTION = "attribution";
	private const string RESULT_PROVIDER = "provider";
	private const string RESULT_CREDIT = "credit";

	private const string RESPONSE_WARNING = "WARNING";

	private const int DEFAULT_COUNT = 1;
	private const int DEFAULT_OFFSET = 0;
	private const int DEFAULT_TEXTURE_SIZE = 1024;
	private const string DEFAULT_VERBOSE = VALUE_FALSE;

	private const int NUM_INVALID = -1;

	public Europeana() { }

	public string PrintError(int error) {
		string message = "";

		if (error <= NUM_INVALID || error >= ERRORS.Length) {
			message = UNKNOWN_ERROR;
		} else {
			message = ERRORS[error];
		}

		Debug.Log (message);
		return message;
	}

	public EuropeanaRequest GetImages(Dictionary<string, string> options, Action<EuropeanaObject> single, Action<List<EuropeanaObject>> total = null, Action<int> error = null) {
		return GetRecords (TYPE_IMAGE, options, single, total, error);
	}

	public EuropeanaRequest GetImages(string search, int count, int offset, int width, int height, bool keepAspect, bool powerOf2, int cropPercentage, int textureSize, Action<EuropeanaObject> single, Action<List<EuropeanaObject>> total = null, Action<int> error = null) {

		Dictionary<string, string> options = new Dictionary<string, string> ();
		options.Add (OPTION_SEARCH, search);
		options.Add (OPTION_COUNT, count.ToString ());
		options.Add (OPTION_OFFSET, offset.ToString ());
		options.Add (OPTION_WIDTH, width.ToString ());
		options.Add (OPTION_HEIGHT, height.ToString ());
		options.Add (OPTION_KEEP_ASPECT, keepAspect ? VALUE_TRUE : VALUE_FALSE);
		options.Add (OPTION_POWER_OF_2, powerOf2 ? VALUE_TRUE : VALUE_FALSE);
		options.Add (OPTION_CROP_PERCENTAGE, cropPercentage.ToString ());
		options.Add (OPTION_TEXTURE_SIZE, textureSize.ToString ());

		return GetImages (options, single, total, error);
	}

	public EuropeanaRequest GetImage(Dictionary<string, string> options, Action<EuropeanaObject> callback, Action<int> error = null) {
		string recordId = options [OPTION_ID];
		EuropeanaRequest request = new EuropeanaRequest ();
//		request.MediaCallback = callback;
		request.ErrorCallback = error;
		request.Options = new Dictionary<string, string> (options);

		return GetRecord (recordId, TYPE_IMAGE, request, callback);
	}
	
	public EuropeanaRequest GetImage(string recordId, int width, int height, bool keepAspect, bool powerOf2, int cropPercentage, int textureSize, Action<EuropeanaObject> callback, Action<int> error = null) {
		
		Dictionary<string, string> options = new Dictionary<string, string> ();
		options.Add (OPTION_ID, recordId);
		options.Add (OPTION_WIDTH, width.ToString ());
		options.Add (OPTION_HEIGHT, height.ToString ());
		options.Add (OPTION_KEEP_ASPECT, keepAspect ? VALUE_TRUE : VALUE_FALSE);
		options.Add (OPTION_POWER_OF_2, powerOf2 ? VALUE_TRUE : VALUE_FALSE);
		options.Add (OPTION_CROP_PERCENTAGE, cropPercentage.ToString ());
		options.Add (OPTION_TEXTURE_SIZE, textureSize.ToString ());
		
		return GetImage (options, callback, error);
	}

	public EuropeanaRequest GetRecords(string type, Dictionary<string, string> options, Action<EuropeanaObject> single, Action<List<EuropeanaObject>> total = null, Action<int> error = null) {
		EuropeanaRequest request = new EuropeanaRequest ();
		request.SingleCallback = single;
		request.TotalCallback = total;
		request.ErrorCallback = error;
		request.Options = new Dictionary<string, string> (options);

		StartCoroutine (DoGetRecords (type, request));
		return request;
	}

	private IEnumerator DoGetRecords(string type, EuropeanaRequest request) {
		if (request.CheckEnded()) {
			return false;
		}

		string isVerbose = DEFAULT_VERBOSE;
		try {
			isVerbose = request.Options[OPTION_VERBOSE_LOGGING];
		} catch {
			isVerbose = DEFAULT_VERBOSE;
		}
		if (isVerbose.ToLower () == VALUE_FALSE) {
			request.Verbose = false;
		} else {
			request.Verbose = true;
		}

		if (request.Verbose) Debug.Log ("Europeana getting records with type " + type);

		if (type == null) {
			request.SingleCallback = null;

			if (request.Verbose) Debug.Log("Europeana type not specified");

			if (request.ErrorCallback != null) {
				request.Error = ERROR_NO_TYPE;
				request.CallErrorCallback(request.Error);
			} else if (request.TotalCallback != null) {
				request.ArchiveObjects = new List<EuropeanaObject>();
				request.CallTotalCallback(request.ArchiveObjects);
			}
			request.End();
			return false;
		}

		type = type.ToLower ();

		string search = null;
		try {
			search = request.Options[OPTION_SEARCH];
		} catch {
			search = null;
		}

		if (search == null) {
			request.SingleCallback = null;

			if (request.Verbose) Debug.Log("Europeana search option not provided!");
			
			if (request.ErrorCallback != null) {
				request.Error = ERROR_NO_TERMS;
				request.CallErrorCallback(request.Error);
			} else if (request.TotalCallback != null) {
				request.ArchiveObjects = new List<EuropeanaObject>();
				request.CallTotalCallback(request.ArchiveObjects);
			}
			request.End();
			return false;
		}

		int count = DEFAULT_COUNT;
		try {
			count = Int16.Parse (request.Options[OPTION_COUNT]);
		} catch {
			count = DEFAULT_COUNT;
		}

		int offset = DEFAULT_OFFSET;
		try {
			offset = Int32.Parse (request.Options[OPTION_OFFSET]);
		} catch {
			offset = DEFAULT_OFFSET;
		}

		string url = API_URL + ":" + API_PORT + "/";
		url += PARAM_QUERY + PARAM_TYPE + PARAM_EQUALS + TYPE_RECORDS;
		url += PARAM_SEPERATOR + PARAM_SEARCH + PARAM_EQUALS + WWW.EscapeURL (search);
		url += PARAM_SEPERATOR + PARAM_MEDIA + PARAM_EQUALS + type;
		url += PARAM_SEPERATOR + PARAM_COUNT + PARAM_EQUALS + count;
		url += PARAM_SEPERATOR + PARAM_FIRST_INDEX + PARAM_EQUALS + offset;
		url += PARAM_SEPERATOR + PARAM_ONLY_AVAILABLE + PARAM_EQUALS + VALUE_TRUE;

		int minWidth = -1;
		try {
			minWidth = Int16.Parse (request.Options[OPTION_MIN_WIDTH]);
		} catch {
			minWidth = -1;
		}
		if (minWidth > -1) {
			url += PARAM_SEPERATOR + PARAM_MIN_WIDTH + PARAM_EQUALS + minWidth;
		}

		int minHeight = -1;
		try {
			minHeight = Int16.Parse (request.Options[OPTION_MIN_HEIGHT]);
		} catch {
			minHeight = -1;
		}
		if (minHeight > -1) {
			url += PARAM_SEPERATOR + PARAM_MIN_HEIGHT + PARAM_EQUALS + minHeight;
		}

		if (request.Verbose) Debug.Log ("Europeana records query Url: " + url);

		try {
			request.Www = new WWW (url);
		} catch {
			if (request.ErrorCallback != null) {
				request.Error = ERROR_REQUEST;
				request.CallErrorCallback(request.Error);
			} else if (request.TotalCallback != null) {
				request.ArchiveObjects = new List<EuropeanaObject>();
				request.CallTotalCallback(request.ArchiveObjects);
			}
			request.End ();
			return false;
		}

		List<string> ids = new List<string> ();

		if (request.Www != null) {
			yield return request.Www;

			if (request.CheckEnded()) {
				return false;
			}

			if (BadWWWResponse(request)) {
				return false;
			}

			JSONObject json = null;
			JSONObject jsonList = null;

			try {
				json = new JSONObject (request.Www.text);
				jsonList = json [RESULT_ITEMS];
			} catch {
				if (request.ErrorCallback != null) {
					request.Error = ERROR_BAD_DATA;
					request.CallErrorCallback(request.Error);
				} else if (request.TotalCallback != null) {
					request.ArchiveObjects = new List<EuropeanaObject>();
					request.CallTotalCallback(request.ArchiveObjects);
				}
				request.End ();
				return false;
			}

			if (jsonList != null) {
				for (int i = 0; i < jsonList.list.Count; i++) {
					JSONObject jsonId = (JSONObject)jsonList.list [i];
					string id = jsonId.str;
					ids.Add (id);
				}
			}
		}

		if (ids.Count <= 0) {
			if (request.Verbose) Debug.Log("Europeana no results found with specified parameters and search terms: "+search);

			if (request.ErrorCallback != null) {
				request.Error = ERROR_NO_RESULTS;
				request.CallErrorCallback(request.Error);
			} else if (request.TotalCallback != null) {
				request.ArchiveObjects = new List<EuropeanaObject>();
				request.CallTotalCallback(request.ArchiveObjects);
			}
			request.End ();
			return false;
		}

		IterateRecords (ids, -1, type, null, request);
	}

	private void IterateRecords(List<string> items, int index, string type, List<EuropeanaObject> objects, EuropeanaRequest request) {
		if (request.CheckEnded()) {
			return;
		}

		if (objects == null) {
			objects = new List<EuropeanaObject>();
		}

		index++;
		if (index >= items.Count) {
			if (items.Count <= 0 && request.ErrorCallback != null) {
				request.Error = ERROR_NONE_FOUND;
				request.CallErrorCallback(request.Error);
			} else if (request.TotalCallback != null) {
				request.ArchiveObjects = objects;
				request.CallTotalCallback(request.ArchiveObjects);
			}
			request.End ();
			return;
		}

		string item = items [index];

		GetRecord (item, type, request, newObject => {
			if (request.CheckEnded()) {
				return;
			}

			if (newObject != null) {
				newObject.Index = objects.Count;
				objects.Add(newObject);
				request.CallSingleCallback(newObject);
			}

			IterateRecords(items, index, type, objects, request);
		});
	}

	public EuropeanaRequest GetRecord(string id, string type, EuropeanaRequest request, Action<EuropeanaObject> callback = null) {
		if (request.CheckEnded()) {
			return null;
		}

		EuropeanaRequest subRequest = new EuropeanaRequest ();
		subRequest.RecordCallback = callback;
		request.AddSubRequest (subRequest);

		StartCoroutine(DoGetRecord(id, type, subRequest));
		return subRequest;
	}

	private IEnumerator DoGetRecord(string id, string type, EuropeanaRequest request) {
		if (request.CheckEnded()) {
			return false;
		}

		string url = API_URL + ":" + API_PORT + "/";
		url += PARAM_QUERY + PARAM_TYPE + PARAM_EQUALS + TYPE_RECORD;
		url += PARAM_SEPERATOR + PARAM_ID + PARAM_EQUALS + id;

		if (request.Verbose) Debug.Log ("Europeana record query Url: " + url);

		try {
			request.Www = new WWW (url);
		} catch {
			request.CallRecordCallback(null);
			request.End ();
			return false;
		}

		yield return request.Www;

		if (request.CheckEnded()) {
			return false;
		}

		if (BadWWWResponse(request)) {
			request.CallRecordCallback(null);
			return false;
		}

		JSONObject json = null;

		Debug.Log ("Text (for id "+id+") : " + request.Www.text);

		try {
			json = new JSONObject (request.Www.text);
		} catch {
			request.CallRecordCallback(null);
			request.End ();
			return false;
		}

		request.ArchiveObject = new EuropeanaObject (id, type);

		try {
			request.ArchiveObject.Title = json [RESULT_TITLE].str;
		} catch {
		}

		try {
			request.ArchiveObject.Description = json [RESULT_DESCRIPTION].str;
		} catch {
		}

		JSONObject attribution = null;
		try {
			attribution = json[RESULT_ATTRIBUTION];
		} catch {
		}

		if (attribution != null) {
			try {
				request.ArchiveObject.Provider = attribution [RESULT_PROVIDER].str;
			} catch {
				request.ArchiveObject.Provider = null;
			}
			
			try {
				request.ArchiveObject.Credit = attribution [RESULT_CREDIT].str;
			} catch {
				request.ArchiveObject.Credit = null;
			}
		}

		GetMediaForRecord (id, type, request.ArchiveObject, request, successObject => {
			if (request.CheckEnded()) {
				return;
			}

			request.CallRecordCallback(successObject);
		});
	}

	public EuropeanaRequest GetMediaForRecord(string id, string type, EuropeanaObject recordObject, EuropeanaRequest request, Action<EuropeanaObject> callback) {
		if (request.CheckEnded()) {
			return null;
		}

		EuropeanaRequest subRequest = new EuropeanaRequest ();
		subRequest.MediaCallback = callback;
		request.AddSubRequest (subRequest);

		StartCoroutine(DoGetMediaForRecord(id, type, recordObject, subRequest));
		return subRequest;
	}
	
	private IEnumerator DoGetMediaForRecord(string id, string type, EuropeanaObject recordObject, EuropeanaRequest request) {
		if (request.CheckEnded()) {
			return false;
		}

		string url = API_URL + ":" + API_PORT + "/";
		url += PARAM_QUERY + PARAM_TYPE + PARAM_EQUALS + type;
		url += PARAM_SEPERATOR + PARAM_ID + PARAM_EQUALS + id;

		if (type == TYPE_IMAGE) {
			try {
				int width = Int16.Parse (request.Options [OPTION_WIDTH]);
				if (width > 0) {
					url += PARAM_SEPERATOR + PARAM_WIDTH + PARAM_EQUALS + width;
				}
			} catch {
				if (request.Verbose)
					Debug.Log ("Europeana getting image, width not supplied");
			}

			try {
				int height = Int16.Parse (request.Options [OPTION_HEIGHT]);
				if (height > 0) {
					url += PARAM_SEPERATOR + PARAM_HEIGHT + PARAM_EQUALS + height;
				}
			} catch {
				if (request.Verbose)
					Debug.Log ("Europeana getting image, height not supplied");
			}

			try {
				string aspect = request.Options [OPTION_KEEP_ASPECT];
				string keepAspect = VALUE_FALSE;
				if (aspect.ToLower () == VALUE_TRUE) {
					keepAspect = VALUE_TRUE;
				}
				url += PARAM_SEPERATOR + PARAM_KEEP_ASPECT + PARAM_EQUALS + keepAspect;
			} catch {
				if (request.Verbose)
					Debug.Log ("Europeana getting image, keep aspect ratio not supplied");
			}

			try {
				string pow2 = request.Options [OPTION_POWER_OF_2];
				string powerOf2 = VALUE_FALSE;
				if (pow2.ToLower () == VALUE_TRUE) {
					powerOf2 = VALUE_TRUE;
				}
				url += PARAM_SEPERATOR + PARAM_POWER_OF_2 + PARAM_EQUALS + powerOf2;
			} catch {
				if (request.Verbose)
					Debug.Log ("Europeana getting image, power of 2 not supplied");
			}

			try {
				int crop = Int32.Parse (request.Options [OPTION_CROP_PERCENTAGE]);
				if (crop > 0) {
					url += PARAM_SEPERATOR + PARAM_CROP_PERCENTAGE + PARAM_EQUALS + crop;
				}
			} catch {
				if (request.Verbose)
					Debug.Log ("Europeana getting image, crop percentage not supplied");
			}
		} else {
			if (request.Verbose) Debug.Log ("Europeana media type "+type+" not implemented");

			request.CallMediaCallback(null);
			request.End ();
			return false;
		}

		if (request.Verbose) Debug.Log ("Europeana getting media Url: " + url);

		try {
			request.Www = new WWW (url);
		} catch {
			request.CallMediaCallback(null);
			if (request.ErrorCallback != null) {
				request.Error = ERROR_MEDIA_REQUEST;
				request.CallErrorCallback(request.Error);
			}
			request.End();
			return false;
		}

		yield return request.Www;

		if (request.CheckEnded ()) {
			return false;
		}

		if (BadWWWResponse(request)) {
			request.CallMediaCallback(null);
			return false;
		}

		if (type == TYPE_IMAGE) {
			int size = DEFAULT_TEXTURE_SIZE;

			try {
				int testSize = Int16.Parse (request.Options[OPTION_TEXTURE_SIZE]);
				if (testSize > 0) {
					size = testSize;
				}
			} catch {
				size = DEFAULT_TEXTURE_SIZE;
			}

			try {
				Texture2D texture = new Texture2D (size, size);
				request.Www.LoadImageIntoTexture(texture);

				recordObject.TextureData = texture;
				request.ArchiveObject = recordObject;
				request.CallMediaCallback(recordObject);
				request.End ();
			} catch {
				request.CallMediaCallback(null);
				request.End ();
			}
		} else {
			if (request.Verbose) Debug.Log("Europeana media type "+type+" not implemented");
			request.ArchiveObject = recordObject;
			request.CallMediaCallback(recordObject);
			request.End ();
		}
	}

	private bool BadWWWResponse(EuropeanaRequest request) {
		int warning = ERROR_NONE;
		try {
			warning = Int16.Parse (request.Www.responseHeaders[RESPONSE_WARNING]);
		} catch {
			warning = ERROR_NONE;
		}
		
		if (warning != ERROR_NONE) {
			if (request.Verbose) Debug.Log("Europeana response code: "+warning+", message: "+request.Www.text);

			request.Error = warning;
			request.CallErrorCallback(request.Error);
			request.End ();
			return true;
		}
		
		return false;
	}

}
