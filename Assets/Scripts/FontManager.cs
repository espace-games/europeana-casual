﻿using UnityEngine;
using System.Collections;

public class FontManager : MonoBehaviour {

	private static FontManager INSTANCE = null;

	public Font OpenSans_Light_12 = null;
	public Font OpenSans_Light_30 = null;

	public Font OpenSans_Light_Italic_30 = null;

	public Font OpenSans_Regular_48 = null;
	
	public static Font GetFontOpenSansLight12()
	{
		return (INSTANCE != null ? INSTANCE.OpenSans_Light_12 : null);
	}

	public static Font GetFontOpenSansLight30()
	{
		return (INSTANCE != null ? INSTANCE.OpenSans_Light_30 : null);
	}

	public static Font GetFontOpenSansLightItalic30()
	{
		return (INSTANCE != null ? INSTANCE.OpenSans_Light_Italic_30 : null);
	}

	public static Font GetFontOpenSansRegular48()
	{
		return (INSTANCE != null ? INSTANCE.OpenSans_Regular_48 : null);
	}

	public static bool ApplyFont_OpenSansLight12(SGGUI.GUILabel lbl)
	{
		return ApplyFont(lbl, GetFontOpenSansLight12(), 12);
	}


	public static bool ApplyFont_OpenSansLight30(SGGUI.GUILabel lbl)
	{
		return ApplyFont(lbl, GetFontOpenSansLight30(), 30);
	}

	public static bool ApplyFont_OpenSansLightItalic30(SGGUI.GUILabel lbl)
	{
		return ApplyFont(lbl, GetFontOpenSansLightItalic30(), 30);
	}

	public static bool ApplyFont_OpenSansRegular48(SGGUI.GUILabel lbl)
	{
		return ApplyFont(lbl, GetFontOpenSansRegular48(), 48);
	}


	private static bool ApplyFont(SGGUI.GUILabel lbl, Font font, int fontSize)
	{
		if(lbl != null && font != null)
		{
			lbl.Style.fontSize = fontSize;
			lbl.Style.font = font;
			lbl.Autosize();
			return true;
		}
		return true;
	}

	void Awake()
	{
		INSTANCE = this;
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
