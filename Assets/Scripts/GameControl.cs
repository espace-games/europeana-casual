﻿using UnityEngine;
using System.Collections;

public class GameControl : MonoBehaviour {


	public static bool ONLINE_ACCESS_ENABLED = true;

	public static float THINK_TIME = 0.1f;
	public static float TIME_BEST = THINK_TIME + 0.3f;
	public static float TIME_AVERAGE = THINK_TIME + 0.8f;
	public static float TIME_WORST = THINK_TIME + 1.5f;


	public static float BAD_CLICK_VALUE = 0.5f;
	public static float OK_CLICK_VALUE = 1.0f;
	public static float GOOD_CLICK_VALUE = 1.5f;

	public enum eState
	{
		OnTitleScreen,
		SelectingSource,
		PlayingGame
	}

	public PaintingGame paintingGame = null;
	public GameUI gui = null;
	public Camera cameraEnvironment = null;
	public Camera cameraGame = null;

	public AudioSource[] sfxTones = null;
	public AudioSource[] sfxResults = null;

	public PaintingProvider offlineProvider = null;
	public PaintingProviderOnline onlineProvider = null;
	public string _currentSearchTerm = null;


	private PathFollower _cameraPathFollower = null;

	private static GameControl INSTANCE = null;

	private bool _initialised = false;

	private int _currentLevel = 1;
	private int _scoreRound = 0;
	private int _scoreBanked = 0;
	private float _lastSuccessPercentage = 0.0f;
	private eState _state = eState.OnTitleScreen;

	public static void PlaySFXTone(int index)
	{
		if(INSTANCE != null && INSTANCE.sfxTones != null && index >= 0 && index < INSTANCE.sfxTones.Length)
		{
			if(INSTANCE.sfxTones[index] != null)
			{
				INSTANCE.sfxTones[index].Play();
			}
		}
	}

	public static void PlaySFXResults(int index)
	{
		if(INSTANCE != null && INSTANCE.sfxResults != null && index >= 0 && index < INSTANCE.sfxResults.Length)
		{
			if(INSTANCE.sfxResults[index] != null)
			{
				INSTANCE.sfxResults[index].Play();
			}
		}
	}
	
	public static int[] PAINT_LINE_MINI_GAME_SCORES = {100, 200, 400};

	public static float ClickCountRequired(float distance)
	{
		return Mathf.Max((distance/PaintingGame.MIN_EDGE_SIZE), 1.0f);
	}

	public static void SetState(eState state)
	{
		if(INSTANCE != null)
		{
			INSTANCE.InternalSetState(state);
		}
	}

	public static PaintingGame GetPaintingGame()
	{
		return (INSTANCE != null ? INSTANCE.paintingGame : null);
	}

	public static GameUI GetGUI()
	{
		return (INSTANCE != null ? INSTANCE.gui : null);
	}

	//Points & Results
	public static int GetCurrentPointsDisplayAccount()
	{
		return (INSTANCE != null ? (INSTANCE._scoreBanked + INSTANCE._scoreRound) : 0);
	}

	public static void AddRoundPoints(int points, Vector2 positionEarned)
	{
		if(points > 0 && INSTANCE != null)
		{
			INSTANCE._scoreRound += points;

			if(INSTANCE.gui != null)
			{
			
				SGGUI.GamePage page = (SGGUI.GamePage)INSTANCE.gui.GetPage(GameUI.ePages.Game);
			
				if(page != null)
				{
					page.SetPointsDisplay(GetCurrentPointsDisplayAccount());
					//page.ShowParticle(points.ToString(), 1.0f, Color.white, positionEarned, 0.0f, -Vector2.up * 200.0f);
					//page.ShowParticle(points, positionEarned);
				}
			}
		}
	}

	public static int BankPoints(int bonus)
	{
		if(INSTANCE != null)
		{
			INSTANCE._scoreBanked += (INSTANCE._scoreRound + bonus);
			INSTANCE._scoreRound = 0;

			return INSTANCE._scoreBanked;
		}

		return 0;
	}

	public static float GetLastSuccessPercentage()
	{
		return (INSTANCE != null ? INSTANCE._lastSuccessPercentage : 0);
	}

	public static void SetLastSuccessPercentage(float successPercentage)
	{
		if(INSTANCE != null)
		{
			INSTANCE._lastSuccessPercentage = successPercentage;
		}
	}

	public static int GetCurrentLevel()
	{
		return (INSTANCE != null ? INSTANCE._currentLevel : 1);
	}

	public static int AdvanceLevel()
	{
		if(INSTANCE != null)
		{
			INSTANCE._currentLevel++;
		}
		return GetCurrentLevel();
	}

	public static int CalculateBonus(int restorationPercentage)
	{
		return (restorationPercentage * 100 * GetCurrentLevel()) + Random.Range(1, 99);
	}

	public static int PassPercentageForLevel(int level)
	{
		int min = 20;
		int step = 5;
		int max = 90;
		return Mathf.Clamp(min + ((level - 1) * step), min, max);
	}

	public static bool SetOnlineSearchTerm(string searchTerm, OnlineImageMonitor monitor)
	{
		if(INSTANCE != null)
		{
			return INSTANCE.InternalSetOnlineSearchTerm(searchTerm, monitor);
		}
		return false;
	}

	private bool InternalSetOnlineSearchTerm(string searchTerm, OnlineImageMonitor monitor)
	{
		if(searchTerm != _currentSearchTerm)
		{
			_currentSearchTerm = searchTerm;
			if(ONLINE_ACCESS_ENABLED && onlineProvider != null)
			{
				return onlineProvider.DownloadPaintings(_currentSearchTerm, monitor);
			}
		}
		return false;
	}


	public static PaintingProvider.PaintingInfo GetRandomPainting(bool offlineOnly)
	{
		return (INSTANCE != null ? INSTANCE.InternalGetRandomPainting(offlineOnly) : null);
	}

	private PaintingProvider.PaintingInfo InternalGetRandomPainting(bool offlineOnly)
	{
		PaintingProvider.PaintingInfo info = null;
		if(ONLINE_ACCESS_ENABLED && onlineProvider != null && !offlineOnly && _currentSearchTerm != null && _currentSearchTerm.Length > 0)
		{
			info = onlineProvider.GetNewRandomPainting();
		}

		if(info == null && offlineProvider != null)
		{
			info = offlineProvider.GetNewRandomPainting();
		}

		return info;
	}

	public static PaintingProvider GetOfflinePaintingProvider()
	{
		return (INSTANCE != null ? INSTANCE.offlineProvider : null);
	}

	void Awake()
	{
		INSTANCE = this;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Initialise();

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			switch(_state)
			{
			case eState.OnTitleScreen:

				if(gui != null && gui.CurrentPage == GameUI.ePages.Information)
				{
					gui.ShowPage(GameUI.ePages.Title);
				}
				else
				{
					Debug.Log ("Application Quit");
					Application.Quit();
				}
				break;
			case eState.SelectingSource:

				break;
			case eState.PlayingGame:
				InternalSetState(eState.OnTitleScreen);
				break;
			default:
				
				break;
			}
		}
	}

	protected void InternalSetState(eState state)
	{
		_state = state;
		switch(_state)
		{
		case eState.OnTitleScreen:
			if(paintingGame != null)
			{
				paintingGame.Reset();
			}
			_currentLevel = 1;
			_scoreBanked = 0;
			_scoreRound = 0;
			EnableTitleCamera();
			if(gui != null)
			{
				gui.ShowPage(GameUI.ePages.Title);
				gui.SetVignetteAlpha(0.5f);
			}

			break;
		case eState.SelectingSource:
			if(paintingGame != null)
			{
				paintingGame.Reset();
			}
			_currentLevel = 1;
			_scoreBanked = 0;
			_scoreRound = 0;
			EnableTitleCamera();
			if(gui != null)
			{
				gui.ShowPage(GameUI.ePages.Source);
				gui.SetVignetteAlpha(0.5f);
			}
			break;
		
		case eState.PlayingGame:
			EnableGameCamera();
			_currentLevel = 1;
			_scoreBanked = 0;
			_scoreRound = 0;

			if(paintingGame != null)
			{
				paintingGame.StartGame();
				gui.SetVignetteAlpha(0.0f);
			}

			if(gui != null)
			{
				gui.ShowPage(GameUI.ePages.Game);
			}
			break;
		default:

			break;
		}
	}

	public void Initialise()
	{
		if(!_initialised)
		{
			_cameraPathFollower = (cameraEnvironment != null) ? cameraEnvironment.gameObject.GetComponent<PathFollower>() : null;

			InternalSetState(eState.OnTitleScreen);
			_initialised = true;
		}
	}

	public void EnableEnvironmentCamera()
	{
		if(cameraEnvironment != null)
		{

		}
	}

	public void EnableTitleCamera()
	{
		SafeEnableCamera(cameraGame, false);
		if(_cameraPathFollower != null)
		{
			_cameraPathFollower.ResetProgression();
			_cameraPathFollower.Move(true);
		}

		SafeEnableCamera(cameraEnvironment, true);
	}

	public void EnableGameCamera()
	{
		SafeEnableCamera(cameraEnvironment, false);

		if(_cameraPathFollower != null)
		{
			_cameraPathFollower.ResetProgression();
			_cameraPathFollower.Move(false);
		}

		SafeEnableCamera(cameraGame, true);
	}

	private void SafeEnableCamera(Camera cam, bool enable)
	{
		if(cam != null)
		{
			cam.gameObject.SetActive(enable);
		}
	}
}
