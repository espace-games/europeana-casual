﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour {


	public enum ePages
	{
		Title = 0,
		Game,
		Source,
		Results,
		Information,
		Instructions,
		COUNT
	}


	public Camera currentCamera = null;
	public Texture vignetteTexture = null;
	public Font fontSmaller = null;
	public Font fontSmall = null;
	public Font fontMedium = null;
	public Font fontLarge = null;
	
	private SGGUI.GUIScreen _gui = null;
	private SGGUI.GUILabel _testLabel = null;
	private float _screenWidth = Screen.width;
	private float _screenHeight = Screen.height;
	private Brush _brush = null;
	private bool _wasTouched = false;
	private Vector3 _lastTouch = Vector3.zero;
	private SGGUI.Page[] _pages = null;
	private ePages _currentPage = ePages.COUNT;

	private float _vignetteAlphaCurrent = 0.5f;
	private float _vignetteAlphaDesired = 0.5f;

	public void SetVignetteAlpha(float alpha)
	{
		_vignetteAlphaDesired = Mathf.Clamp(alpha, 0.0f, 1.0f);
	}

	public void ShowFeedBackText(int points, SGGUI.FlagParticles.eRating rating, Vector2 position)
	{
		SGGUI.GamePage page = (SGGUI.GamePage)GetPage(ePages.Game);
		if(page != null)
		{
			page.ShowParticle(points, rating, position);
		}
	}

	public void StartBrushLineMiniGame(Brush brush)
	{
		_brush = brush;

		SGGUI.GamePage page = (SGGUI.GamePage)GetPage(ePages.Game);
		page.UpdateBrushPosition(_brush, currentCamera, _screenWidth, _screenHeight);

		//UpdateBrushPosition();
		SGGUI.PaintLineMiniGame game = GetPaintingGame();
		if(game != null)
		{
			game.Start();
		}
	}

	public SGGUI.PaintLineMiniGame.ClickResults StopBrushLineMiniGame()
	{
		_brush = null;

		SGGUI.PaintLineMiniGame game = GetPaintingGame();
		if(game != null)
		{
			return game.Stop();
		}
		return null;
	}

	public void ShowPage(ePages page)
	{
		if(page != _currentPage)
		{
			if(_pages != null)
			{
				int currentPageIndex = (int)_currentPage;
				if(currentPageIndex >= 0 && currentPageIndex < _pages.Length)
				{
					if(_pages[currentPageIndex] != null)
					{
						_pages[currentPageIndex].Exit();
					}
				}
			}

			_currentPage = page;
			int pageIndex = (int)_currentPage;
			if(pageIndex >= 0 && pageIndex < _pages.Length)
			{
				if(_pages[pageIndex] != null)
				{
					_pages[pageIndex].Enter();
				}
			}
		}
	}

	public ePages CurrentPage
	{
		get { return _currentPage; }
	}

	public SGGUI.Page GetPage(ePages page)
	{
		if(_pages != null)
		{
			int index = (int)page;
			if(index >= 0 && index < _pages.Length)
			{
				return _pages[index];
			}
		}
		return null;
	}

	private SGGUI.PaintLineMiniGame GetPaintingGame()
	{
		SGGUI.GamePage page = (SGGUI.GamePage)GetPage(ePages.Game);
		return (page != null ? page.GameInput : null);
	}

	// Use this for initialization
	void Start () {
	
		_gui = new SGGUI.GUIScreen();

		/*float cornerTestSize = 64.0f;

		SGGUI.GUIImage image = (SGGUI.GUIImage)_gui.AddChild(new SGGUI.GUIImage(-cornerTestSize, cornerTestSize, "clickbox_bottomleft.png"));
		image.Docking = SGGUI.GUIEntity.eDock.TopRight;

		image = (SGGUI.GUIImage)_gui.AddChild(new SGGUI.GUIImage(cornerTestSize, cornerTestSize, "clickbox_bottomright.png"));
		image.Docking = SGGUI.GUIEntity.eDock.TopLeft;

		image = (SGGUI.GUIImage)_gui.AddChild(new SGGUI.GUIImage(-cornerTestSize, -cornerTestSize, "clickbox_topleft.png"));
		image.Docking = SGGUI.GUIEntity.eDock.BottomRight;

		image = (SGGUI.GUIImage)_gui.AddChild(new SGGUI.GUIImage(cornerTestSize, -cornerTestSize, "clickbox_topright.png"));
		image.Docking = SGGUI.GUIEntity.eDock.BottomLeft;*/

		_testLabel = (SGGUI.GUILabel)_gui.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Hello World"));
		_testLabel.Docking = SGGUI.GUIEntity.eDock.MiddleMiddle;
		_testLabel.Colour = Color.red;
		_testLabel.Style.fontSize = 48;
		_testLabel.Stroke = 2.0f;
		_testLabel.StrokeColour = Color.black;
		_testLabel.Style.alignment = TextAnchor.MiddleCenter;
		_testLabel.Visible = false;


		SGGUI.GUIEntity pageDock = _gui.AddChild(new SGGUI.GUIEntity());

		_pages = new SGGUI.Page[(int)ePages.COUNT];
		_pages[(int)ePages.Title] = SGGUI.TitlePage.Create(pageDock);
		_pages[(int)ePages.Source] = SGGUI.SourcePage.Create(pageDock);
		_pages[(int)ePages.Game] = SGGUI.GamePage.Create(pageDock);
		_pages[(int)ePages.Results] = SGGUI.ResultsPage.Create(pageDock);
		_pages[(int)ePages.Information] = SGGUI.InformationPage.Create(pageDock);
		_pages[(int)ePages.Instructions] = SGGUI.HelpPage.Create(pageDock);

		StopBrushLineMiniGame();

		ShowPage(ePages.COUNT);
	}
	
	// Update is called once per frame
	void Update () {
	
		//UpdateBrushPosition();
		InputNormal();

		if(_gui != null)
		{
			_gui.Update(Time.deltaTime);
		}

		SGGUI.Page currentPage = GetCurrentPage();

		if(currentPage != null)
		{
			currentPage.ActiveUpdate(Time.deltaTime);
		}
	}

	private SGGUI.Page GetCurrentPage()
	{
		if(_pages != null)
		{
			int index = (int)_currentPage;
			if(index >= 0 && index < _pages.Length)
			{
				return _pages[index];
			}
		}
		return null;
	}

	private void InputNormal()
	{		
		if(Input.GetMouseButton(0))
		{
			if(!_wasTouched)
			{
				_wasTouched = true;
				_lastTouch = Input.mousePosition;
			
				_gui.TouchDown(_lastTouch.x, Screen.height - _lastTouch.y);
				

			}
			else
			{
				_lastTouch =  Input.mousePosition;;

				_gui.TouchMove(_lastTouch.x, Screen.height -_lastTouch.y);
			}
		}
		else
		{
			if(_wasTouched)
			{
				_wasTouched = false;
				_gui.TouchUp(_lastTouch.x, Screen.height - _lastTouch.y);
			}
		}
	}

	void OnGUI()
	{		
		if(_gui != null)
		{
			if(currentCamera != null)
			{
				// determine the game window's current aspect ratio
				float windowaspect = (float)Screen.width / (float)Screen.height;
				
				// current viewport height should be scaled by this amount
				float scaleheight = windowaspect / SGGUI.GUIScreen.REF_ASPECTRATIO;

				Rect rect = new Rect(0.0f, 0.0f, 0.0f, 0.0f);//cam.rect;
				// if scaled height is less than current height, add letterbox
				if (scaleheight < 1.0f)
				{
					rect.width = 1.0f;
					rect.height = scaleheight;
					rect.x = 0;
					rect.y = (1.0f - scaleheight) / 2.0f;
					
					//cam.rect = rect;
					
					//SINGLE_PIXEL_SIZE = 1.0f/scaleheight;
				}
				else // add pillarbox
				{
					float scalewidth = 1.0f / scaleheight;
					
					
					
					rect.width = scalewidth;
					rect.height = 1.0f;
					rect.x = (1.0f - scalewidth) / 2.0f;
					rect.y = 0;
					
					//cam.rect = rect;
					
					//SINGLE_PIXEL_SIZE = 1.0f/scalewidth;
				}

				_screenWidth = rect.width * Screen.width;
				_screenHeight = rect.height * Screen.height;
			}

			_gui.Render(_screenWidth, _screenHeight, Screen.width, Screen.height);

		}


		_vignetteAlphaCurrent = Mathf.Lerp(_vignetteAlphaCurrent, _vignetteAlphaDesired, 0.1f);
		/*if(vignetteTexture != null)
		{
			GUI.color = new Color(1.0f, 1.0f, 1.0f, _vignetteAlphaCurrent);
			GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), vignetteTexture);
		}*/
	}
}
