﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public interface OnlineImageMonitor
{
	void OnFirstImage();
	void OnAllImages();
	void OnError(int error);
}


public class PaintingProviderOnline : PaintingProvider {
	
	public Europeana europeanaAccess = null;

	private List<PaintingProvider.PaintingInfo> _paintings = new List<PaintingInfo>();
	private int _currentPaintingIndex = -1;
	private int _currentOffset = 0;

	private EuropeanaRequest _currentRequest = null;
	private OnlineImageMonitor _currentMonitor = null;

	private string _searchTerms = null;

	private bool _hasErrored = false;

	// Use this for initialization
	void Start () {
	
		//GetPaintings("beaver");

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	

	public bool DownloadPaintings(string searchTerms, OnlineImageMonitor monitor)
	{
		_currentPaintingIndex = -1;
		_currentMonitor = null;
		_hasErrored = false;
		if(_currentRequest != null)
		{
			_currentRequest.Cancel();
			_currentRequest = null;
		}
		_currentMonitor = monitor;

		_currentOffset = 1;

		if(_paintings != null)
		{
			_paintings.Clear();
		}
		else
		{
			_paintings = new List<PaintingInfo>();
		}

		_searchTerms = searchTerms;

		return GetNextPainting();
	}

	private bool GetNextPainting()
	{
		if(_searchTerms != null && _searchTerms.Length > 0 && europeanaAccess != null)
		{
			_currentRequest = europeanaAccess.GetImages(new Dictionary<string, string>() {
				{ Europeana.OPTION_SEARCH, _searchTerms + " painting"},
				{ Europeana.OPTION_COUNT, "1" },
				{ Europeana.OPTION_OFFSET, _currentOffset.ToString() },
				{ Europeana.OPTION_WIDTH, "1024" },
				{ Europeana.OPTION_HEIGHT, "768" },
				{ Europeana.OPTION_KEEP_ASPECT, Europeana.VALUE_TRUE },
				{ Europeana.OPTION_POWER_OF_2, Europeana.VALUE_TRUE },
				{ Europeana.OPTION_CROP_PERCENTAGE, "2" },
				{ Europeana.OPTION_TEXTURE_SIZE, "1024" },
				{ Europeana.OPTION_MIN_WIDTH, "400" },
				{ Europeana.OPTION_MIN_HEIGHT, "300" },
				{ Europeana.OPTION_VERBOSE_LOGGING, "true" }
			}, image => {
				
				lock(_paintings)
				{
					Debug.Log("Retrieved image: "+image.Id+" index: "+image.Index);
					
					if(_paintings == null)
					{
						_paintings = new List<PaintingProvider.PaintingInfo>();
					}
					
					PaintingProvider.PaintingInfo info = new PaintingProvider.PaintingInfo();
					info.texture = image.TextureData;
					info.title = image.Title;
					info.author = image.Credit;
					info.source = image.Provider;
					
					_paintings.Add(info);
				}
				
				if(_currentMonitor != null)
				{
					_currentMonitor.OnFirstImage();
				}
				
			}
			, images => {
				Debug.Log("Finished retrieving images: "+images.Count);
				
				if(_currentMonitor != null)
				{
					_currentMonitor.OnAllImages();
				}
			}, error => {
				europeanaAccess.PrintError(error);
				_hasErrored = true;
				if(_currentMonitor != null)
				{
					_currentMonitor.OnError(error);
				}
			});
			_currentOffset++;
			return true;
		}

		return false;
	}


	public override PaintingInfo GetNewRandomPainting()
	{
		PaintingInfo info = null;

		lock(_paintings)
		{
			if(_paintings != null && _paintings.Count > 0)
			{
				_currentPaintingIndex = (_currentPaintingIndex + 1) % _paintings.Count;

				info = _paintings[_currentPaintingIndex];

				if(!_hasErrored)
				{
					GetNextPainting();
				}
			}
		}

		return info;
	}

	public override int GetPaintingCount()
	{
		int count = 0;
		lock(_paintings)
		{
			count = (_paintings != null ? _paintings.Count : 0);
		}
		return count;
	}

	public override PaintingInfo GetPaintingByIndex(int index)
	{
		PaintingInfo info = null;

		lock(_paintings)
		{
			info = ((_paintings != null && index >= 0 && index < _paintings.Count) ? _paintings[index] : null);
		}
		return info;
	}

	public override bool ReadyForUse()
	{
		bool ready = false;
		lock(_paintings)
		{
			ready = (_paintings != null && _paintings.Count > 0);
		}
		return ready;
	}
}
