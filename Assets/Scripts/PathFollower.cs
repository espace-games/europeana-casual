﻿using UnityEngine;
using System.Collections;

public delegate void OnPathComplete();

public class PathFollower : MonoBehaviour {

	public static float DEFAULT_LOOK_LERP = 0.05f;
	public SplinePath _path = null;
	public float speed = 50.0f;
	public bool objectFacesbackwards = false;
	public bool matchUp = false;
	public Transform overrideLook = null;
	public float lookLerp = DEFAULT_LOOK_LERP;

	private float _spiralCurrent = 0.0f;
	private float _spiralRotation = 0.0f;

	private float _progression = 0.0f;

	private bool _move = false;


	private OnPathComplete _delegateOnPathComplete = null;
	
	public OnPathComplete DelegateOnPathComplete
	{
		set { _delegateOnPathComplete = value; }
	}

	public void Move(bool move)
	{
		_move = move;
	}

	public void Spiral(float spiral)
	{
		_spiralCurrent = spiral;

		_spiralRotation = 0.0f;

	}

	public void ResetProgression()
	{
		_progression = 0.0f;
		UpdatePositionBasedOnProgression(true);
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {



		_spiralRotation += _spiralCurrent * Time.deltaTime;

		if(_path != null && _move)
		{

			_progression += (objectFacesbackwards ? -speed : speed) * Time.deltaTime;
			
			
			bool wrapped = false;
			while(_progression >= _path.PathLength)
			{
				_progression -= _path.PathLength;
				wrapped = true;
			}
			while (_progression < 0.0f)
			{
				_progression += _path.PathLength;
				wrapped = true;
			}

			if(wrapped && _delegateOnPathComplete != null)
			{
				_delegateOnPathComplete();
			}

			UpdatePositionBasedOnProgression((!_path.loop && wrapped));
		}
	}

	private void UpdatePositionBasedOnProgression(bool immediate)
	{
		SGPath.PathPoint point = _path.PathToFollow.GetPositionByProgress(_progression);
		this.transform.position = point.Position;


		SGPath.PathPoint pointNext = _path.PathToFollow.GetPositionByProgress(_progression + 1.0f);


		Vector3 up = Vector3.up;
		if(matchUp)
		{
			up = point.Up;
		}

		Vector3 lookTarget = (overrideLook == null) ? pointNext.Position : overrideLook.position;

		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation((lookTarget - this.transform.position).normalized, up) * Quaternion.AngleAxis(_spiralRotation, up), immediate ? 1.0f : lookLerp);
	}
}
