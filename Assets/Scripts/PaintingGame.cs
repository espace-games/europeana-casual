﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PaintingGame : MonoBehaviour {

	public static bool QUICK_ADVANCE = false;

	private enum eState
	{
		Inactive = 0,
		ChoosingStart,
		PaintingLine,
		CleaningSection,
		FinishingUp,
		Results,
	}

	private class MessedUpAreaWrapper
	{
		public GameObject obj;
		private float messedUpAmount = 0.0f;
		private float volume = 0.0f;

		public float CalculateMessedUpAmount()
		{
			return volume * messedUpAmount;
		}


		public MessedUpAreaWrapper(Box box, float alpha, PaintingGame parent)
		{
			
			if(box != null && parent != null)
			{
				Vector2 center = box.Center;
				
				obj = new GameObject();
				obj.name = "MessedUpArea";
				obj.transform.parent = parent.transform;
				obj.transform.localPosition = new Vector3(center.x, 0.01f, center.y);
				MeshFilter meshFilter = obj.AddComponent<MeshFilter>();
				MeshRenderer meshRenderer = obj.AddComponent<MeshRenderer>();
				meshFilter.mesh = parent.GenerateMesh(box.Width, box.Height, center.x, center.y);
				meshRenderer.material = parent.messedUpMaterial;
				meshRenderer.material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, alpha));
				//meshRenderer.material.SetColor("_Color", Color.red);

				messedUpAmount = alpha;
				volume = box.Width * box.Height;
			}
		}
	}

	private class CleaningPatchWrapper
	{
		private GameObject _dust = null;
		private GameObject _damage = null;
		private float _damageAmount = 0.0f;

		public CleaningPatchWrapper(GameObject dust, GameObject damage, float damageAmount)
		{
			_dust = dust;
			_damage = damage;
			_damageAmount = damageAmount;
		}

		public void SetAlpha(float alpha)
		{
			Color colour = new Color(1.0f, 1.0f, 1.0f, alpha);

			if(_dust != null)
			{
				_dust.renderer.material.SetColor("_Color", colour);
			}

			if(_damage != null)
			{
				colour = new Color(1.0f, 1.0f, 1.0f, Mathf.Max(_damageAmount, alpha));

				_damage.renderer.material.SetColor("_Color", colour);
			}
		}

		public void DestroyGameObject()
		{
			if(_dust != null)
			{
				GameObject.Destroy(_dust);
				_dust = null;
			}

			if(_damage != null)
			{
				GameObject.Destroy(_damage);
				_damage = null;
			}
		}
	}

	private static float PAINTBRUSH_HEIGHT_OFFSET = 0.1f;
	private static float PAINT_LINE_SPEED = 0.25f;

	public static float MIN_SLICE_SIZE = 0.05f;
	public static float MIN_EDGE_SIZE = 0.2f;



	
	public float paintingWidth = 1.0f;
	public float paintingHeight = 1.0f;
	public Brush paintbrush = null;
	public Camera gameCamera = null;
	public Material dustMaterial = null;
	public Material messedUpMaterial = null;
	public Material lineMaterial = null;

	public Vector3 _mouseScreenPosition = Vector3.zero;
	public Vector3 _mouseWorldPosition = Vector3.zero;
	
	public Renderer canvasRenderer = null;

	public Renderer[] wallArt = null;
	
	private Box _box = null;
	private Edge _nearestEdge = null;
	private Edge _oppositeEdge = null;

	private eState _state = eState.ChoosingStart;

	private Vector3[] _cleaningPath = null;
	private CleaningPatchWrapper[] _cleaningPatches = null;
	private int _currentCleaningIndex = 0;
	private Box _beingCleaned = null;

	private float _lastFailureAmount = 0.0f;

	private List<MessedUpAreaWrapper> _messedUpAreas = null;

	private float _cameraSpeed = 0.0f;
	private float _cameraZoom = 0.5f;

	private PaintingProvider.PaintingInfo _currentPainting = null;

	private Line _currentLine = null;

	private float _currentLineDistance = 0.0f;
	private float _currentBrushSpeed = 0.0f;
	private float _timeForLine = 0.0f;
	private float _clickCountRequired = 1.0f;

	private void ChangeLine(Line line)
	{
		if(_currentLine != null)
		{
			_currentLine.DestroyGameObject();
			_currentLine = null;
		}
		_currentLine = line;
	}

	/*private LineWrapper GetCurrentLine()
	{
		return ((_lines != null && _lines.Count > 0) ? _lines[_lines.Count-1] : null);
	}

	private LineWrapper AddLine(LineWrapper line)
	{
		if(line != null)
		{
			if(_lines == null)
			{
				_lines = new List<LineWrapper>();
			}

			_lines.Add(line);
		}
		return line;
	}*/

	public PaintingProvider.PaintingInfo CurrentPainting
	{
		get { return _currentPainting; }
	}

	public static float BrushSpeed()
	{
		return (0.1f * (float)GameControl.GetCurrentLevel());
	}

	private void SetCameraZoom(float zoom, bool immediate)
	{
		_cameraZoom = zoom;
		_cameraSpeed = 0.0f;
		if(immediate && gameCamera != null)
		{
			gameCamera.orthographicSize = _cameraZoom;
		}
	}

	private void DestroyAllPatches()
	{
		if(_cleaningPatches != null)
		{
			for(int i = 0; i < _cleaningPatches.Length; ++i)
			{
				if(_cleaningPatches[i] != null)
				{
					_cleaningPatches[i].DestroyGameObject();
					_cleaningPatches[i] = null;
				}
			}
		}
	}

	private void DestroyAllMessedUpAreas()
	{
		if(_messedUpAreas != null)
		{
			for(int i = 0; i < _messedUpAreas.Count; ++i)
			{
				if(_messedUpAreas[i] != null)
				{
					if(_messedUpAreas[i].obj != null)
					{
						GameObject.Destroy(_messedUpAreas[i].obj);
					}
					_messedUpAreas[i] = null;
				}
			}

			_messedUpAreas.Clear();
		}
	}

	private void AddMessedUpArea(MessedUpAreaWrapper obj)
	{
		if(obj != null)
		{
			if(_messedUpAreas == null)
			{
				_messedUpAreas = new List<MessedUpAreaWrapper>();
			}

			_messedUpAreas.Add(obj);
		}
	}

	public void StartGame()
	{
		Reset ();
		_state = eState.ChoosingStart;
		SetCameraZoom(0.5f, true);
		if(paintbrush != null)
		{
			paintbrush.Show();
		}
	}

	public void StartResults()
	{
		float totalArea = (paintingWidth * paintingHeight) - _box.Area;
		
		float messedUpArea = 0.0f;
		
		if(_messedUpAreas != null)
		{
			for(int i = 0; i < _messedUpAreas.Count; ++ i)
			{
				if(_messedUpAreas[i] != null)
				{
					messedUpArea += _messedUpAreas[i].CalculateMessedUpAmount();
				}
			}
		}
		
		float successPercentage = 1.0f - (messedUpArea/totalArea);
		GameControl.SetLastSuccessPercentage(successPercentage);
		
		SetCameraZoom(0.28f, false);

		GameUI gui = GameControl.GetGUI();

		if(gui != null)
		{
			SGGUI.GamePage page = (SGGUI.GamePage)gui.GetPage(GameUI.ePages.Game);
			if(page != null)
			{
				page.ShowResults(0, 0, 0, false);
			}
			//gui.ShowPage(GameUI.ePages.Results);
		}
		_state = eState.Results;
	}

	public void ContinueGame()
	{
		Reset ();
		GameControl.AdvanceLevel();
		_state = eState.ChoosingStart;
		SetCameraZoom(0.5f, false);


		GameUI gui = GameControl.GetGUI();
		if(gui != null)
		{
			SGGUI.GamePage page = (SGGUI.GamePage)gui.GetPage(GameUI.ePages.Game);
			if(page != null)
			{
				page.HideResults(false);
			}

			//gui.ShowPage(GameUI.ePages.Game);
		}

		if(paintbrush != null)
		{
			paintbrush.Show();
		}
	}

	public void EndGame()
	{
		GameControl.SetState(GameControl.eState.OnTitleScreen);
	}

	public void Reset()
	{

		/*if(_edges != null)
		{
			_edges.Clear();
		}



		_edges = new List<Edge>();

		_edges.Add(new Edge(new Vector2(0.0f, paintingHeight * 0.5f), paintingWidth, Edge.eEdgeLocation.Top));
		_edges.Add(new Edge(new Vector2(0.0f, -paintingHeight * 0.5f), paintingWidth, Edge.eEdgeLocation.Bottom));

		_edges.Add(new Edge(new Vector2(paintingWidth * 0.5f, 0.0f), paintingHeight, Edge.eEdgeLocation.Right));
		_edges.Add(new Edge(new Vector2(-paintingWidth * 0.5f, 0.0f), paintingHeight, Edge.eEdgeLocation.Left));*/

		if(canvasRenderer != null)
		{
			PaintingProvider.PaintingInfo paintingInfo = GameControl.GetRandomPainting(false);

			if(paintingInfo != null && paintingInfo.texture != null)
			{
				_currentPainting = paintingInfo;
				canvasRenderer.material.mainTexture = _currentPainting.texture;
			}
		}

		if(_beingCleaned != null)
		{
			_beingCleaned.DestroyDustLayer();
			_beingCleaned = null;
		}

		DestroyAllPatches();
		DestroyAllMessedUpAreas();
		_cleaningPath = null;
		_currentCleaningIndex = 0;


		ChangeBox(new Box(new Vector2(-paintingWidth * 0.5f, paintingHeight * 0.5f), new Vector2(paintingWidth * 0.5f, -paintingHeight * 0.5f)));
		_state = eState.Inactive;
		if(paintbrush != null)
		{
			paintbrush.Hide();
		}

		ChangeLine(null);
		paintbrush.PlayBrushLong(false);
		paintbrush.DustKickup(false);

		GameUI gui = GameControl.GetGUI();
		if(gui != null)
		{
			gui.StopBrushLineMiniGame();

			SGGUI.GamePage page = (SGGUI.GamePage)gui.GetPage(GameUI.ePages.Game);
			if(page != null)
			{
				page.HideResults(true);
			}
		}
	}

	// Use this for initialization
	void Start () {
	
		Reset ();

		if(wallArt != null)
		{
			for(int i = 0; i < wallArt.Length; ++i)
			{
				if(wallArt[i] != null)
				{
					PaintingProvider.PaintingInfo info = GameControl.GetRandomPainting(true);
					if(info != null && info.texture != null)
					{
						wallArt[i].material.mainTexture = info.texture;
					}
				}
			}
		}
	}

	private float Distance1D(float a, float b)
	{
		return Mathf.Sqrt((b-a) * (b-a));
	}

	private float PaintBrushHeight()
	{
		return this.transform.position.y + PAINTBRUSH_HEIGHT_OFFSET;
	}

	private void Update_ChoosingStart()
	{
		_nearestEdge = _oppositeEdge = null;
		
		if(paintbrush != null && gameCamera != null && Input.mousePresent && _box != null)
		{
			_mouseScreenPosition = Input.mousePosition;
			
			_mouseWorldPosition = gameCamera.ScreenToWorldPoint(_mouseScreenPosition);
			
			_nearestEdge = _box.GetNearestEdge(_mouseWorldPosition);
			
			if(_nearestEdge != null)
			{
				paintbrush.transform.position = _nearestEdge.GetIntersectionPointThreeDee(_mouseWorldPosition, PaintBrushHeight(), MIN_SLICE_SIZE);
				
				paintbrush.SetModelRotation(_nearestEdge.Location, false, false);

				if(Input.GetMouseButtonDown(0))
				{
					_oppositeEdge = _box.GetOppositeEdge(_nearestEdge);
					if(_oppositeEdge != null)
					{
						Vector3 startPos = paintbrush.transform.position;
						Vector3 endPos = _oppositeEdge.GetIntersectionPointThreeDee(paintbrush.transform.position, PaintBrushHeight(), MIN_SLICE_SIZE);

						_currentLineDistance = Vector3.Distance(startPos, endPos);



						_currentBrushSpeed = BrushSpeed();

						_timeForLine = (_currentLineDistance/_currentBrushSpeed);

						_clickCountRequired = GameControl.ClickCountRequired(_currentLineDistance);

						float minTime = 1.0f;//GameControl.TIME_BEST * _clickCountRequired;

						if(_timeForLine < minTime)
						{
							_currentBrushSpeed = _currentLineDistance/minTime;
							_timeForLine = _currentLineDistance/_currentBrushSpeed;
						}


						_state = eState.PaintingLine;
						GameUI gui = GameControl.GetGUI();

						paintbrush.PlayBrushLong(true);

						paintbrush.SetModelRotation(_oppositeEdge.Location, false, true);

						ChangeLine(new Line(this, startPos, endPos, false));
						if(gui != null)
						{
							gui.StartBrushLineMiniGame(paintbrush);
						}
					}
				}
			}
		}
	}

	private float NormalisedPointOnLine(float start, float end, float point)
	{
		float length = end - start;
		return (point-start) /length;
	}

	private void Update_PaintingLine()
	{
		if(paintbrush != null && _oppositeEdge != null)
		{
			Vector3 intersectionPoint = _oppositeEdge.GetIntersectionPointThreeDee(paintbrush.transform.position, PaintBrushHeight(), MIN_SLICE_SIZE);

			Vector3 direction = intersectionPoint - paintbrush.transform.position;
			direction.Normalize();

			float movement = _currentBrushSpeed * Time.deltaTime;

			if(Vector3.Distance(paintbrush.transform.position, intersectionPoint) <= movement)
			{
				paintbrush.transform.position = intersectionPoint;

				Box[] newBoxes = null;

				if(_oppositeEdge.IsHorizontal())
				{
					float splitPosition = NormalisedPointOnLine(_oppositeEdge.PointA.x, _oppositeEdge.PointB.x, intersectionPoint.x);

					newBoxes = _box.SplitVertically(splitPosition);
				}
				else
				{
					float splitPosition = 1.0f - NormalisedPointOnLine(_oppositeEdge.PointA.y, _oppositeEdge.PointB.y, intersectionPoint.z);
					newBoxes = _box.SplitHorizontally(splitPosition);
				}

				if(newBoxes != null && newBoxes.Length == 2 && newBoxes[0] != null && newBoxes[1] != null)
				{
					Box nextBox = (newBoxes[0].Area > newBoxes[1].Area ? newBoxes[0] : newBoxes[1]);
					_beingCleaned = ((newBoxes[0] == nextBox) ? newBoxes[1] : newBoxes[0]);

					SGGUI.PaintLineMiniGame.ClickResults results = null;

					GameUI gui = GameControl.GetGUI();
					if(gui != null)
					{
						results = gui.StopBrushLineMiniGame();
					}

					float distanceTravelled = _oppositeEdge.GetDistance(_nearestEdge);

					_lastFailureAmount = 0.0f;

					if(distanceTravelled > 0.0f && results != null)
					{
						float clickValueRequired = (float)_clickCountRequired;
						float clickValueAchieved = ((float)results.BadCount * 0.5f) + (float)(results.OKCount * 1.0f) + (float)(results.GoodCount * 1.5f);

						Debug.Log("Click Valye Required: " + clickValueRequired);
						Debug.Log("Clicks Value Achieved: " + clickValueAchieved);

						_lastFailureAmount = 1.0f - Mathf.Clamp((clickValueAchieved/clickValueRequired), 0.0f, 1.0f);
					}

					ChangeBox(nextBox);
					GenerateCleaningPath(_beingCleaned);

					_currentCleaningIndex = 0;
					_state = eState.CleaningSection;


					nextBox.Final = (nextBox.Width <= MIN_EDGE_SIZE || nextBox.Height <= MIN_EDGE_SIZE);

					paintbrush.PlayBrushLong(false);
					ChangeLine(null);
				}
				else
				{
					_state = eState.ChoosingStart;
					GameUI gui = GameControl.GetGUI();
					if(gui != null)
					{
						gui.StopBrushLineMiniGame();
					}
					paintbrush.PlayBrushLong(false);
					ChangeLine(null);
				}
			}
			else
			{
				paintbrush.transform.position += (direction * movement);

				if(_currentLine != null)
				{
					_currentLine.UpdateMesh(paintbrush.transform.position);
				}
			}
		}
	}

	private void Update_CleaningSection()
	{
		if(_cleaningPath != null && paintbrush != null && _currentCleaningIndex >= 0 && _currentCleaningIndex < _cleaningPath.Length)
		{
			if(_currentCleaningIndex == 0)
			{
				paintbrush.transform.position = _cleaningPath[0];
				_currentCleaningIndex++;
				paintbrush.DustKickup(true);
				paintbrush.PlayBrushStrokeAudio();
			}

			float movement = 2.0f * Time.deltaTime;

			if(Vector3.Distance(_cleaningPath[_currentCleaningIndex], paintbrush.transform.position) <= movement)
			{
				paintbrush.transform.position = _cleaningPath[_currentCleaningIndex];
			
				_currentCleaningIndex++;

				paintbrush.TweakModelRotation(false);

				if(_currentCleaningIndex >= _cleaningPath.Length)
				{
					_cleaningPath = null;
					_currentCleaningIndex = 0;

					paintbrush.DustKickup(false);

					DestroyAllPatches();
					Debug.Log("failure percent: " + _lastFailureAmount);
					if(_lastFailureAmount > 0.0f)
					{
						AddMessedUpArea(new MessedUpAreaWrapper(_beingCleaned, _lastFailureAmount, this));
					}

					if(_beingCleaned != null)
					{
						_beingCleaned.DestroyDustLayer();
						_beingCleaned = null;
					}

					if(_box == null || _box.Final)
					{
						paintbrush.Hide();
						_state = eState.FinishingUp;
					}
					else
					{
						_state = eState.ChoosingStart;

						if(QUICK_ADVANCE)
						{
							//Use these for quick on cleaning patch advance for testing
							paintbrush.Hide();
							_state = eState.FinishingUp;
						}
					}

				}
				else
				{
					paintbrush.PlayBrushStrokeAudio();
				}
			}
			else
			{
				Vector3 direction = (_cleaningPath[_currentCleaningIndex] - paintbrush.transform.position).normalized;
				paintbrush.transform.position += direction * movement;

				if(_cleaningPatches != null)
				{
					int patchIndex = _currentCleaningIndex-1;
					if(patchIndex >= 0 && patchIndex < _cleaningPatches.Length)
					{
						if(_cleaningPatches[patchIndex] != null)
						{
							//Todo Set Alpha based on distance from destination
							_cleaningPatches[patchIndex].SetAlpha((Vector3.Distance(paintbrush.transform.position, _cleaningPath[_currentCleaningIndex])/Vector3.Distance(_cleaningPath[_currentCleaningIndex-1], _cleaningPath[_currentCleaningIndex])));
						}
					}
				}
			}
		}
	}

	private void Update_FinishingUp()
	{
		if(_box == null || _box.FadeOut(Time.deltaTime))
		{
			/*Texture2D picture = grabPhoto();

			if(pictureTest != null)
			{
				pictureTest.renderer.material.mainTexture = picture;
			}*/

			//Reset();

			//CaptureTexture();



			StartResults();
		}
	}

	public Texture2D grabPhoto()
	{
		/*
		// capture the virtuCam and save it as a square PNG.
		int sqr = 512;
		
		virtuCamera.camera.aspect = 1.0f;
		// recall that the height is now the "actual" size from now on
		// the .aspect property is very tricky in Unity, and bizarrely is NOT shown in the editor
		// the editor will still incorrectly show the frustrum being screen-shaped
		
		RenderTexture tempRT = new RenderTexture(sqr,sqr, 24 );
		// the "24" can be 0,16,24 or formats like RenderTextureFormat.Default, ARGB32 etc.
		
		virtuCamera.camera.targetTexture = tempRT;
		virtuCamera.camera.Render();
		
		RenderTexture.active = tempRT;
		Texture2D virtualPhoto = new Texture2D(sqr,sqr, TextureFormat.RGB24, false);
		// false, meaning no need for mipmaps
		virtualPhoto.ReadPixels( new Rect(0, 0, sqr,sqr), 0, 0); // you get the center section
		
		RenderTexture.active = null; // "just in case" 
		virtuCamera.camera.targetTexture = null;
		//////Destroy(tempRT); - tricky on android and other platforms, take care
		
		byte[] bytes;
		bytes = virtualPhoto.EncodeToPNG();
		
		System.IO.File.WriteAllBytes( OurTempSquareImageLocation(), bytes );
		// virtualCam.SetActive(false); ... not necesssary but take care
		
		// now use the image somehow...
		YourOngoingRoutine( OurTempSquareImageLocation() );*/

		Texture2D virtualPhoto = null;

		if(gameCamera != null)
		{
			int texWidth = 512;
			int texHeight = 256;
			RenderTexture tempRT = new RenderTexture(texWidth, texHeight, 24 );
			gameCamera.targetTexture = tempRT;
			gameCamera.Render();

			RenderTexture.active = tempRT;
			virtualPhoto = new Texture2D(texWidth,texHeight, TextureFormat.RGB24, false); // false, meaning no need for mipmaps
			virtualPhoto.ReadPixels( new Rect(0, 0, texWidth,texHeight), 0, 0); // you get the center section
			RenderTexture.active = null; // "just in case"
			Destroy(tempRT);


			gameCamera.targetTexture = null;
		}

		return virtualPhoto;
	}
	private string OurTempSquareImageLocation()
	{
		string r = Application.persistentDataPath + "/p.png";
		return r;
	}
	
	private void Update_Results()
	{
		/*if(Input.GetMouseButtonDown(0))
		{
			//Reset ();
			//GameControl.SetState(GameControl.eState.OnTitleScreen);
		}*/
	}

	private void GenerateCleaningPath(Box box)
	{
		if(box != null)
		{
			/*Debug.Log("failure percent: " + _lastFailureAmount);
			if(_lastFailureAmount > 0.0f)
			{
				AddMessedUpArea(new MessedUpAreaWrapper(box, _lastFailureAmount, this));
			}*/

			List<Vector3> path = new List<Vector3>();
			List<CleaningPatchWrapper> cleaningPatches = new List<CleaningPatchWrapper>();



			Vector2 lastPosition = box.TopLeft;
			Vector2 newPosition = lastPosition;

			path.Add(Edge.TwoDeeToThreeDee(lastPosition, PaintBrushHeight()));

			float strokeSize = 0.1f;

			if(box.Height > box.Width)
			{
				if(paintbrush != null)
				{
					paintbrush.SetModelRotation(Edge.eEdgeLocation.Bottom, false, true);
				}
				while(Distance1D(lastPosition.y, box.BottomRight.y) > strokeSize)
				{
					newPosition.y = lastPosition.y - strokeSize;
					newPosition.x = ((lastPosition.x == box.TopLeft.x) ? box.BottomRight.x : box.TopLeft.x);

					path.Add(Edge.TwoDeeToThreeDee(newPosition, PaintBrushHeight()));
					cleaningPatches.Add(CreateCleaningPatch(lastPosition, newPosition, cleaningPatches.Count));

					lastPosition = newPosition;
				}

				if(box.BottomRight.x == lastPosition.x)
				{
					newPosition.y = box.BottomRight.y;
					newPosition.x = box.TopLeft.x;
				}
				else
				{
					newPosition = box.BottomRight;
				}
				
				path.Add(Edge.TwoDeeToThreeDee(newPosition, PaintBrushHeight()));
				cleaningPatches.Add(CreateCleaningPatch(lastPosition, newPosition, cleaningPatches.Count));
			}
			else
			{
				if(paintbrush != null)
				{
					paintbrush.SetModelRotation(Edge.eEdgeLocation.Right, false, true);
				}
				while(Distance1D(lastPosition.x, box.BottomRight.x) > strokeSize)
				{
					newPosition.x = lastPosition.x + strokeSize;
					newPosition.y = ((lastPosition.y == box.TopLeft.y) ? box.BottomRight.y : box.TopLeft.y);

					path.Add(Edge.TwoDeeToThreeDee(newPosition, PaintBrushHeight()));
					cleaningPatches.Add(CreateCleaningPatch(lastPosition, newPosition, cleaningPatches.Count));

					lastPosition = newPosition;
				}

				if(box.BottomRight.y == lastPosition.y)
				{
					newPosition.x = box.BottomRight.x;
					newPosition.y = box.TopLeft.y;
				}
				else
				{
					newPosition = box.BottomRight;
				}

				path.Add(Edge.TwoDeeToThreeDee(newPosition, PaintBrushHeight()));
				cleaningPatches.Add(CreateCleaningPatch(lastPosition, newPosition, cleaningPatches.Count));
			}


			_cleaningPath = path.ToArray();
			DestroyAllPatches();
			_cleaningPatches = cleaningPatches.ToArray();
		}
	}


	private void ChangeBox(Box newBox)
	{
		if(_box != null)
		{
			_box.DestroyDustLayer();
			_box.DestroyLines();
			_box = null;
		}

		_box = newBox;
		CreateDustLayer(_box);
		if(_box != null)
		{
			_box.GenerateLines(this);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(gameCamera != null)
		{
			if(gameCamera.orthographicSize != _cameraZoom)
			{
				_cameraSpeed += Time.deltaTime * 0.02f;
				if(gameCamera.orthographicSize < _cameraZoom)
				{
					gameCamera.orthographicSize = Mathf.Min (gameCamera.orthographicSize + _cameraSpeed, _cameraZoom);
				}
				else if(gameCamera.orthographicSize > _cameraZoom)
				{
					gameCamera.orthographicSize = Mathf.Max (gameCamera.orthographicSize - _cameraSpeed, _cameraZoom);
				}
			}
		}
		switch(_state)
		{
		case eState.ChoosingStart:
			Update_ChoosingStart();
			break;
		case eState.PaintingLine:
			Update_PaintingLine();
			break;
		case eState.CleaningSection:
			Update_CleaningSection();
			break;
		case eState.FinishingUp:
			Update_FinishingUp();
			break;
		case eState.Results:
			Update_Results();
			break;
		default:
			break;
		}


	}
	
	void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;

		if(_box != null)
		{
			for(int i = 0; i < _box.EdgeCount; ++i)
			{
				Edge edge = _box.GetEdge(i);
				if(edge != null)
				{
					Vector3 pointA = edge.GetThreeDeePointA(PaintBrushHeight());
					Vector3 pointB = edge.GetThreeDeePointB(PaintBrushHeight());
					
					Gizmos.DrawLine(pointA, pointB);
				}
			}
		}

		if(_cleaningPath != null && _cleaningPath.Length >= 2)
		{
			for(int i = 0; i < _cleaningPath.Length - 1; ++i)
			{
				Gizmos.DrawLine(_cleaningPath[i], _cleaningPath[i + 1]);
			}
		}
	}

	private CleaningPatchWrapper CreateCleaningPatch(Vector2 posA, Vector2 posB, int index)
	{
		Vector2 center = (posA + posB) * 0.5f;

		MeshFilter meshFilter = null;
		MeshRenderer meshRenderer = null;
		Mesh mesh = GenerateMesh(Distance1D(posA.x, posB.x), Distance1D(posA.y, posB.y), center.x, center.y);

		GameObject objDust = new GameObject();
		objDust.name = "DustPatch " + index;
		objDust.transform.parent = this.transform;
		objDust.transform.localPosition = new Vector3(center.x, 0.01f, center.y);
		meshFilter = objDust.AddComponent<MeshFilter>();
		meshRenderer = objDust.AddComponent<MeshRenderer>();
		meshFilter.mesh = mesh;
		meshRenderer.material = dustMaterial;


		GameObject objDamage = new GameObject();
		objDamage.name = "DamagePatch " + index;
		objDamage.transform.parent = this.transform;
		objDamage.transform.localPosition = new Vector3(center.x, 0.01f, center.y);
		meshFilter = objDamage.AddComponent<MeshFilter>();
		meshRenderer = objDamage.AddComponent<MeshRenderer>();
		meshFilter.mesh = mesh;
		meshRenderer.material = messedUpMaterial;
		
		return new CleaningPatchWrapper(objDust, objDamage, _lastFailureAmount);
	}

	private GameObject CreateMessedUpArea(Box box, float alpha)
	{
		GameObject obj = null;

		if(box != null)
		{
			Vector2 center = box.Center;
			
			obj = new GameObject();
			obj.name = "MessedUpArea";
			obj.transform.parent = this.transform;
			obj.transform.localPosition = new Vector3(center.x, 0.01f, center.y);
			MeshFilter meshFilter = obj.AddComponent<MeshFilter>();
			MeshRenderer meshRenderer = obj.AddComponent<MeshRenderer>();
			meshFilter.mesh = GenerateMesh(box.Width, box.Height, center.x, center.y);
			meshRenderer.material = messedUpMaterial;
			meshRenderer.material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, alpha));
			//meshRenderer.material.SetColor("_Color", Color.red);
		}
		
		return obj;
	}
	
	
	private void CreateDustLayer(Box box)
	{
		if(box != null)
		{
			MeshFilter meshFilter = null;
			MeshRenderer meshRenderer = null;

			GameObject objDust = new GameObject();
			objDust.name = "DustLayer";
			objDust.transform.parent = this.transform;
			objDust.transform.localPosition = new Vector3(box.Center.x, 0.01f, box.Center.y);
			meshFilter = objDust.AddComponent<MeshFilter>();
			meshRenderer = objDust.AddComponent<MeshRenderer>();

			Mesh mesh = GenerateMesh(box.Width, box.Height, box.Center.x, box.Center.y);

			meshFilter.mesh = mesh;
			meshRenderer.material = dustMaterial;


			GameObject objDirt = new GameObject();
			objDirt.name = "DustLayer";
			objDirt.transform.parent = this.transform;
			objDirt.transform.localPosition = new Vector3(box.Center.x, 0.01f, box.Center.y);
			meshFilter = objDirt.AddComponent<MeshFilter>();
			meshRenderer = objDirt.AddComponent<MeshRenderer>();
			meshFilter.mesh = mesh;
			meshRenderer.material = messedUpMaterial;


			box.SetDustLayer(objDust, objDirt);
		}
	}

	public Mesh GenerateMesh(float width, float height, float xOff, float yOff)
	{
		Mesh mesh = new Mesh();

		Vector3[] vertices = new Vector3[4];
		
		vertices[0] = new Vector3 (-(width* 0.5f), 0.0f, (height * 0.5f));
		vertices[1] = new Vector3 ((width* 0.5f), 0.0f, (height * 0.5f));
		vertices[2] = new Vector3 ((width* 0.5f), 0.0f, -(height * 0.5f));
		vertices[3] = new Vector3 (-(width* 0.5f), 0.0f, -(height * 0.5f));

		int[] triangles = new int[6];
		triangles[0] = 0;
		triangles[1] = 1;
		triangles[2] = 3;
		triangles[3] = 1;
		triangles[4] = 2;
		triangles[5] = 3;

		float u = ((xOff - (width * 0.5f)) + (paintingWidth * 0.5f))/paintingWidth;
		float v = ((yOff - (height * 0.5f)) + (paintingHeight * 0.5f))/paintingHeight;
		float texWidth = width/paintingWidth;
		float texHeight = height/paintingHeight;

		Vector2[] uvs = new Vector2[4];
		uvs[0] = new Vector2 (u, v + texHeight);
		uvs[1] = new Vector2 (u + texWidth, v + texHeight);
		uvs[2] = new Vector2 (u + texWidth, v);
		uvs[3] = new Vector2 (u, v);
		
		mesh.Clear ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.Optimize ();
		mesh.RecalculateNormals();

		return mesh;
	}
}
