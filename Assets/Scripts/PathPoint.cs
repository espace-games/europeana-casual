﻿using UnityEngine;
using System.Collections;

public class PathPoint : MonoBehaviour {

	public static float GIZMO_LENGTH = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawSphere(this.transform.position, 0.5f);


		Gizmos.DrawLine(this.transform.position, this.transform.position + (this.transform.forward * GIZMO_LENGTH));

		Gizmos.color = Color.green;
		Gizmos.DrawLine(this.transform.position, this.transform.position + (this.transform.up * GIZMO_LENGTH));

		Gizmos.color = Color.red;
		Gizmos.DrawLine(this.transform.position, this.transform.position + (this.transform.right * GIZMO_LENGTH));
	}
}
