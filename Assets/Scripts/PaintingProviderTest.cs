﻿using UnityEngine;
using System.Collections;

public class PaintingProviderTest : PaintingProvider {

	public PaintingProvider.PaintingInfo[] paintings = null;
	private int _currentPaintingIndex = 0;
	private bool _initialised = false;

	// Use this for initialization
	void Start () {
	
		Initialise();

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public override PaintingInfo GetNewRandomPainting()
	{
		Initialise();
		if(paintings != null && paintings.Length > 0 && _currentPaintingIndex >= 0 && _currentPaintingIndex < paintings.Length)
		{
			int paintingIndex = _currentPaintingIndex;
			_currentPaintingIndex = (_currentPaintingIndex+1)%paintings.Length;
			return paintings[paintingIndex];
		}
		return null;
	}

	public override int GetPaintingCount()
	{
		Initialise();
		return (paintings != null ? paintings.Length : 0);
	}

	public override PaintingInfo GetPaintingByIndex(int index)
	{
		Initialise();
		return ((paintings != null && index >= 0 && index < paintings.Length) ? paintings[index] : null);
	}

	private void Initialise()
	{
		if(!_initialised)
		{
			//Choose Random Start
			if(paintings != null && paintings.Length > 0)
			{
				_currentPaintingIndex = Random.Range(0, paintings.Length);
			}

			_initialised = true;
		}
	}

	public override bool ReadyForUse()
	{
		return _initialised;
	}
}
