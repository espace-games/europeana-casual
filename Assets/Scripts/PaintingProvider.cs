﻿using UnityEngine;
using System;
using System.Collections;

public abstract class PaintingProvider : MonoBehaviour {

	[System.Serializable]
	public class PaintingInfo
	{
		public Texture2D texture;
		public string title;
		public string author;
		public string source;

		public PaintingInfo()
		{
			texture = null;
			title = null;
			author = null;
			source = null;
		}
	}
	public abstract PaintingInfo GetNewRandomPainting();
	public abstract int GetPaintingCount();
	public abstract PaintingInfo GetPaintingByIndex(int index);
	public abstract bool ReadyForUse();
}
