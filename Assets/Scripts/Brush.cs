﻿using UnityEngine;
using System.Collections;

public class Brush : MonoBehaviour {


	public GameObject model = null;
	public ParticleSystem dustKickup = null;

	private AudioSource[] _brushStrokes = null;

	private AudioSource _brushLong = null;

	private bool _playLineAudio = false;
	private float _timer = 0.0f;

	private Quaternion _rotationDesired = Quaternion.identity;

	private Edge.eEdgeLocation _currentEdgeLocation = Edge.eEdgeLocation.Top;

	public void Show()
	{
		if(model != null)
		{
			model.SetActive(true);
		}
	}

	public void Hide()
	{
		if(model != null)
		{
			model.SetActive(false);
		}
	}

	public void PlayBrushLong(bool play)
	{
		/*_playLineAudio = play;
		if(_playLineAudio && !IsStrokeAudioPlaying())
		{
			PlayBrushStrokeAudio();
		}*/

		_playLineAudio = play;
		if(_brushLong != null && _playLineAudio != _brushLong.isPlaying)
		{
			if(_playLineAudio)
			{
				_brushLong.Play();
				RandomTime();
				PlayBrushStrokeAudio();
			}
			else
			{
				_brushLong.Stop();
			}
		}
	}



	private void RandomTime()
	{
		_timer = Random.Range(1.5f, 3.0f);
	}

	public void PlayBrushStrokeAudio()
	{
		if(_brushStrokes != null)
		{
			for(int i = 0; i < _brushStrokes.Length; ++i)
			{
				if(_brushStrokes[i] != null && !_brushStrokes[i].isPlaying)
				{
					_brushStrokes[i].Play();
					return;
				}
			}
		}
	}

	private bool IsStrokeAudioPlaying()
	{
		if(_brushStrokes != null)
		{
			for(int i = 0; i < _brushStrokes.Length; ++i)
			{
				if(_brushStrokes[i] != null && _brushStrokes[i].isPlaying)
				{
					return true;
				}
			}
		}
		return false;
	}

	// Use this for initialization
	void Start () {
	
		_rotationDesired = this.transform.rotation;

		_brushStrokes = this.GetComponents<AudioSource>();

		_brushLong = (model != null ? model.GetComponent<AudioSource>() : null);

	}
	
	// Update is called once per frame
	void Update () {
		if(_playLineAudio/* && !IsStrokeAudioPlaying()*/)
		{
			_timer -= Time.deltaTime;
			if(_timer <= 0.0f)
			{
				RandomTime();
				PlayBrushStrokeAudio();
			}
		}

		if(model != null)
		{
			model.transform.rotation = Quaternion.Slerp(model.transform.rotation, _rotationDesired, 0.2f);
		}
	}

	public void TweakModelRotation(bool snap)
	{
		SetModelRotation(_currentEdgeLocation, snap, true);
	}

	public void SetModelRotation(Edge.eEdgeLocation location, bool snap, bool random)
	{
		if(model != null)
		{
			_currentEdgeLocation = location;

			float randomAngle = random ? Random.Range(-20.0f, 20.0f) : 0.0f;

			switch(location)
			{
			case Edge.eEdgeLocation.Top:
				_rotationDesired = Quaternion.Euler(90.0f, 180.0f + randomAngle, 0.0f);
				break;
			case Edge.eEdgeLocation.Bottom:
				_rotationDesired = Quaternion.Euler(90.0f, 0.0f + randomAngle, 0.0f);
				break;
			case Edge.eEdgeLocation.Left:
				_rotationDesired = Quaternion.Euler(90.0f, 90.0f + randomAngle, 0.0f);
				break;
			case Edge.eEdgeLocation.Right:
				_rotationDesired = Quaternion.Euler(90.0f, 270.0f + randomAngle, 0.0f);
				break;
			default:
				break;
			}

			if(snap)
			{
				model.transform.rotation = _rotationDesired;
			}
		}
	}

	public void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(this.transform.position, 0.01f);
	}

	public void DustKickup(bool start)
	{
		if(dustKickup != null && start != dustKickup.isPlaying)
		{
			if(start)
			{
				dustKickup.Play();
			}
			else
			{
				dustKickup.Stop();
			}
		}
	}
}
