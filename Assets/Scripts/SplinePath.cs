﻿using UnityEngine;
using System.Collections;

public class SplinePath : MonoBehaviour {

	public bool loop = false;

	private SGPath.Path _path = null;

	
	public float PathLength
	{
		get { return _path != null ? _path.PathLength : 0.0f; }	
	}
	
	public SGPath.Path PathToFollow
	{
		get { return _path; }	
	}
	
	// Use this for initialization
	void Start () {
		
		if(this.transform.childCount > 1)
		{
			/*Transform[] controlPoints = new Transform[this.transform.childCount];
			
			for(int i = 0; i < this.transform.childCount; ++i)
			{
				controlPoints[i] = this.transform.GetChild(i).transform;
			}*/
			
			_path = BuildPath();
		}
		
	}
	
	private SGPath.Path BuildPath()
	{
		if(this.transform.childCount > 1)
		{
			Transform[] controlPoints = new Transform[this.transform.childCount];
			
			for(int i = 0; i < this.transform.childCount; ++i)
			{
				controlPoints[i] = this.transform.GetChild(i).transform;
			}

			SGPath.PathPoint[] path = loop ? SGMath.CatmullRom.SmoothLoop(controlPoints, 100) : SGMath.CatmullRom.SmoothPath(controlPoints, 100);
			return new SGPath.Path(path, loop);
		}
		
		return null;
	}
	
	// Update is called once per frame
	void Update () {
	
		if(_path != null && _path.Loops() != loop)
		{
			_path = BuildPath();
		}
	}
	
	public void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		
		
		SGPath.Path toRender = _path;
		
		if(toRender == null)
		{
			toRender = BuildPath();
		}
		
		if(toRender != null && toRender.PointCount > 1)
		{
			for(int i = 0; i < (loop ? toRender.PointCount : toRender.PointCount-1); ++i)
			{
				//Gizmos.DrawSphere( _path[i], 0.1f);
				Gizmos.DrawLine(toRender.GetPositionByIndex(i).Position, toRender.GetPositionByIndex((i + 1) %toRender.PointCount).Position);
			}
		}
		
		
		
		if(this.transform.childCount > 1)
		{
			for(int i = 0; i < this.transform.childCount; ++i)
			{
				Gizmos.DrawSphere(this.transform.GetChild(i).position, 0.1f);
				//Gizmos.DrawLine(this.transform.GetChild(i).position, this.transform.GetChild((i + 1) %this.transform.childCount).position);
			}
		}
	}
}
