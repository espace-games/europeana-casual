﻿using System;

namespace SGPath
{
	public class PathPoint
	{
		public static PathPoint ZERO = new PathPoint();


		UnityEngine.Vector3 _position = UnityEngine.Vector3.zero;
		float _distance = 0.0f;
		UnityEngine.Vector3 _up = UnityEngine.Vector3.up;
			
		public UnityEngine.Vector3 Position
		{
			get { return _position; }
			set { _position = value; }	
		}
			
		public float Distance
		{
			get { return _distance; }	
		}

		public UnityEngine.Vector3 Up
		{
			get { return _up; }
			set { _up = value; }	
		}
			
		public PathPoint()
		{
				
		}

		public PathPoint(PathPoint point)
		{
			_position = point.Position;
			_distance = point.Distance;
			_up = point.Up;
		}

		public PathPoint(UnityEngine.Vector3 position)
		{
			_position = position;
		}

		public PathPoint(UnityEngine.Transform position)
		{
			_position = position.position;
			_up = position.up;
		}

		public PathPoint(UnityEngine.Vector3 position, float distance)
		{
			_position = position;
			_distance = distance;
		}

		public PathPoint(UnityEngine.Vector3 position, float distance, UnityEngine.Vector3 up)
		{
			_position = position;
			_distance = distance;
			_up = up;
		}

		public PathPoint(PathPoint point, float distance)
		{
			_position = point.Position;
			_up = point.Up;
			_distance = distance;
		}

		public static PathPoint Lerp(PathPoint a, PathPoint b, float t)
		{
			return new PathPoint(UnityEngine.Vector3.Lerp(a.Position, b.Position, t), UnityEngine.Mathf.Lerp (a.Distance, b.Distance, t), UnityEngine.Vector3.Lerp (a.Up, b.Up, t).normalized);
		}
	}
}

