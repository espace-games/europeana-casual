﻿using System;

namespace SGPath
{
	public class Path
	{
		PathPoint[] _path = null;
		bool _loop = false;
		
		public float PathLength
		{
			get { return ((_path != null && _path.Length > 0) ? _path[_path.Length -1].Distance : 0.0f); }	
		}
		
		public int PointCount
		{
			get { return (_path != null ? _path.Length : 0); }
		}

		public bool Loops()
		{
			return _loop;
		}
		
		public UnityEngine.Vector3 Destination
		{
			get { return GetPositionByIndex(PointCount-1).Position; }	
		}
		
		public Path (UnityEngine.Vector3[] path, bool loop)
		{
			_loop = loop;
			if(path != null && path.Length > 0)
			{
				if(_loop)
				{
					_path = new PathPoint[path.Length + 1];	
					
					_path[0] = new PathPoint(path[0], 0.0f);
					
					for(int i = 1; i < path.Length; ++i)
					{
						_path[i] = new PathPoint(path[i], _path[i -1].Distance + UnityEngine.Vector3.Distance(path[i - 1], path[i]));
					}
					
					_path[path.Length] = new PathPoint(path[0], _path[path.Length -1].Distance + UnityEngine.Vector3.Distance(path[path.Length -1], path[0]));
				}
				else
				{
					_path = new PathPoint[path.Length];	
					
					_path[0] = new PathPoint(path[0], 0.0f);
					
					for(int i = 1; i < _path.Length; ++i)
					{
						_path[i] = new PathPoint(path[i], _path[i -1].Distance + UnityEngine.Vector3.Distance(path[i - 1], path[i]));
					}
				}
			}
		}


		public Path (PathPoint[] path, bool loop)
		{
			_loop = loop;
			if(path != null && path.Length > 0)
			{
				if(_loop)
				{
					_path = new PathPoint[path.Length + 1];	
					
					_path[0] = new PathPoint(path[0], 0.0f);
					
					for(int i = 1; i < path.Length; ++i)
					{
						_path[i] = new PathPoint(path[i], _path[i -1].Distance + UnityEngine.Vector3.Distance(path[i - 1].Position, path[i].Position));
					}
					
					_path[path.Length] = new PathPoint(path[0], _path[path.Length -1].Distance + UnityEngine.Vector3.Distance(path[path.Length -1].Position, path[0].Position));
				}
				else
				{
					_path = new PathPoint[path.Length];	
					
					_path[0] = new PathPoint(path[0], 0.0f);
					
					for(int i = 1; i < _path.Length; ++i)
					{
						_path[i] = new PathPoint(path[i], _path[i -1].Distance + UnityEngine.Vector3.Distance(path[i - 1].Position, path[i].Position));
					}
				}
			}
		}


		
		public PathPoint GetPositionByProgress(float fDistance)
		{
			if(_path != null && _path.Length > 0)
			{
				if(_loop)
				{
					while(fDistance < 0.0f)
					{
						fDistance += PathLength;
					}
					
					fDistance = fDistance % PathLength;
				}
				else
				{
					if(fDistance <= 0.0f)
					{
						return _path[0];	
					}
					
					if(fDistance >= PathLength)
					{
						return _path[_path.Length-1];	
					}
				}
				
				int i = 0;
				
				while(_path[i].Distance < fDistance)
				{
					i++;	
				}
				
				PathPoint a = _path[ (i > 0 ? i : _path.Length) -1];
				PathPoint b = _path[i];
				
				float dist = b.Distance - a.Distance;
				float localDis = fDistance - a.Distance;
				
				//return UnityEngine.Vector3.Lerp(a.Position, b.Position, localDis/dist);
				return PathPoint.Lerp(a, b, localDis/dist);
			}
			return PathPoint.ZERO;
		}
		
		public PathPoint GetPositionByIndex(int index)
		{
			return ((_path != null && index >= 0 && index < _path.Length) ? _path[index] : PathPoint.ZERO);	
		}
	}
}

