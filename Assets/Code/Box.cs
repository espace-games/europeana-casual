﻿using UnityEngine;
using System.Collections;

public class Box {


	private Vector2 _topLeft = Vector2.zero;
	private Vector2 _bottomRight = Vector2.zero;
	private Vector2 _center = Vector2.zero;
	private float _width = 0.0f;
	private float _height = 0.0f;
	private Edge[] _edges = null;
	private Line[] _lines = null;
	private GameObject _dustLayer = null;
	private GameObject _damageLayer = null;

	private bool _final = false;
	private float _alpha = 1.0f;


	public Vector2 TopLeft
	{
		get { return _topLeft; }
	}

	public Vector2 BottomRight
	{
		get { return _bottomRight; }
	}

	public Vector2 Center
	{
		get { return _center; }
	}

	public float Width
	{
		get { return _width; }
	}

	public float Height
	{
		get { return _height; }
	}

	public float Area
	{
		get { return Width * Height; }
	}

	public int EdgeCount
	{
		get { return (_edges != null ? _edges.Length : 0); }
	}

	public bool Final
	{
		get { return _final; }
		set { _final = value; }
	}

	public Edge GetEdge(int index)
	{
		return ((_edges != null && index >= 0 && index < _edges.Length) ? _edges[index] : null);
	}


	public void SetDustLayer(GameObject dustLayer, GameObject damageLayer)
	{
		_dustLayer = dustLayer;
		_damageLayer = damageLayer;
	}

	public void GenerateLines(PaintingGame game)
	{
		if(game != null)
		{
			_lines = new Line[_edges.Length];

			for(int i = 0; i < _edges.Length; ++i)
			{
				_lines[i] = null;
			}

			float height = game.transform.position.y + 0.01f;

			float halfPaintingWidth = game.paintingWidth * 0.5f;
			float halfPaintingHeight = game.paintingHeight * 0.5f;


			int index = (int)Edge.eEdgeLocation.Top;
			Edge edge = GetEdge(index);
			if(edge != null && edge.PointA.y != halfPaintingHeight && edge.PointA.y != -halfPaintingHeight)
			{
				_lines[index] = new Line(game, _edges[index].GetThreeDeePointA(height), _edges[index].GetThreeDeePointB(height), true);
			}

			index = (int)Edge.eEdgeLocation.Bottom;
			edge = GetEdge(index);
			if(edge != null && edge.PointA.y != halfPaintingHeight && edge.PointA.y != -halfPaintingHeight)
			{
				_lines[index] = new Line(game, _edges[index].GetThreeDeePointA(height), _edges[index].GetThreeDeePointB(height), true);
			}

			index = (int)Edge.eEdgeLocation.Left;
			edge = GetEdge(index);
			if(edge != null && edge.PointA.x != halfPaintingWidth && edge.PointA.x != -halfPaintingWidth)
			{
				_lines[index] = new Line(game, _edges[index].GetThreeDeePointA(height), _edges[index].GetThreeDeePointB(height), true);
			}

			index = (int)Edge.eEdgeLocation.Right;
			edge = GetEdge(index);
			if(edge != null && edge.PointA.x != halfPaintingWidth && edge.PointA.x != -halfPaintingWidth)
			{
				_lines[index] = new Line(game, _edges[index].GetThreeDeePointA(height), _edges[index].GetThreeDeePointB(height), true);
			}
		}


		/*for(int i = 0; i < _edges.Length; ++i)
		{
			_lines[i] = new Line(game, _edges[i].GetThreeDeePointA(height), _edges[i].GetThreeDeePointB(height));
			_lines[i].UpdateMesh(_edges[i].GetThreeDeePointB(height));
		}*/
	}

	public void DestroyDustLayer()
	{
		if(_dustLayer != null)
		{
			GameObject.Destroy(_dustLayer);
			_dustLayer = null;
		}

		if(_damageLayer != null)
		{
			GameObject.Destroy(_damageLayer);
			_damageLayer = null;
		}
	}

	public void DestroyLines()
	{
		if(_lines != null)
		{
			for(int i = 0; i < _lines.Length; ++i)
			{
				if(_lines[i] != null)
				{
					_lines[i].DestroyGameObject();
					_lines[i] = null;
				}
			}
		}
	}


	public Box(Vector2 topLeft, Vector2 bottomRight)
	{
		_topLeft = topLeft;
		_bottomRight = bottomRight;
		_width = _bottomRight.x - _topLeft.x;
		_height = _topLeft.y - _bottomRight.y;

		_center = (_topLeft + _bottomRight) * 0.5f;

		_edges = new Edge[4];
		_edges[(int)Edge.eEdgeLocation.Top] = new Edge(new Vector2(_center.x, TopLeft.y), Width, Edge.eEdgeLocation.Top);
		_edges[(int)Edge.eEdgeLocation.Bottom] = new Edge(new Vector2(_center.x, BottomRight.y), Width, Edge.eEdgeLocation.Bottom);
		_edges[(int)Edge.eEdgeLocation.Left] = new Edge(new Vector2(TopLeft.x, _center.y), Height, Edge.eEdgeLocation.Left);
		_edges[(int)Edge.eEdgeLocation.Right] = new Edge(new Vector2(BottomRight.x, _center.y), Height, Edge.eEdgeLocation.Right);
	}

	public Box[] SplitHorizontally(float normalisedPosition)
	{
		Box[] boxes = new Box[2];
		boxes[0] = new Box(TopLeft, new Vector2(BottomRight.x, TopLeft.y - (Height * Mathf.Clamp(normalisedPosition, 0.0f, 1.0f))));
		boxes[1] = new Box(new Vector2(boxes[0].TopLeft.x, boxes[0].BottomRight.y), BottomRight);

		return boxes;
	}

	public Box[] SplitVertically(float normalisedPosition)
	{
		Box[] boxes = new Box[2];
		boxes[0] = new Box(TopLeft, new Vector2(TopLeft.x + (Width * Mathf.Clamp(normalisedPosition, 0.0f, 1.0f)), BottomRight.y));
		boxes[1] = new Box(new Vector2(boxes[0].BottomRight.x, boxes[0].TopLeft.y), BottomRight);

		return boxes;
	}

	public Edge GetNearestEdge(Vector3 position)
	{
		Edge nearest = null;
		float distanceNearest = float.MaxValue;
		
		if(_edges != null)
		{
			for(int i = 0; i < _edges.Length; ++i)
			{
				if(_edges[i] != null)
				{
					float distance = _edges[i].Distance(position);
					if(distance < distanceNearest)
					{
						distanceNearest = distance;
						nearest = _edges[i];
					}
				}
			}
		}
		return nearest;
	}
	
	public Edge GetOppositeEdge(Edge edge)
	{
		if(_edges != null && edge != null)
		{
			Edge.eEdgeLocation required = edge.OppositeLocation;
			
			for(int i = 0; i < _edges.Length; ++i)
			{
				if(_edges[i] != null && _edges[i] != edge)
				{
					if(_edges[i].Location == required)
					{
						return _edges[i];
					}
				}
			}
		}
		return null;
	}

	public bool FadeOut(float deltaTime)
	{
		if(this._dustLayer != null && this._dustLayer.renderer != null && this._dustLayer.renderer.material != null)
		{
			_alpha = Mathf.Max(0.0f, _alpha - (2.0f * deltaTime));
			Color colour = new Color(1.0f, 1.0f, 1.0f, _alpha);
			this._dustLayer.renderer.material.SetColor("_Color", colour);

			if(this._damageLayer != null)
			{
				this._damageLayer.renderer.material.SetColor("_Color", colour);
			}
			if(_lines != null)
			{
				for(int i = 0; i < _lines.Length; ++i)
				{
					if(_lines[i] != null && _lines[i].obj != null && _lines[i].obj.renderer != null)
					{
						_lines[i].obj.renderer.material.SetColor("_Color", colour);
					}
				}
			}
		}
		else
		{
			_alpha = 0.0f;
		}

		return (_alpha <= 0.0f);
	}
}
