﻿using UnityEngine;
using System.Collections;

public class Line
{
	public GameObject obj = null;
	
	private PaintingGame _parent = null;
	private Vector3 _startPosition = Vector3.zero;
	private MeshFilter _meshFilter = null;
	
	public Line(PaintingGame parent, Vector3 startPos, Vector3 endPos, bool startWithFullLine)
	{
		_parent = parent;
		obj = new GameObject();
		obj.name = "Line";
		
		
		Vector3 direction = (endPos - startPos).normalized;
		
		
		obj.transform.parent = _parent.transform;
		_startPosition = obj.transform.position = startPos;
		
		obj.transform.rotation = Quaternion.LookRotation(direction);
		
		_meshFilter = obj.AddComponent<MeshFilter>();
		MeshRenderer meshRenderer = obj.AddComponent<MeshRenderer>();
		_meshFilter.mesh = new Mesh();
		if(startWithFullLine)
		{
			UpdateMesh(_meshFilter.mesh, 0.01f, Vector3.Distance(startPos, endPos));
		}
		else
		{
			UpdateMesh(_meshFilter.mesh, 0.01f, 0.0f);
		}
		meshRenderer.material = parent.lineMaterial;
		meshRenderer.material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, 1.0f));
		
	}

	public void DestroyGameObject()
	{
		if(obj != null)
		{
			GameObject.Destroy(obj);
			obj = null;
			_meshFilter = null;
		}
	}
	
	public void UpdateMesh(Vector3 currentPosition)
	{
		UpdateMesh(_meshFilter.mesh, 0.01f, Vector3.Distance(_startPosition, currentPosition));
	}
	
	protected Mesh UpdateMesh(Mesh mesh, float width, float length)
	{
		length = Mathf.Max (0.01f, length);
		
		Vector3[] vertices = new Vector3[4];
		
		vertices[0] = new Vector3 (-(width* 0.5f), 0.0f, length);
		vertices[1] = new Vector3 ((width* 0.5f), 0.0f, length);
		vertices[2] = new Vector3 ((width* 0.5f), 0.0f, 0.0f);
		vertices[3] = new Vector3 (-(width* 0.5f), 0.0f, 0.0f);
		
		int[] triangles = new int[6];
		triangles[0] = 0;
		triangles[1] = 1;
		triangles[2] = 3;
		triangles[3] = 1;
		triangles[4] = 2;
		triangles[5] = 3;
		
		float u = 0.0f;
		float v = 0.0f;
		float texWidth = 1.0f;
		float texHeight = length/width;
		
		Vector2[] uvs = new Vector2[4];
		uvs[0] = new Vector2 (u, v + texHeight);
		uvs[1] = new Vector2 (u + texWidth, v + texHeight);
		uvs[2] = new Vector2 (u + texWidth, v);
		uvs[3] = new Vector2 (u, v);
		
		mesh.Clear ();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.Optimize ();
		mesh.RecalculateNormals();
		
		return mesh;
	}
}