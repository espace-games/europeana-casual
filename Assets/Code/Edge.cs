﻿using UnityEngine;
using System.Collections;

public class Edge
{
	public enum eEdgeLocation
	{
		Top = 0,
		Bottom,
		Left,
		Right
	}


	public static bool IsLocationHorizontal(eEdgeLocation location)
	{
		return (location == eEdgeLocation.Top || location == eEdgeLocation.Bottom);
	}

	public static eEdgeLocation GetOppositeLocation(eEdgeLocation location)
	{
		switch(location)
		{
		case eEdgeLocation.Top:
			return eEdgeLocation.Bottom;
		case eEdgeLocation.Bottom:
			return eEdgeLocation.Top;
		case eEdgeLocation.Left:
			return eEdgeLocation.Right;
		case eEdgeLocation.Right:
			return eEdgeLocation.Left;
		default:
			break;
		}
		return eEdgeLocation.Bottom;
	}

	public static float Distance1D(float a, float b)
	{
		return Mathf.Sqrt((b-a) * (b-a));
	}

	public static Vector2 ThreeDeeToTwoDee(Vector3 threeDee)
	{
		return new Vector2(threeDee.x, threeDee.z);
	}

	public static Vector3 TwoDeeToThreeDee(Vector2 twoDee, float height)
	{
		return new Vector3(twoDee.x, height, twoDee.y);
	}

	private Vector2 _center = Vector2.zero;
	private Vector2 _pointA = Vector2.zero;
	private Vector2 _pointB = Vector2.zero;
	private eEdgeLocation _edgeLocation = eEdgeLocation.Top;


	public Vector2 PointA
	{
		get { return _pointA; }
	}

	public Vector2 PointB
	{
		get { return _pointB; }
	}
	
	public eEdgeLocation Location
	{
		get { return _edgeLocation; }
	}

	public eEdgeLocation OppositeLocation
	{
		get { return Edge.GetOppositeLocation(_edgeLocation); }
	}

	public Vector3 GetThreeDeePointA(float height)
	{
		return Edge.TwoDeeToThreeDee(_pointA, height);
	}

	public Vector3 GetThreeDeePointB(float height)
	{
		return Edge.TwoDeeToThreeDee(_pointB, height);
	}

	public float GetDistance(Edge other)
	{
		return (other != null ? Vector2.Distance(this._center, other._center) : 0.0f);
	}

	public Edge(Vector2 center, float length, eEdgeLocation location)
	{
		_center = center;
		_edgeLocation = location;


		Vector2 offset = Edge.IsLocationHorizontal(_edgeLocation) ? new Vector2(length * 0.5f, 0.0f) : new Vector2(0.0f, length * 0.5f);
		_pointA = center - offset;
		_pointB = center + offset;
	}

	public bool IsHorizontal()
	{
		return Edge.IsLocationHorizontal(_edgeLocation);
	}

	public Vector3 GetIntersectionPointThreeDee(Vector3 position, float height, float minSliceSize)
	{
		return Edge.TwoDeeToThreeDee(CalculateIntersectionPoint(position, minSliceSize), height);
	}

	public Vector2 CalculateIntersectionPoint(Vector3 position, float minSliceSize)
	{
		return CalculateIntersectionPoint(Edge.ThreeDeeToTwoDee(position), minSliceSize);
	}

	public Vector2 CalculateIntersectionPoint(Vector2 position, float minSliceSize)
	{
		Vector2 intersectionPoint = _center;
		
		if(IsHorizontal())
		{
			intersectionPoint.x = Mathf.Clamp(position.x, _pointA.x + minSliceSize, _pointB.x - minSliceSize);
		}
		else
		{
			intersectionPoint.y = Mathf.Clamp(position.y, _pointA.y + minSliceSize, _pointB.y - minSliceSize);
		}

		return intersectionPoint;
	}

	public float Distance(Vector3 position)
	{
		return Distance(Edge.ThreeDeeToTwoDee(position));
	}

	public float Distance(Vector2 position)
	{
		Vector2 intersectionPoint = CalculateIntersectionPoint(position, 0.0f);
		return Vector2.Distance(intersectionPoint, position);
	}

	public float DistanceToNearestPoint(Vector3 position)
	{
		return DistanceToNearestPoint(Edge.ThreeDeeToTwoDee(position));
	}

	public float DistanceToNearestPoint(Vector2 position)
	{
		float distA = Vector2.Distance(PointA, position);
		float distB = Vector2.Distance(PointB, position);

		return (distA < distB ? distA : distB);
	}
}
