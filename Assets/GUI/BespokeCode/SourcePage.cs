﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class SourcePage : Page, OnlineImageMonitor
	{
		private string STR_GETTNG_IMAGES = "Attempting to get images please wait...";
		private string STR_ERROR = "No images received, using offline images.";

		private bool _mouseHasBeenUp = false;
		private GUIBox _border = null;
		private GUIButton _btnBack = null;
		private GUIBox _boxBackButton = null;
		private GUIBox _boxPlayButton = null;
		private GUIButton _btnPlay = null;
		private GUIEditableText _txtSearchTerms = null;
		private GUILabel _lblGettingImages = null;
		private GUIBox _boxInputBorder = null;

		private bool _waitingForImages = false;
		private float _timeOut = 0.0f;


		public static SourcePage Create(GUIEntity parent)
		{
			SourcePage page = null;
			if(parent != null)
			{
				page = (SourcePage)parent.AddChild(new SourcePage());
				page.Reset();
			}
			return page;
		}
		
		private GUILabel _lblTitle = null;
		private float _titleIdealHeight = 1.0f;
		private GUILabel _lblInstructionsA = null;
		private GUILabel _lblInstructionsB = null;

		private static float START_Y = -300.0f;
		private static float PADDING = 50.0f;

		private Font _largeFont = null;
		private Font _smallFont = null;

		private float _errorTimer = 0.0f;

		
		protected SourcePage()
		{
			_border = (SGGUI.GUIBox)AddChild(new SGGUI.GUIBox(1280.0f - (BOX_BORDER * 2.0f), 800.0f - (BOX_BORDER * 2.0f), new Color(0.0f, 0.0f, 0.0f, 0.5f)));
			_border.BorderSize = 4.0f;
			_border.IgnoreAutoScaling = true;


			_lblTitle = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 240.0f, "Select Artwork Source"));
			_lblTitle.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansRegular48(_lblTitle);
			_titleIdealHeight = _lblTitle.Height;


			_lblInstructionsA = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 180.0f, "Enter a search term to use online images:"));
			_lblInstructionsA.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansLight30(_lblInstructionsA);


			GameUI gui = GameControl.GetGUI();

			Font headerFont = null;

			if(gui != null)
			{
				_largeFont = gui.fontSmall;
				_smallFont = gui.fontSmaller;
				headerFont = gui.fontMedium;
			}


			//GUIImage searchBoxBackground = (GUIImage)_box.AddChild(new GUIImage(0.0f, 270.0f, "searchbox.png"));


			_txtSearchTerms = (GUIEditableText)AddChild(new GUIEditableText(0.0f, 0.0f, "search terms"));
			_txtSearchTerms.Style.font = FontManager.GetFontOpenSansLight30();
			_txtSearchTerms.Style.fontSize = 30;
			_txtSearchTerms.Style.alignment = TextAnchor.MiddleCenter;

			_txtSearchTerms.Colour = Color.white;
			_txtSearchTerms.Text = "WWWWWWWWWWWWWWWWWWWW";	//20 Ws to get width to fit everything!
			_txtSearchTerms.Autosize();
			_txtSearchTerms.CharacterLimit = 20;
			_txtSearchTerms.IgnoreAutoScaling = true;


			_boxInputBorder = (SGGUI.GUIBox)AddChild(new GUIBox(0.0f, 0.0f, _txtSearchTerms.Width + (2.0f * INNER_BORDER), _txtSearchTerms.Height + (2.0f * INNER_BORDER), new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_boxInputBorder.BorderSize = 4.0f;
			_boxInputBorder.IgnoreAutoScaling = true;

			_lblInstructionsB = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 360.0f, "Or leave blank to use included works of art!"));
			_lblInstructionsB.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansLight30(_lblInstructionsB);



			_btnBack = (SGGUI.GUIButton)AddChild(new SGGUI.GUIButton(110.0f, -50.0f, "button_back_up.png", "button_back_down.png", "button_back_up.png"));
			_btnBack.Docking = eDock.BottomLeft;
			_btnBack.OnPressDelegate = Back_OnPress;
			_btnBack.IgnoreAutoScaling = true;
			
			
			_boxBackButton = (SGGUI.GUIBox)AddChild(new GUIBox(110.0f, -50.0f, _btnBack.Width, _btnBack.Height, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_boxBackButton.Docking = eDock.BottomLeft;
			_boxBackButton.BorderSize = 4.0f;
			_boxBackButton.IgnoreAutoScaling = true;


			_btnPlay = (SGGUI.GUIButton)AddChild(new SGGUI.GUIButton(-110.0f, -50.0f, "button_play_up.png", "button_play_down.png", "button_play_up.png"));
			_btnPlay.Docking = eDock.BottomRight;
			_btnPlay.OnPressDelegate = Play_OnPress;
			_btnPlay.IgnoreAutoScaling = true;

			_boxPlayButton = (SGGUI.GUIBox)AddChild(new GUIBox(110.0f, -50.0f, _btnPlay.Width, _btnPlay.Height, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_boxPlayButton.Docking = eDock.BottomRight;
			_boxPlayButton.BorderSize = 4.0f;
			_boxPlayButton.IgnoreAutoScaling = true;

			_lblGettingImages = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, STR_GETTNG_IMAGES));
			_lblGettingImages.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansLight30(_lblGettingImages);

		}

		private void Autosize()
		{
			float screenWidth = (float)Screen.width;
			float screenHeight = (float)Screen.height;
			
			float btnBackWidth = 0.0f;
			float btnBackHeight = 0.0f;
			float borderWidth = 0.0f;
			float borderHeight = 0.0f;
			
			Rect clipping = new Rect(0.0f, 0.0f, 0.0f, 0.0f);


			float btnScale =  Mathf.Min ((0.5f / GUIScreen.GUIScale()), 1.0f);
			if(_btnBack != null)
			{

				_btnBack.ScaleX = _btnBack.ScaleY = btnScale;
				_btnBack.X = BOX_BORDER + (_btnBack.Width * 0.5f * btnScale);
				_btnBack.Y = -BOX_BORDER - (_btnBack.Height * 0.5f * btnScale);
				
				btnBackWidth = _btnBack.Width * btnScale;
				btnBackHeight = _btnBack.Height * btnScale;
				
				if(_boxBackButton != null)
				{
					_boxBackButton.Autosize(btnBackWidth, btnBackHeight);
					_boxBackButton.X = _btnBack.X;
					_boxBackButton.Y = _btnBack.Y;
				}
			}

			if(_btnPlay != null)
			{
				_btnPlay.ScaleX = _btnPlay.ScaleY = btnScale;
				_btnPlay.X = -(BOX_BORDER + (_btnPlay.Width * 0.5f * btnScale));
				_btnPlay.Y = -BOX_BORDER - (_btnPlay.Height * 0.5f * btnScale);

				if(_boxPlayButton != null)
				{
					_boxPlayButton.Autosize(_btnPlay.Width * btnScale, _btnPlay.Height * btnScale);
					_boxPlayButton.X = _btnPlay.X;
					_boxPlayButton.Y = _btnPlay.Y;
				}
			}


			if(_border != null)
			{
				_border.Autosize(Mathf.Max((screenWidth - (2.0f * btnBackWidth) - (4.0f * BOX_BORDER)), 32.0f), (screenHeight - (2.0f * BOX_BORDER)));
				borderWidth = _border.Width;
				borderHeight = _border.Height;
			}

			float maxWidth = Mathf.Max (borderWidth - (2.0f * BOX_BORDER), 1.0f);
			float maxHeight = Mathf.Max (borderHeight - (2.0f * BOX_BORDER), 1.0f);

			float scale = 1.0f;

			float heightRequired = 0.0f;

			if(_txtSearchTerms!= null)
			{
				if(_txtSearchTerms.Width > maxWidth)
				{
					scale = maxWidth/_txtSearchTerms.Width;
				}

				_txtSearchTerms.ScaleX = _txtSearchTerms.ScaleY = scale;
				
				if(_boxInputBorder != null)
				{
					_boxInputBorder.Autosize((_txtSearchTerms.Width + (2.0f * INNER_BORDER)) * scale, (_txtSearchTerms.Height + (2.0f * INNER_BORDER)) * scale);


					heightRequired += _boxInputBorder.Height * scale;


					if(_lblInstructionsA != null)
					{
						float offset = (((_boxInputBorder.Height * 0.5f) + (_lblInstructionsA.Height * 0.5f) + (2.0f * INNER_BORDER)) * scale);
						_lblInstructionsA.Y = _boxInputBorder.Y - offset;

						_lblInstructionsA.ScaleX = _lblInstructionsA.ScaleY = scale;

						heightRequired += (_lblInstructionsA.Height * scale) + offset;
					}
					
					if(_lblInstructionsB != null)
					{
						float offset = (((_boxInputBorder.Height * 0.5f) + (_lblInstructionsB.Height * 0.5f) + (2.0f * INNER_BORDER)) * scale);
						_lblInstructionsB.Y = _boxInputBorder.Y + offset;

						_lblInstructionsB.ScaleX = _lblInstructionsB.ScaleY = scale;

						heightRequired += (_lblInstructionsB.Height * scale) + offset;
					}
				}
			}
			
			if(_lblTitle != null)
			{
				_lblTitle.Y = (-screenHeight* 0.5f) + BOX_BORDER + INNER_BORDER + (_lblTitle.Height * 0.5f * _lblTitle.ScaleY);
				_lblTitle.ScaleX = _lblTitle.ScaleY = scale;

				heightRequired += _lblTitle.Height * scale;
			}


			if(heightRequired > 0.0f && heightRequired > maxHeight)
			{
				scale *= maxHeight/heightRequired;

				if(_lblTitle != null)
				{
					_lblTitle.ScaleX = _lblTitle.ScaleY = scale;
				}

				_lblInstructionsA.ScaleX = _lblInstructionsA.ScaleY = scale;
				_lblInstructionsB.ScaleX = _lblInstructionsB.ScaleY = scale;
			}

			if(_lblGettingImages != null)
			{
				_lblGettingImages.ScaleX = _lblGettingImages.ScaleY = scale;
			}
		}
		
		protected override void Draw(float parentAlpha)
		{
			Autosize();
		}

		
		
		private void ShowWaitingForImages(bool show)
		{
			GUIEntity.SafeSetVisible(_lblGettingImages, show);

			if(_lblGettingImages != null)
			{
				_lblGettingImages.Text = STR_GETTNG_IMAGES;
				_lblGettingImages.Autosize();
			}

			GUIEntity.SafeSetVisible(_btnPlay, !show);
			GUIEntity.SafeSetVisible(_boxPlayButton, !show);

			GUIEntity.SafeSetVisible(_btnBack, !show);
			GUIEntity.SafeSetVisible(_boxBackButton, !show);

			GUIEntity.SafeSetVisible(_lblInstructionsA, !show);
			GUIEntity.SafeSetVisible(_lblInstructionsB, !show);
			GUIEntity.SafeSetVisible(_boxInputBorder, !show);
			GUIEntity.SafeSetVisible(_txtSearchTerms, !show);
			GUIEntity.SafeSetVisible(_lblTitle, !show);

			//GUIEntity.SafeSetVisible(_box, !show);
		}

		
		protected override void StartEnter()
		{
			ShowWaitingForImages(false);
			_waitingForImages = false;
			if(_txtSearchTerms != null)
			{
				_txtSearchTerms.Text = "";
			}
		

			GUIButton.SafeEnable(_btnBack, true);
			GUIButton.SafeEnable(_btnPlay, true);
			_mouseHasBeenUp = false;
		}
		
		protected override bool UpdateEnter(float deltaTime)
		{
			this.Visible = true;
			return true;
		}
		
		protected override void StartExit()
		{
			_waitingForImages = false;
			GUIButton.SafeEnable(_btnBack, false);
			GUIButton.SafeEnable(_btnPlay, false);
		}
		
		protected override void DoReset()
		{
			
		}
		

		public override void ActiveUpdate(float deltaTime)
		{
			if(_mouseHasBeenUp)
			{

			}
			else if(!Input.GetMouseButton(0))
			{
				_mouseHasBeenUp = true;
			}

			if(_waitingForImages)
			{
				if(_timeOut > 0.0f)
				{
					_timeOut -= Time.deltaTime;
					if(_timeOut <= 0.0f)
					{
						//StartOfflineGame();
						ShowFailureMessage(STR_ERROR);
					}
				}

				if(_errorTimer > 0.0f)
				{
					_errorTimer -= Time.deltaTime;

					if(_errorTimer <= 0.0f)
					{
						StartOfflineGame();
					}
				}
			}
		}

		public void Back_OnPress(GUIEntity guiEntity)
		{
			if(_waitingForImages)
			{
				GameControl.SetOnlineSearchTerm(null, null);
				ShowWaitingForImages(false);
			}
			else
			{
				GameControl.SetState(GameControl.eState.OnTitleScreen);
				GameControl.PlaySFXTone(0);
			}
		}

		public void Play_OnPress(GUIEntity guiEntity)
		{

			string searchText = null;
			if(_txtSearchTerms != null && _txtSearchTerms.Text.Length > 0)
			{
				searchText = _txtSearchTerms.Text.Trim();
			}

			if( GameControl.SetOnlineSearchTerm(searchText, this))
			{
				//Online images
				_timeOut = 10.0f;
				_waitingForImages = true;
				ShowWaitingForImages(true);

			}
			else
			{
				//Offline images
				GameControl.SetState(GameControl.eState.PlayingGame);
			}

			/*GameUI gui = GameControl.GetGUI();
			if(gui)
			{
				gui.ShowPage(GameUI.ePages.Title);
			}
			GameControl.PlaySFXTone(0);*/
		}

		public void OnFirstImage()
		{
			if(_waitingForImages)
			{
				GameControl.SetState(GameControl.eState.PlayingGame);
				_waitingForImages = false;
			}
		}

		public void OnAllImages()
		{
			if(_waitingForImages)
			{
				GameControl.SetState(GameControl.eState.PlayingGame);
				_waitingForImages = false;
			}
		}

		public void OnError(int error)
		{
			if(_waitingForImages && error != Europeana.ERROR_CANCELLED)
			{
				//StartOfflineGame();
				ShowFailureMessage(STR_ERROR);
			}
		}

		private void StartOfflineGame()
		{
			GameControl.SetOnlineSearchTerm(null, null);
			GameControl.SetState(GameControl.eState.PlayingGame);
			_waitingForImages = false;
		}

		private void ShowFailureMessage(string message)
		{
			if(message != null && _lblGettingImages != null)
			{
				_lblGettingImages.Text = message;
				_lblGettingImages.Autosize();
			}
			_errorTimer = 2.0f;
			GUIEntity.SafeSetVisible(_btnBack, false);
			GUIEntity.SafeSetVisible(_boxBackButton, false);
			GUIEntity.SafeSetVisible(_btnPlay, false);
			GUIEntity.SafeSetVisible(_boxPlayButton, false);
		}
	}
}


