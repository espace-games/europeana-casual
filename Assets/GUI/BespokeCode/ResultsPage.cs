﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class ResultsPage : Page
	{
		private static string STR_ADVANCE = "On To The Next Painting!";
		private static string STR_FAILURE = "Game Over!";

		private static string STR_SCORE_ADVANCE = "Current Score";
		private static string STR_SCORE_FAILURE = "Final Score";

		private bool _mouseHasBeenUp = false;
		private GUIEntity _box = null;
		private GUILabel _restorationSuccessResult = null;
		private GUILabel _lblBonusResult = null;
		private GUILabel _lblCurrentScoreTitle = null;
		private GUILabel _lblCurrentScoreResult = null;
		private int _successPercentage = 0;
		private int _successRequired = 0;

		private GUILabel _lblResult = null;

		public static ResultsPage Create(GUIEntity parent)
		{
			ResultsPage page = null;
			if(parent != null)
			{
				page = (ResultsPage)parent.AddChild(new ResultsPage());
				page.Reset();
			}
			return page;
		}

		private GUILabel _lblTitle = null;
		
		protected ResultsPage()
		{
			//_box = AddChild(new GUIBox(0.0f, 0.0f, 900.0f, 700.0f, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_box = AddChild(new GUIEntity());

			_lblTitle = CreateTitle(_box, 0.0f, -300.0f, "Results");

			GUILabel restorationSuccessTitle = CreateSubTitle(_box, 0.0f, -220.0f, "Restoration Success");
			_restorationSuccessResult = CreateSubTitle(_box, 0.0f, -160.0f, "XXX%");
			_restorationSuccessResult.Style.fontStyle = FontStyle.Normal;

			GUILabel lblBonusTitle = CreateSubTitle(_box, 0.0f, -80.0f, "Bonus");
			_lblBonusResult = CreateSubTitle(_box, 0.0f, -20.0f, "0");
			_lblBonusResult.Style.fontStyle = FontStyle.Normal;


			_lblCurrentScoreTitle = CreateSubTitle(_box, 0.0f, 60.0f, "Total Score");
			_lblCurrentScoreResult = CreateSubTitle(_box, 0.0f, 120.0f, "XXXXXXX");
			_lblCurrentScoreResult.Style.fontStyle = FontStyle.Normal;

			_lblResult = CreateTitle(_box, 0.0f, 220.0f, "On To The Next Painting!");
			_lblResult.Style.fontStyle = FontStyle.Italic;

			GUILabel clickToContinue = CreateNormalText(_box, 310.0f, 320.0f, "Click To Continue");
		}

		protected override void StartEnter()
		{
			_mouseHasBeenUp = false;
			_successPercentage = (int)((GameControl.GetLastSuccessPercentage() * 100.0f) + 0.5f);
			_successRequired = GameControl.PassPercentageForLevel(GameControl.GetCurrentLevel());

			if(_restorationSuccessResult != null)
			{
				_restorationSuccessResult.Text = _successPercentage.ToString() + "%";
			}

			int bonus = GameControl.CalculateBonus(_successPercentage);


			if(_lblBonusResult != null)
			{
				_lblBonusResult.Text = bonus.ToString();
			}

			int currentScore = GameControl.BankPoints(bonus);

			if(_lblCurrentScoreTitle != null)
			{
				_lblCurrentScoreTitle.Text = (ShouldAdvance() ? STR_SCORE_ADVANCE : STR_SCORE_FAILURE);
			}

			if(_lblCurrentScoreResult != null)
			{
				_lblCurrentScoreResult.Text = currentScore.ToString();
			}

			if(_lblResult != null)
			{
				_lblResult.Text = (ShouldAdvance() ? STR_ADVANCE : STR_FAILURE);
			}

			GameControl.PlaySFXResults(ShouldAdvance() ? 0 : 1);
		}

		protected override bool UpdateEnter(float deltaTime)
		{
			this.Visible = true;
			return true;
		}
		
		protected override void StartExit()
		{
			
		}
		
		protected override void DoReset()
		{

		}

		private bool ShouldAdvance()
		{
			return (_successPercentage >= _successRequired);
		}

		public override void ActiveUpdate(float deltaTime)
		{
			if(_mouseHasBeenUp)
			{
				if(Input.GetMouseButtonDown(0))
				{
					PaintingGame paintingGame = GameControl.GetPaintingGame();
					if(paintingGame != null)
					{
						if(ShouldAdvance())
						{
							if(paintingGame != null)
							{
								paintingGame.ContinueGame();
							}
						}
						else
						{
							if(paintingGame != null)
							{
								paintingGame.EndGame();
							}
						}
					}
					else
					{
						GameControl.SetState(GameControl.eState.OnTitleScreen);
					}

					//GameControl.SetState(GameControl.eState.PlayingGame);
				}
			}
			else if(!Input.GetMouseButton(0))
			{
				_mouseHasBeenUp = true;
			}
		}
	}
}
