﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class TextParticles : GUIEntity
	{
		private class ParticleWrapper
		{
			private GUIEntity _parent = null;
			private GUILabel _label = null;
			private Vector2 _velocity = Vector2.zero;
			private float _lifeCurrent = 0.0f;
			private float _lifeMax = 1.0f;


			public ParticleWrapper(GUIEntity parent, GUILabel label)
			{
				_parent = parent;
				_label = label;

				if(_label != null)
				{
					//_label.Style.fontSize = 48;
					_label.Style.alignment = TextAnchor.MiddleCenter;
					//_label.Stroke = 2.0f;
					//_label.StrokeColour = Color.black;


					_label.Visible = false;
					
					GameUI gui = GameControl.GetGUI();
					if(gui != null && gui.fontSmall != null)
					{
						_label.Style.font = gui.fontMedium;
					}

					_label.DropShadow = true;
				}

				Reset ();
			}

			public bool IsActive()
			{
				return (_label != null && _label.Visible);
			}

			public void Reset()
			{
				if(IsActive() && _parent != null)
				{
					_parent.RemoveChild(_label);
				}
				_lifeCurrent = 0.0f;
				GUIEntity.SafeSetVisible(_label, false);
			}

			public void Spawn(string text, float lifeTime, Color colour, Vector2 position, float rotation, Vector2 velocity)
			{
				if(text != null && text.Length > 0.0f && lifeTime > 0.0f && _label != null && _parent != null)
				{
					Reset ();
					_lifeCurrent = _lifeMax = lifeTime;
					_velocity = velocity;
					_label.Colour = colour;
					_label.X = position.x;
					_label.Y = position.y;
					_label.A = 1.0f;
					_label.Text = text;
					_label.Visible = true;
					_parent.AddChild(_label);
					_label.Rotation = rotation;
				}
			}

			public void Update(float deltaTime)
			{
				if(IsActive())
				{
					_lifeCurrent -= deltaTime;
					if(_lifeCurrent <= 0.0f)
					{
						Reset();
					}
					else
					{
						_label.X += _velocity.x * deltaTime;
						_label.Y += _velocity.y * deltaTime;
						_label.A = _lifeCurrent/_lifeMax;
					}
				}
			}
		}

		private GUIImage _holder = null;

		private ParticleWrapper[] _lblScoreParticles = null;

		public TextParticles(int maxParticles)
		{
			maxParticles = Mathf.Max(1, maxParticles);
			_lblScoreParticles = new ParticleWrapper[maxParticles];
			for(int i = 0; i <_lblScoreParticles.Length; ++i)
			{
				_lblScoreParticles[i] = new ParticleWrapper(this, new SGGUI.GUILabel("PARTICLE"));
			}
		}

		protected override void Process(float deltaTime)
		{
			if(_lblScoreParticles != null)
			{
				for(int i = 0; i < _lblScoreParticles.Length; ++i)
				{
					_lblScoreParticles[i].Update(deltaTime);
				}
			}
		}

		public bool ShowParticle(string text, float lifeTime, Color colour, Vector2 position, float rotation, Vector2 velocity)
		{
			if(text != null && text.Length > 0 && _lblScoreParticles != null)
			{
				for(int i = 0; i < _lblScoreParticles.Length; ++i)
				{
					if(_lblScoreParticles[i] != null && !_lblScoreParticles[i].IsActive())
					{
						_lblScoreParticles[i].Spawn(text, lifeTime, colour, position, rotation, velocity);

						return true;
					}
				}
			}

			return false;
		}
	}


}
