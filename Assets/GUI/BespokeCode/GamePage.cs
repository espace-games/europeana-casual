﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class GamePage : Page
	{
		private static float DESIRED_BORDER_HEIGHT = 80.0f;
		private static string STR_SCORE_ADVANCE = "Current Score";
		private static string STR_SCORE_FAILURE = "Final Score";


		public static GamePage Create(GUIEntity parent)
		{
			GamePage page = null;
			if(parent != null)
			{
				page = (GamePage)parent.AddChild(new GamePage());
				page.Reset();
			}
			return page;
		}

		private SGGUI.GUIImage _imgScoreHolder = null;
		private SGGUI.GUILabel _lblScore = null;
		private SGGUI.PaintLineMiniGame _paintingGame = null;
		private SGGUI.FlagParticles _feedback = null;
		private SGGUI.GUILabel _lblCurrentLevel = null;
		private SGGUI.GUILabel _lblNextLevel = null;

		private SGGUI.GUIEffect _effTitle = null;

		private SGGUI.GUILabel _lblPaintingTitle = null;
		private SGGUI.GUILabel _lblPaintingSource = null;

		private SGGUI.GUIBox _boxTop = null;
		private SGGUI.GUIBox _boxBottom = null;

		private SGGUI.GUIBox _resultsBorder = null;
		private SGGUI.GUILabel _lblResultsTitle = null;

		private SGGUI.GUILabel _lblRestorationAchievedTitle = null;
		private SGGUI.GUILabel _lblRestorationAchievedResult = null;

		private SGGUI.GUILabel _lblBonusTitle = null;
		private SGGUI.GUILabel _lblBonusResults = null;

		private SGGUI.GUILabel _lblCurrentScoreTitle = null;
		private SGGUI.GUILabel _lblCurrentScoreResults = null;

		private SGGUI.GUIButton _btnContinue = null;
		private SGGUI.GUIBox _boxContinueBorder = null;

		private SGGUI.GUIBox _boxUnderline = null;

		private int _resultSuccessPercentage = 0;
		private int _resultSuccessRequired = 0;

		private float _scoreOffsetCurrent = 0.0f;
		private float _scoreOffsetDesired = 0.0f;

		private Texture2D _texContinueUp = null;
		private Texture2D _texContinueDown = null;

		private Texture2D _texFinishUp = null;
		private Texture2D _textFinishDown = null;

		public PaintLineMiniGame GameInput
		{
			get { return _paintingGame; }
		}
		
		protected GamePage()
		{
			_feedback = (SGGUI.FlagParticles)AddChild(new SGGUI.FlagParticles(10));
		
			_boxTop = (SGGUI.GUIBox)AddChild(new SGGUI.GUIBox(Screen.width, 80.0f, new Color(0.0f, 0.0f, 0.0f, 0.5f)));
			_boxTop.IgnoreAutoScaling = true;
			
			_boxBottom = (SGGUI.GUIBox)AddChild(new SGGUI.GUIBox(Screen.width, 80.0f, new Color(0.0f, 0.0f, 0.0f, 0.5f)));
			_boxBottom.IgnoreAutoScaling = true;

			_lblNextLevel = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Next Level At XX%"));
			_lblNextLevel.IgnoreAutoScaling = true;
			_lblNextLevel.Docking = eDock.TopMiddle;
			FontManager.ApplyFont_OpenSansLight30(_lblNextLevel);

			_lblCurrentLevel = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Level X"));
			_lblCurrentLevel.IgnoreAutoScaling = true;
			_lblCurrentLevel.Docking = eDock.TopLeft;
			FontManager.ApplyFont_OpenSansLight30(_lblCurrentLevel);


			_lblPaintingTitle = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Title"));
			_lblPaintingTitle.IgnoreAutoScaling = true;
			_lblPaintingTitle.Docking = eDock.BottomLeft;
			FontManager.ApplyFont_OpenSansLight30(_lblPaintingTitle);

			_lblPaintingSource = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Source"));
			_lblPaintingSource.IgnoreAutoScaling = true;
			_lblPaintingSource.Docking = eDock.BottomRight;
			FontManager.ApplyFont_OpenSansLight30(_lblPaintingSource);

			_imgScoreHolder = (SGGUI.GUIImage)AddChild(new GUIImage("score_holder.png"));
			_imgScoreHolder.IgnoreAutoScaling = true;
			_imgScoreHolder.Docking = eDock.TopRight;

			_lblScore = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "000000"));
			_lblScore.IgnoreAutoScaling = true;
			_lblScore.Docking = eDock.TopRight;
			FontManager.ApplyFont_OpenSansLight30(_lblScore);

			_paintingGame = (SGGUI.PaintLineMiniGame)AddChild(new SGGUI.PaintLineMiniGame());

			_resultsBorder = (SGGUI.GUIBox)AddChild(new SGGUI.GUIBox(1280.0f - (BOX_BORDER * 2.0f), 800.0f - (BOX_BORDER * 2.0f), new Color(0.0f, 0.0f, 0.0f, 0.5f)));
			_resultsBorder.BorderSize = 4.0f;
			_resultsBorder.IgnoreAutoScaling = true;

			_lblResultsTitle = (SGGUI.GUILabel)_resultsBorder.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "RESULTS"));
			_lblResultsTitle.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansRegular48(_lblResultsTitle);

			_boxUnderline = (SGGUI.GUIBox)_resultsBorder.AddChild(new GUIBox(0.0f, 0.0f, 10.0f, 4.0f, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_boxUnderline.IgnoreAutoScaling = true;
			_boxUnderline.BorderSize = 4.0f;

			_lblRestorationAchievedTitle = (SGGUI.GUILabel)_resultsBorder.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Restoration Achieved"));
			_lblRestorationAchievedTitle.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansLight30(_lblRestorationAchievedTitle);
			_lblRestorationAchievedResult = (SGGUI.GUILabel)_resultsBorder.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "100%"));
			_lblRestorationAchievedResult.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansRegular48(_lblRestorationAchievedResult);


			_lblBonusTitle = (SGGUI.GUILabel)_resultsBorder.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Bonus"));
			_lblBonusTitle.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansLight30(_lblBonusTitle);
			_lblBonusResults = (SGGUI.GUILabel)_resultsBorder.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "0"));
			_lblBonusResults.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansRegular48(_lblBonusResults);

			_lblCurrentScoreTitle = (SGGUI.GUILabel)_resultsBorder.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "Current Score"));
			_lblCurrentScoreTitle.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansLight30(_lblCurrentScoreTitle);
			_lblCurrentScoreResults = (SGGUI.GUILabel)_resultsBorder.AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "0"));
			_lblCurrentScoreResults.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansRegular48(_lblCurrentScoreResults);


			_texContinueUp = GUIEntity.LoadImage("button_next_painting_up.png");
			_texContinueDown = GUIEntity.LoadImage("button_next_painting_down.png");
			
			_texFinishUp = GUIEntity.LoadImage("button_return_up.png");
			_textFinishDown = GUIEntity.LoadImage("button_return_down.png");


			_btnContinue = (SGGUI.GUIButton)_resultsBorder.AddChild(new GUIButton(0.0f, 0.0f, _texContinueUp, _texContinueDown, _texContinueDown));
			_btnContinue.IgnoreAutoScaling = true;
			_btnContinue.OnPressDelegate = Continue_OnPress;
			_btnContinue.Enabled = false;

			_boxContinueBorder = (SGGUI.GUIBox)_resultsBorder.AddChild(new GUIBox(0.0f, 0.0f, _btnContinue.Width, _btnContinue.Height, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_boxContinueBorder.BorderSize = 4.0f;
			_boxContinueBorder.IgnoreAutoScaling = true;
		}


		private void ApplyScale(GUIEntity entity, float scale, float maxWidth)
		{
			if(entity != null)
			{
				entity.ScaleX = entity.ScaleY = scale;
				RescaleWidth(entity, maxWidth);
			}
		}
		private void RescaleWidth(GUIEntity entity, float maxWidth)
		{
			if(entity != null)
			{
				if((entity.Width * entity.ScaleX) > maxWidth)
				{
					entity.ScaleY = (entity.ScaleX *= (maxWidth / (entity.Width * entity.ScaleX)));
				}
			}
		}

		public void ShowResults(int restorationAchieved, int bonus, int total, bool fin)
		{
			_resultSuccessPercentage = (int)((GameControl.GetLastSuccessPercentage() * 100.0f) + 0.5f);
			_resultSuccessRequired = GameControl.PassPercentageForLevel(GameControl.GetCurrentLevel());

			//GUIEntity.SafeSetVisible(_paintingGame);
			//GUIEntity.SafeSetVisible(_imgScoreHolder, false);
			//GUIEntity.SafeSetVisible(_lblScore, false);

			//GUIEntity.SafeSetVisible(_resultsBorder, true);

			GUIButton.SafeEnable(_btnContinue, true);

			_scoreOffsetDesired = 1.0f;

			UpdateResults();
		}

		public void HideResults(bool immediate)
		{
			//GUIEntity.SafeSetVisible(_imgScoreHolder, true);
			//GUIEntity.SafeSetVisible(_lblScore, true);
			
			//GUIEntity.SafeSetVisible(_resultsBorder, false);

			GUIButton.SafeEnable(_btnContinue, false);
			_scoreOffsetDesired = 0.0f;

			UpdateGameUI();
		}

		private void Autosize()
		{
			float screenWidth = (float)Screen.width;
			float screenHeight = (float)Screen.height;
			

			float maxBorderHeight = screenHeight / 7.0f;

			float borderScale = DESIRED_BORDER_HEIGHT > maxBorderHeight ? (maxBorderHeight/DESIRED_BORDER_HEIGHT) : 1.0f;

			float topBorderHeight = DESIRED_BORDER_HEIGHT * borderScale;
			float bottomBorderHeight = topBorderHeight;

			float topScaleMod = 1.0f;
			float topWidthRequired = 5.0f * BOX_BORDER * borderScale;	//Space between each element!

			if(_lblCurrentLevel != null)
			{
				topWidthRequired += (_lblCurrentLevel.Width + BOX_BORDER) * borderScale;
			}

			if(_lblNextLevel != null)
			{
				topWidthRequired += _lblNextLevel.Width * borderScale;
			}

			if(_imgScoreHolder != null)
			{
				topWidthRequired += (_imgScoreHolder.Width + BOX_BORDER) * borderScale;
			}

			if(topWidthRequired > screenWidth)
			{
				topScaleMod = screenWidth/topWidthRequired;
			}

			topBorderHeight *= topScaleMod;

			if(_boxTop != null)
			{
				_boxTop.Autosize(screenWidth, topBorderHeight);
				_boxTop.Y = -(screenHeight * 0.5f) + (topBorderHeight * 0.5f);
			}

			if(_lblCurrentLevel != null)
			{
				_lblCurrentLevel.X = ((_lblCurrentLevel.Width * 0.5f) + BOX_BORDER) * borderScale * topScaleMod;
				_lblCurrentLevel.Y = topBorderHeight * 0.5f;
				_lblCurrentLevel.ScaleX = _lblCurrentLevel.ScaleY = borderScale * topScaleMod;
			}

			if(_lblNextLevel != null)
			{
				_lblNextLevel.Y = topBorderHeight * 0.5f;
				_lblNextLevel.ScaleX = _lblNextLevel.ScaleY = borderScale * topScaleMod;
			}


		
			float animOffset = 0.0f;

			if(_imgScoreHolder != null)
			{
				_imgScoreHolder.X = -((_imgScoreHolder.Width * 0.5f) + BOX_BORDER) * borderScale * topScaleMod;

				animOffset = -(_scoreOffsetCurrent * _imgScoreHolder.Height * _imgScoreHolder.ScaleY);
				_imgScoreHolder.Y = _imgScoreHolder.Height * _imgScoreHolder.ScaleY * 0.5f + animOffset;
				_imgScoreHolder.ScaleX = _imgScoreHolder.ScaleY = borderScale * topScaleMod;
			}

			if(_lblScore != null)
			{
				_lblScore.X = (_imgScoreHolder != null ? _imgScoreHolder.X : (-((_lblScore.Width * 0.5f) + BOX_BORDER) * borderScale * topScaleMod));
				_lblScore.Y = (topBorderHeight * 0.5f) + animOffset;
				_lblScore.ScaleX = _lblScore.ScaleY = borderScale * topScaleMod;
			}


			float bottomScaleMod = 1.0f;
			float bottomWidthRequired = BOX_BORDER * borderScale;	//Space between the two bits of text!

			if(_lblPaintingTitle != null)
			{
				bottomWidthRequired += (_lblPaintingTitle.Width + BOX_BORDER) * borderScale;
			}

			if(_lblPaintingSource != null)
			{
				bottomWidthRequired += (_lblPaintingSource.Width + BOX_BORDER) * borderScale;
			}

			if(bottomWidthRequired > screenWidth)
			{
				bottomScaleMod = screenWidth/bottomWidthRequired;
			}

			bottomBorderHeight *= bottomScaleMod;

			if(_boxBottom != null)
			{
				_boxBottom.Autosize(screenWidth, bottomBorderHeight);
				_boxBottom.Y = (screenHeight * 0.5f) - (bottomBorderHeight * 0.5f);
				
			}

			if(_lblPaintingTitle != null)
			{
				_lblPaintingTitle.X = ((_lblPaintingTitle.Width * 0.5f) + BOX_BORDER) * borderScale * bottomScaleMod;
				_lblPaintingTitle.Y = -bottomBorderHeight * 0.5f;
				_lblPaintingTitle.ScaleX = _lblPaintingTitle.ScaleY = borderScale * bottomScaleMod;
			}
			
			if(_lblPaintingSource != null)
			{
				_lblPaintingSource.X = -((_lblPaintingSource.Width * 0.5f) + BOX_BORDER) * borderScale * bottomScaleMod;
				_lblPaintingSource.Y = -bottomBorderHeight * 0.5f;
				_lblPaintingSource.ScaleX = _lblPaintingSource.ScaleY = borderScale * bottomScaleMod;
			}

			//Process results

			float resultsBorderWidth = 1.0f;
			float resultsBorderHeight = 1.0f;

			const float SCREEN_EDGE_WIDTH = 128.0f;

			if(_resultsBorder != null)
			{
				_resultsBorder.Autosize(Mathf.Max((screenWidth - (2.0f * SCREEN_EDGE_WIDTH) - (4.0f * BOX_BORDER)), 32.0f), (screenHeight - topBorderHeight - bottomBorderHeight - (2.0f * INNER_BORDER)));
				resultsBorderWidth = _resultsBorder.Width;
				resultsBorderHeight = _resultsBorder.Height;
			}

			float maxWidth = Mathf.Max(resultsBorderWidth - (2.0f * INNER_BORDER), 1.0f);

			float heightAvailable = resultsBorderHeight;

			if(_btnContinue != null)
			{
				float continueButtonScale = Mathf.Clamp(((screenHeight/5.0f) / _btnContinue.Height), 0.0f, 1.0f);

				if((_btnContinue.Width * continueButtonScale) > maxWidth)
				{
					continueButtonScale *= maxWidth/(_btnContinue.Width * continueButtonScale);
				}
				
				_btnContinue.ScaleX = _btnContinue.ScaleY = continueButtonScale;
				
				_btnContinue.Y = (resultsBorderHeight * 0.5f) - (_btnContinue.Height * 0.5f * continueButtonScale) - BOX_BORDER;

				heightAvailable -= (_btnContinue.Height * continueButtonScale) + (2.0f * INNER_BORDER);
				
				if(_boxContinueBorder != null)
				{
					_boxContinueBorder.Y = _btnContinue.Y;
					
					_boxContinueBorder.Autosize(_btnContinue.Width * continueButtonScale, _btnContinue.Height * continueButtonScale);
				}
			}


			float heightRequired = (2.0f * BOX_BORDER);

			if(_lblResultsTitle != null)
			{
				heightRequired += _lblResultsTitle.Height;
			}

			if(_lblRestorationAchievedTitle != null)
			{
				heightRequired += _lblRestorationAchievedTitle.Height;
			}

			if(_lblRestorationAchievedResult != null)
			{
				heightRequired += _lblRestorationAchievedResult.Height;
			}

			if(_lblBonusTitle != null)
			{
				heightRequired += _lblBonusTitle.Height;
			}
			
			if(_lblBonusResults != null)
			{
				heightRequired += _lblBonusResults.Height;
			}

			if(_lblCurrentScoreTitle != null)
			{
				heightRequired += _lblCurrentScoreTitle.Height;
			}
			
			if(_lblCurrentScoreResults != null)
			{
				heightRequired += _lblCurrentScoreResults.Height;
			}

			float resultsScale = 1.0f;

			if((heightRequired > 0) && (heightRequired > heightAvailable))
			{
				resultsScale = heightAvailable/heightRequired;
			}

			float resultTextStep = heightAvailable / 4.0f;

			if(_btnContinue != null)
			{
				_btnContinue.Y = (resultsBorderHeight * 0.5f) - (_btnContinue.Height * 0.5f * _btnContinue.ScaleY) - (BOX_BORDER * resultsScale);
				
				if(_boxContinueBorder != null)
				{
					_boxContinueBorder.Y = _btnContinue.Y;
				}
			}

			float resultTextY = -30.0f * resultsScale;

			if(_lblResultsTitle != null)
			{
				ApplyScale(_lblResultsTitle, resultsScale, maxWidth);

				_lblResultsTitle.Y = (-resultsBorderHeight* 0.5f) + (BOX_BORDER * resultsScale) + (_lblResultsTitle.Height * 0.5f * resultsScale);

				if(_btnContinue != null)
				{
					resultTextY += ((_lblResultsTitle.Y + _btnContinue.Y) * 0.5f);
				}

				if(_boxUnderline != null)
				{
					_boxUnderline.Y = _lblResultsTitle.Y + (30.0f * _lblResultsTitle.ScaleY );
					_boxUnderline.Autosize(_lblResultsTitle.Width * _lblResultsTitle.ScaleX, 4.0f);
				}
			}

		
			const float RESULT_TEXT_OFFSET = 20.0f;

			if(_lblRestorationAchievedTitle != null)
			{
				ApplyScale(_lblRestorationAchievedTitle, resultsScale, maxWidth);
				_lblRestorationAchievedTitle.Y = (resultTextY - (1.0f * resultTextStep)) - (RESULT_TEXT_OFFSET * _lblRestorationAchievedTitle.ScaleY);
			}
			if(_lblRestorationAchievedResult != null)
			{
				ApplyScale(_lblRestorationAchievedResult, resultsScale, maxWidth);
				_lblRestorationAchievedResult.Y = (resultTextY - (1.0f * resultTextStep)) + (RESULT_TEXT_OFFSET * _lblRestorationAchievedResult.ScaleY);;
			}

		
			if(_lblBonusTitle != null)
			{
				ApplyScale(_lblBonusTitle, resultsScale, maxWidth);
				_lblBonusTitle.Y = resultTextY - (RESULT_TEXT_OFFSET * _lblBonusTitle.ScaleY);
			}
			if(_lblBonusResults != null)
			{
				ApplyScale(_lblBonusResults, resultsScale, maxWidth);
				_lblBonusResults.Y = resultTextY + (RESULT_TEXT_OFFSET * _lblBonusResults.ScaleY);
			}


			if(_lblCurrentScoreTitle != null)
			{
				ApplyScale(_lblCurrentScoreTitle, resultsScale, maxWidth);
				_lblCurrentScoreTitle.Y = (resultTextY + (1.0f * resultTextStep)) - (RESULT_TEXT_OFFSET * _lblCurrentScoreTitle.ScaleY);
			}

			if(_lblCurrentScoreResults != null)
			{
				ApplyScale(_lblCurrentScoreResults, resultsScale, maxWidth);
				_lblCurrentScoreResults.Y = (resultTextY + (1.0f * resultTextStep)) + (RESULT_TEXT_OFFSET * _lblCurrentScoreResults.ScaleY);
			}
		}
		
		protected override void Draw(float parentAlpha)
		{
			Autosize();
		}


		private void UpdatePaintingInfo()
		{
			PaintingGame game = GameControl.GetPaintingGame();
			PaintingProvider.PaintingInfo info = (game != null ? game.CurrentPainting : null);

			GUIEntity.SafeSetVisible(_lblPaintingTitle, info != null);
			GUIEntity.SafeSetVisible(_lblPaintingSource, info != null);

			if(info != null)
			{
				GUILabel.SafeSetText(_lblPaintingTitle, NullCheck(info.title) + ", by " + NullCheck(info.author));
				if(_lblPaintingTitle != null)
				{
					_lblPaintingTitle.Autosize();
				}
				GUILabel.SafeSetText(_lblPaintingSource, NullCheck(info.source));
				if(_lblPaintingSource != null)
				{
					_lblPaintingSource.Autosize();
				}
			}
		}
		
		protected override void StartEnter()
		{
			UpdateGameUI();
		}

		private void UpdateGameUI()
		{
			SetPointsDisplay(GameControl.GetCurrentPointsDisplayAccount());
			
			int currentLevel = GameControl.GetCurrentLevel();
			
			if(_lblCurrentLevel != null)
			{
				_lblCurrentLevel.Text = "Level " + currentLevel.ToString();
				_lblCurrentLevel.Autosize();
			}
			
			if(_lblNextLevel != null)
			{
				_lblNextLevel.Text = "Next Level at " + GameControl.PassPercentageForLevel(currentLevel).ToString() + "%";
				_lblNextLevel.Autosize();
			}
			
			if(_effTitle != null)
			{
				_effTitle.SetToStart();
				_effTitle.Play(true, false);
			}
			
			UpdatePaintingInfo();
		}

		private void UpdateResults()
		{
			if(_lblRestorationAchievedResult != null)
			{
				_lblRestorationAchievedResult.Text = _resultSuccessPercentage.ToString() + "%";
				_lblRestorationAchievedResult.Autosize();
			}
			
			int bonus = GameControl.CalculateBonus(_resultSuccessPercentage);
			
			
			if(_lblBonusResults != null)
			{
				_lblBonusResults.Text = bonus.ToString();
				_lblBonusResults.Autosize();
			}
			
			int currentScore = GameControl.BankPoints(bonus);
			
			if(_lblCurrentScoreTitle != null)
			{
				_lblCurrentScoreTitle.Text = (ShouldAdvance() ? STR_SCORE_ADVANCE : STR_SCORE_FAILURE);
				_lblCurrentScoreTitle.Autosize();
			}
			
			if(_lblCurrentScoreResults != null)
			{
				_lblCurrentScoreResults.Text = currentScore.ToString();
				_lblCurrentScoreResults.Autosize();
			}

			if(_btnContinue != null)
			{
				if(ShouldAdvance())
				{
					_btnContinue.TextureNormal = _texContinueUp;
					_btnContinue.TextureTouched = _btnContinue.TextureDisabled = _texContinueDown;
				}
				else
				{
					_btnContinue.TextureNormal = _texFinishUp;
					_btnContinue.TextureTouched = _btnContinue.TextureDisabled = _textFinishDown;
				}

			}
			
			/*if(_lblResult != null)
			{
				_lblResult.Text = (ShouldAdvance() ? STR_ADVANCE : STR_FAILURE);
			}*/
			
			GameControl.PlaySFXResults(ShouldAdvance() ? 0 : 1);
		}
		
		protected override bool UpdateEnter(float deltaTime)
		{
			GUIEffect.SafeUpdate(_effTitle, deltaTime);
			this.Visible = true;
			return true;
		}
		
		protected override void StartExit()
		{
			
		}
		
		protected override void DoReset()
		{

		}

		public void UpdateBrushPosition(Brush brush, Camera currentCamera, float screenWidth, float screenHeight)
		{
			if(brush != null && _paintingGame != null && currentCamera != null)
			{
				Vector3 screenPosition = currentCamera.WorldToScreenPoint(brush.transform.position);;
				screenPosition.x -= ((float)Screen.width * 0.5f);
				screenPosition.y = (-(float)Screen.height * 0.5f) + ((float)Screen.height - screenPosition.y);
				
				screenPosition.x *= (SGGUI.GUIScreen.REF_WIDTH / screenWidth);
				screenPosition.y *= (SGGUI.GUIScreen.REF_HEIGHT / screenHeight);
				
				_paintingGame.X = screenPosition.x;
				_paintingGame.Y = screenPosition.y;
			}
		}

		public void StartGameInput(Brush brush, Camera currentCamera, float screenWidth, float screenHeight)
		{
			UpdateBrushPosition(brush, currentCamera, screenWidth, screenHeight);
			if(_paintingGame != null)
			{
				_paintingGame.Start();
			}
		}

		public SGGUI.PaintLineMiniGame.ClickResults StopGameInput()
		{
			if(_paintingGame != null)
			{
				return _paintingGame.Stop();
			}
			return null;
		}

		public void ShowParticle(int score, FlagParticles.eRating rating, Vector2 position)
		{
			if(_feedback != null)
			{
				_feedback.ShowParticle(score, rating, position);
			}
		}

		public void SetPointsDisplay(int points)
		{
			if(_lblScore != null)
			{
				_lblScore.Text = points.ToString("0000000");
				_lblScore.Autosize();
			}
		}

		public override void ActiveUpdate(float deltaTime)
		{
			GUIEffect.SafeUpdate(_effTitle, deltaTime);

			if(_scoreOffsetCurrent > _scoreOffsetDesired)
			{
				_scoreOffsetCurrent = Mathf.Lerp(_scoreOffsetCurrent, _scoreOffsetDesired - 0.1f, 0.1f);

				if(_scoreOffsetCurrent < _scoreOffsetDesired)
				{
					_scoreOffsetCurrent = _scoreOffsetDesired;
				}
			}
			else if(_scoreOffsetCurrent < _scoreOffsetDesired)
			{
				_scoreOffsetCurrent = Mathf.Lerp(_scoreOffsetCurrent, _scoreOffsetDesired + 0.1f, 0.1f);

				if(_scoreOffsetCurrent > _scoreOffsetDesired)
				{
					_scoreOffsetCurrent = _scoreOffsetDesired;
				}
			}

			if(_resultsBorder != null)
			{
				_resultsBorder.A = _scoreOffsetCurrent;
			}

		}

		private bool ShouldAdvance()
		{
			return (_resultSuccessPercentage >= _resultSuccessRequired);
		}

		public void Continue_OnPress(GUIEntity guiEntity)
		{
			if(_scoreOffsetCurrent == 1.0f)
			{
				PaintingGame game = GameControl.GetPaintingGame();
				if(game != null)
				{
					if(ShouldAdvance())
					{
						game.ContinueGame();
					}
					else
					{
						game.EndGame();
					}
				}
			}
			
			GameControl.PlaySFXTone(2);
		}
	}
}
