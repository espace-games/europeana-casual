﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class FlagParticles : GUIEntity
	{
		public enum eRating
		{
			Bad = 0, 
			Good,
			Wonderful
		}


		private class FlagParticleWrapper
		{
			private static float OVERLAP = 16.0f;

			private GUIEntity _parent = null;
			private GUIEntity _root = null;
			private GUIImage _flagMessage = null;
			private GUIImage _flagScore = null;
			private GUILabel _txtScore = null;
			private Vector2 _velocity = Vector2.zero;
			private float _lifeCurrent = 0.0f;
			private float _lifeMax = 1.0f;

			private Texture2D _texFlagBad = null;
			private Texture2D _texFlagGood = null;
			private Texture2D _texFlagWonderful = null;
			private Texture2D _texFlagScore = null;

			private GUIEffect _scaleEffect = null;


			public FlagParticleWrapper(GUIEntity parent, Texture2D texFlagBad, Texture2D texFlagGood, Texture2D texFlagWonderful, Texture2D texFlagScore)
			{
				_texFlagBad = texFlagBad;
				_texFlagGood = texFlagGood;
				_texFlagWonderful = texFlagWonderful;
				_texFlagScore = texFlagScore;

				_parent = parent;
				_root = new GUIEntity();

				_flagScore = (GUIImage)_root.AddChild(new GUIImage(0.0f, -2.0f, texFlagScore));
				_flagMessage = (GUIImage)_root.AddChild(new GUIImage(0.0f, 0.0f, _texFlagGood));

				_txtScore = (GUILabel)_root.AddChild(new GUILabel(0.0f, 0.0f, "000"));
				FontManager.ApplyFont_OpenSansLight30(_txtScore);


				_scaleEffect = new GUIScaleEffect(_root, 0.5f, GUIEffect.eAnim.SmoothStep, 0.0f, 1.0f);
				//_scaleEffect.AddSubEffect(new GUIAlphaEffect(_root, 0.1f, GUIEffect.eAnim.SmoothStep, 0.0f, 1.0f));

				Reset ();
			}

			public bool IsActive()
			{
				return (_root != null && _root.Visible);
			}

			public void Reset()
			{
				if(IsActive() && _parent != null)
				{
					_parent.RemoveChild(_root);
				}
				_lifeCurrent = 0.0f;

				_scaleEffect.Stop();
				_scaleEffect.SetToStart();
				GUIEntity.SafeSetVisible(_root, false);
			}

			public void Spawn(int score, eRating rating, Vector2 position)
			{
				Reset ();


				float directionMod = (position.y < 0.0f ? -1.0f : 1.0f);

				_root.X = position.x;
				_root.Y = position.y - 96.0f * directionMod;
				_lifeCurrent = _lifeMax = 3.0f;




				_velocity = new Vector2(0.0f, -256.0f * directionMod);

				if(_flagMessage != null)
				{
					switch(rating)
					{
					case eRating.Bad:
						_flagMessage.ImgTexture = _texFlagBad;
						break;
					case eRating.Good:
						_flagMessage.ImgTexture = _texFlagGood;
						break;
					case eRating.Wonderful:
						_flagMessage.ImgTexture = _texFlagWonderful;
						break;
					default:
						break;
					}

					_flagMessage.Autosize(_flagMessage.ImgTexture);
					_flagMessage.X = -OVERLAP + _flagMessage.Width * 0.5f;
				}


				_flagScore.X = OVERLAP - _flagScore.Width * 0.5f;
				_txtScore.X = _flagScore.X + 10.0f;
				_txtScore.Y = _flagScore.Y - 3.0f;
				_txtScore.Text = score.ToString();
				_txtScore.Autosize();

				GUIEntity.SafeSetVisible(_root, true);
				_parent.AddChild(_root);

				_scaleEffect.Play(true, false);
			}

			public void Update(float deltaTime)
			{
				if(IsActive())
				{
					_scaleEffect.Update(deltaTime);

					_lifeCurrent -= deltaTime;
					if(_lifeCurrent <= 0.0f)
					{
						Reset();
					}
					else
					{
						_root.X += _velocity.x * deltaTime;
						_root.Y += _velocity.y * deltaTime;

						float fadeTime = _lifeMax * 0.1f;
						_root.A = (_lifeCurrent < fadeTime) ? (_lifeCurrent/fadeTime) : 1.0f;
					}
				}
			}
		}

		private GUIImage _holder = null;

		private FlagParticleWrapper[] _particles = null;

		private Texture2D _texFlagBad = null;
		private Texture2D _texFlagGood = null;
		private Texture2D _texFlagWonderful = null;
		private Texture2D _texFlagScore = null;

		public FlagParticles(int maxParticles)
		{
			
			_texFlagBad = GUIEntity.LoadImage("flag_bad.png");
			_texFlagGood = GUIEntity.LoadImage("flag_good.png");
			_texFlagWonderful = GUIEntity.LoadImage("flag_wonderful.png");
			_texFlagScore = GUIEntity.LoadImage("flag_score.png");

			maxParticles = Mathf.Max(1, maxParticles);
			_particles = new FlagParticleWrapper[maxParticles];
			for(int i = 0; i <_particles.Length; ++i)
			{
				_particles[i] = new FlagParticleWrapper(this, _texFlagBad, _texFlagGood, _texFlagWonderful, _texFlagScore);
			}
		}

		protected override void Process(float deltaTime)
		{
			if(_particles != null)
			{
				for(int i = 0; i < _particles.Length; ++i)
				{
					_particles[i].Update(deltaTime);
				}
			}
		}

		public bool ShowParticle(int score, eRating rating, Vector2 position)
		{
			if(_particles != null)
			{
				for(int i = 0; i < _particles.Length; ++i)
				{
					if(_particles[i] != null && !_particles[i].IsActive())
					{
						_particles[i].Spawn(score, rating, position);

						return true;
					}
				}
			}

			return false;
		}
	}


}
