﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class InformationPage : Page
	{
		private static float SCROLL_DELAY = 3.0f * 1.0f;
		private static float SCROLL_SPEED = 32.0f * 1.0f;
		
		private bool _mouseHasBeenUp = false;
		
		private GUIBox _border = null;
		private GUIEntity _box = null;
		private GUIButton _btnBack = null;
		private GUIBox _boxButton = null;
		
		public static InformationPage Create(GUIEntity parent)
		{
			InformationPage page = null;
			if(parent != null)
			{
				page = (InformationPage)parent.AddChild(new InformationPage());
				page.Reset();
			}
			return page;
		}
		
		private GUILabel _lblTitle = null;
		private float _creditHeight = 0.0f;
		
		private Font _largeFont = null;
		private Font _smallFont = null;
		
		private float _scrollDelay = SCROLL_DELAY;
		
		private float _widestCredit = 0.0f;
		private float _currentCreditScale = 1.0f;
		
		protected InformationPage()
		{
			//this.IgnoreAutoScaling = true;
			_border = (SGGUI.GUIBox)AddChild(new SGGUI.GUIBox(1280.0f - (BOX_BORDER * 2.0f), 800.0f - (BOX_BORDER * 2.0f), new Color(0.0f, 0.0f, 0.0f, 0.5f)));
			_border.BorderSize = 4.0f;
			_border.IgnoreAutoScaling = true;
			
			//_box = AddChild(new GUIBox(0.0f, 0.0f, 900.0f, 700.0f, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_box = AddChild(new GUIEntity(0.0f, 0.0f));
			_box.IgnoreAutoScaling = true;
			
			GUILabel lblGeneral = null;
			
			
			float y = 0.0f;
			
			GUIImage logo = (SGGUI.GUIImage)_box.AddChild(new SGGUI.GUIImage(0.0f, y, "game_logo.png"));
			logo.ScaleX = logo.ScaleY = 0.75f;
			
			y = logo.Height * logo.ScaleY;
			logo.Y = y;
			
			_widestCredit = logo.Width * logo.ScaleX;
			
			y += 200.0f;
			
			//y = AddTitleContent("Commissioned By", "Europeana", y, ref _widestCredit);
			
			
			
			y = AddSubTitle("Europeana Space has received funding from the European Union's", true, y, ref _widestCredit);
			
			y -= 30.0f;
			
			y = AddSubTitle("ICT Policy Support Programme as part of the Competitiveness", true, y, ref _widestCredit);
			
			y -= 30.0f;
			
			y = AddSubTitle("and Innovation Framework Programme, under GA n° 621037", true, y, ref _widestCredit);
			
			y+= 30.0f;
			
			logo = (SGGUI.GUIImage)_box.AddChild(new SGGUI.GUIImage(0.0f, y, "espace_eu_logos.png"));
			logo.ScaleX = logo.ScaleY = 1.0f;
			
			y += logo.Height * logo.ScaleY;
			
			
			y = AddTitleContent("Developed By", " ", y, ref _widestCredit);
			
			y -= 10.0f;
			
			logo = (SGGUI.GUIImage)_box.AddChild(new SGGUI.GUIImage(0.0f, y, "sgil_cu_logos.png"));
			logo.ScaleX = logo.ScaleY = 0.75f;
			
			y += logo.Height * logo.ScaleY;
			
			
			/*y = AddTitleContent("Developed By", "Serious Games International Ltd", y, ref _widestCredit);
			y = AddTitleContent("In Association With", "Coventry University", y, ref _widestCredit);

			y += 20.0f;
*/
			
			
			y = AddSubTitle("All Music & Art Images Sourced Through The Europeana Archive", true, y, ref _widestCredit);
			
			
			
			y = AddTitleContent("Music", "Dancing (WoO.17) 1 - 3", y, ref _widestCredit);
			
			y = AddTitleContent("Composed By", "Ludwig van Beethoven", y, ref _widestCredit);
			y = AddTitleContent("Performed By", "Vienna State Opera & Franz Litschauer", y, ref _widestCredit);
			y = AddTitleContent("Provided By", "Netherlands Institute for Sound and Vision ", y, ref _widestCredit);
			
			y += 40.0f;
			
			y = AddSubTitle("Offine Art Images", false, y, ref _widestCredit);
			
			y += 20.0f;
			
			
			PaintingProvider provider = GameControl.GetOfflinePaintingProvider();
			{
				if(provider != null)
				{
					for(int i = 0; i < provider.GetPaintingCount(); ++i)
					{
						PaintingProvider.PaintingInfo info = provider.GetPaintingByIndex(i);
						if(info != null)
						{
							y = AddPaintingContent(info.title, info.author, info.source, y, ref _widestCredit);
						}
					}
				}
			}
			
			_creditHeight = y;
			
			_btnBack = (SGGUI.GUIButton)AddChild(new SGGUI.GUIButton(110.0f, -50.0f, "button_back_up.png", "button_back_down.png", "button_back_up.png"));
			_btnBack.Docking = eDock.BottomLeft;
			_btnBack.OnPressDelegate = Back_OnPress;
			_btnBack.IgnoreAutoScaling = true;
			
			
			_boxButton = (SGGUI.GUIBox)AddChild(new GUIBox(110.0f, -50.0f, _btnBack.Width, _btnBack.Height, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_boxButton.Docking = eDock.BottomLeft;
			_boxButton.BorderSize = 4.0f;
			_boxButton.IgnoreAutoScaling = true;
			
			//this.EnableClipping(0.0f, 0.0f, 200.0f, 200.0f);
		}
		
		
		private void Autosize()
		{
			float screenWidth = (float)Screen.width;
			float screenHeight = (float)Screen.height;
			
			float btnBackWidth = 0.0f;
			float btnBackHeight = 0.0f;
			float borderWidth = 0.0f;
			
			Rect clipping = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
			
			if(_btnBack != null)
			{
				float scale =  Mathf.Min ((0.5f / GUIScreen.GUIScale()), 1.0f);
				_btnBack.ScaleX = _btnBack.ScaleY = scale;
				_btnBack.X = BOX_BORDER + (_btnBack.Width * 0.5f * scale);
				_btnBack.Y = -BOX_BORDER - (_btnBack.Height * 0.5f * scale);
				
				btnBackWidth = _btnBack.Width * scale;
				btnBackHeight = _btnBack.Height * scale;
				
				if(_boxButton != null)
				{
					_boxButton.Autosize(btnBackWidth, btnBackHeight);
					_boxButton.X = _btnBack.X;
					_boxButton.Y = _btnBack.Y;
				}
			}
			
			
			
			if(_border != null)
			{
				_border.Autosize(Mathf.Max((screenWidth - (2.0f * btnBackWidth) - (4.0f * BOX_BORDER)), 32.0f), (screenHeight - (2.0f * BOX_BORDER)));
				borderWidth = _border.Width;
			}
			
			
			borderWidth = Mathf.Max(borderWidth - (2.0f * INNER_BORDER), 1.0f);
			
			_currentCreditScale = 1.0f;
			
			if(_box != null)
			{
				if(_widestCredit > 0.0f && _widestCredit > borderWidth)
				{
					_currentCreditScale = borderWidth/_widestCredit;
				}
				
				_box.ScaleX = _box.ScaleY = _currentCreditScale;
				
				
				for(int i = 0; i < _box.ChildCount; ++i)
				{
					GUIEntity child = _box.GetChildByIndex(i);
					if(child != null)
					{
						float childHeight = child.Height * child.ScaleY;
						float fadeDistanceMax = (screenHeight * 0.5f) - BOX_BORDER - (childHeight * 0.5f);
						float fadeDistanceMin = fadeDistanceMax - (childHeight * 0.25f);
						
						float dist = _box.Y + (child.Y * _currentCreditScale);
						
						dist = Mathf.Sqrt(dist * dist);
						
						
						child.A = (dist > fadeDistanceMin) ? (1.0f - Mathf.Clamp((dist-fadeDistanceMin)/(fadeDistanceMax-fadeDistanceMin), 0.0f, 1.0f)) : 1.0f;
					}
				}
				
			}
		}
		
		protected override void Draw(float parentAlpha)
		{
			Autosize();
		}
		
		public override void ActiveUpdate(float deltaTime)
		{
			if(_scrollDelay > 0.0f)
			{
				_scrollDelay -= deltaTime;
			}
			
			if(_scrollDelay <= 0.0f && _box != null)
			{
				_box.Y -= SCROLL_SPEED * deltaTime;
				
				if((_box.Y + (_creditHeight * _currentCreditScale)) < (-Screen.height/2))
				{
					
					_box.Y = (Screen.height/2);
				}
			}
			
			if(_mouseHasBeenUp)
			{
				
			}
			else if(!Input.GetMouseButton(0))
			{
				_mouseHasBeenUp = true;
			}
		}
		
		
		private float AddTitleContent(string title, string content, float y, ref float widestCredit)
		{
			if(_box != null && title != null && content != null)
			{
				GUILabel lblGeneral = null;
				
				lblGeneral = (GUILabel)_box.AddChild(new GUILabel(0.0f, y, title));
				FontManager.ApplyFont_OpenSansLight30(lblGeneral);
				
				if(lblGeneral.Width > widestCredit)
				{
					widestCredit = lblGeneral.Width;
				}
				
				y += 50.0f;
				
				lblGeneral = (GUILabel)_box.AddChild(new GUILabel(0.0f, y, content));
				FontManager.ApplyFont_OpenSansRegular48(lblGeneral);
				
				if(lblGeneral.Width > widestCredit)
				{
					widestCredit = lblGeneral.Width;
				}
				
				y += 80.0f;
				
				return y;
			}
			
			return y;
		}
		
		private float AddSubTitle(string subtitle, bool italic, float y, ref float widestCredit)
		{
			if(_box != null && subtitle != null)
			{
				GUILabel lblGeneral = null;
				
				lblGeneral = (GUILabel)_box.AddChild(new GUILabel(0.0f, y, subtitle));
				if(italic)
				{
					FontManager.ApplyFont_OpenSansLightItalic30(lblGeneral);
				}
				else
				{
					FontManager.ApplyFont_OpenSansLight30(lblGeneral);
				}
				
				if(lblGeneral.Width > widestCredit)
				{
					widestCredit = lblGeneral.Width;
				}
				
				y += 80.0f;
				
				return y;
			}
			
			return y;
		}
		
		private float AddPaintingContent(string title, string author, string source, float y, ref float widestCredit)
		{
			
			GUILabel lblGeneral = null;
			
			lblGeneral = (GUILabel)_box.AddChild(new GUILabel(0.0f, y, NullCheck(title)));
			FontManager.ApplyFont_OpenSansRegular48(lblGeneral);
			
			if(lblGeneral.Width > widestCredit)
			{
				widestCredit = lblGeneral.Width;
			}
			
			
			y += 80.0f;
			y = AddTitleContent("By", NullCheck(author), y, ref widestCredit);
			y = AddTitleContent("Provided By", NullCheck(source), y, ref widestCredit);
			y += 80.0f;
			return y;
		}
		
		protected override void StartEnter()
		{
			Autosize();
			_scrollDelay = SCROLL_DELAY;
			
			if(_box != null)
			{
				_box.Y = (float)(-Screen.height/2) + BOX_BORDER;
			}
			GUIButton.SafeEnable(_btnBack, true);
			_mouseHasBeenUp = false;
		}
		
		protected override bool UpdateEnter(float deltaTime)
		{
			this.Visible = true;
			return true;
		}
		
		protected override void StartExit()
		{
			GUIButton.SafeEnable(_btnBack, false);
		}
		
		protected override void DoReset()
		{
			
		}
		
		
		
		public void Back_OnPress(GUIEntity guiEntity)
		{
			GameUI gui = GameControl.GetGUI();
			if(gui)
			{
				gui.ShowPage(GameUI.ePages.Title);
			}
			GameControl.PlaySFXTone(0);
		}
	}
}


