﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SGGUI
{
	public class HelpPage : Page
	{
		private bool _mouseHasBeenUp = false;
		private GUIBox _border = null;
		private GUIButton _btnBack = null;
		private GUIBox _boxBackButton = null;


		public static HelpPage Create(GUIEntity parent)
		{
			HelpPage page = null;
			if(parent != null)
			{
				page = (HelpPage)parent.AddChild(new HelpPage());
				page.Reset();
			}
			return page;
		}
		
		private GUILabel _lblTitle = null;
		private float _titleIdealHeight = 1.0f;
		private List<GUILabel> _text = new List<GUILabel>();

		private static float START_Y = -300.0f;
		private static float PADDING = 50.0f;

		private Font _largeFont = null;
		private Font _smallFont = null;

		private float _errorTimer = 0.0f;

		
		protected HelpPage()
		{
			_border = (SGGUI.GUIBox)AddChild(new SGGUI.GUIBox(1280.0f - (BOX_BORDER * 2.0f), 800.0f - (BOX_BORDER * 2.0f), new Color(0.0f, 0.0f, 0.0f, 0.5f)));
			_border.BorderSize = 4.0f;
			_border.IgnoreAutoScaling = true;


			_lblTitle = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 240.0f, "Instructions"));
			_lblTitle.IgnoreAutoScaling = true;
			FontManager.ApplyFont_OpenSansRegular48(_lblTitle);
			_titleIdealHeight = _lblTitle.Height;


			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "The games requires speed and dexterity to maximise restoration")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "of each picture and to make the images and colours more vivid.")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "In order to score highly, a  player has to select the highlighted")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "button as quickly as possible (to change the tool being used to")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "restore the picture) and a series of sequential correct answers/tool")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "selections result in 100%.")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "The larger the area selected for restoration the more time the player")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "has to restore the painting, increasing the possibility of receiving 100%.")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "The line that sweeps along the selected area for restoration is the")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "the countdown line. The smaller the area that the player selects, the less")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "time one has to select the correct number of tools needed for restoration")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "which would result in a lower score and a less vivid result.")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "For each picture, there is a restoration % target that players must reach")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "to be able to move on to the next picture. This percentage increases for")));
			_text.Add( (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel(0.0f, 0.0f, "each new picture.")));

			foreach(GUILabel t in _text)
			{
				if(t != null)
				{
					t.IgnoreAutoScaling = true;
					FontManager.ApplyFont_OpenSansLight30(t);
				}
			}

			_btnBack = (SGGUI.GUIButton)AddChild(new SGGUI.GUIButton(110.0f, -50.0f, "button_back_up.png", "button_back_down.png", "button_back_up.png"));
			_btnBack.Docking = eDock.BottomLeft;
			_btnBack.OnPressDelegate = Back_OnPress;
			_btnBack.IgnoreAutoScaling = true;
			
			
			_boxBackButton = (SGGUI.GUIBox)AddChild(new GUIBox(110.0f, -50.0f, _btnBack.Width, _btnBack.Height, new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_boxBackButton.Docking = eDock.BottomLeft;
			_boxBackButton.BorderSize = 4.0f;
			_boxBackButton.IgnoreAutoScaling = true;
		}

		protected override void Draw(float parentAlpha)
		{
			float screenWidth = (float)Screen.width;
			float screenHeight = (float)Screen.height;
			
			float btnBackWidth = 0.0f;
			float btnBackHeight = 0.0f;
			float borderWidth = 0.0f;
			float borderHeight = 0.0f;
			
			Rect clipping = new Rect(0.0f, 0.0f, 0.0f, 0.0f);


			float btnScale =  Mathf.Min ((0.5f / GUIScreen.GUIScale()), 1.0f);
			if(_btnBack != null)
			{

				_btnBack.ScaleX = _btnBack.ScaleY = btnScale;
				_btnBack.X = BOX_BORDER + (_btnBack.Width * 0.5f * btnScale);
				_btnBack.Y = -BOX_BORDER - (_btnBack.Height * 0.5f * btnScale);
				
				btnBackWidth = _btnBack.Width * btnScale;
				btnBackHeight = _btnBack.Height * btnScale;
				
				if(_boxBackButton != null)
				{
					_boxBackButton.Autosize(btnBackWidth, btnBackHeight);
					_boxBackButton.X = _btnBack.X;
					_boxBackButton.Y = _btnBack.Y;
				}
			}


			if(_border != null)
			{
				_border.Autosize(Mathf.Max((screenWidth - (2.0f * btnBackWidth) - (4.0f * BOX_BORDER)), 32.0f), (screenHeight - (2.0f * BOX_BORDER)));
				borderWidth = _border.Width;
				borderHeight = _border.Height;
			}

			float maxWidth = Mathf.Max (borderWidth - (2.0f * BOX_BORDER), 1.0f);
			float maxHeight = Mathf.Max (borderHeight - (2.0f * BOX_BORDER), 1.0f);

					
			float lblTitleHeight = 30.0f;

			float widthRequired = 1000.0f;
			float heightRequired = 680.0f;


			float xScale = 1.0f;
			if(widthRequired > 0.0f && widthRequired > borderWidth)
			{
				xScale = borderWidth/widthRequired;
			}

			float yScale = 1.0f;
			if(heightRequired > 0.0f && heightRequired > borderHeight)
			{
				xScale = borderHeight/heightRequired;
			}

			float scale = Mathf.Min (xScale, yScale);
			
			
			if(_lblTitle != null)
			{
				lblTitleHeight = _lblTitle.Height * _lblTitle.ScaleY;
				_lblTitle.Y = (-screenHeight* 0.5f) + BOX_BORDER + INNER_BORDER + (lblTitleHeight * 0.5f);
				_lblTitle.ScaleX = _lblTitle.ScaleY = scale;

				/*heightRequired += _lblTitle.Height * scale;

				if(heightRequired > 0.0f && heightRequired > maxHeight)
				{
					scale *= maxHeight/heightRequired;
					
					_lblTitle.ScaleX = _lblTitle.ScaleY = scale;



				}*/

				float lineHeight = 30.0f * scale;

				if(_text != null)
				{
					for(int i = 0; i < _text.Count; ++i)
					{
						if(_text[i] != null)
						{
							_text[i].ScaleX = _text[i].ScaleY = scale;
							_text[i].Y = _lblTitle.Y + lblTitleHeight + i * lineHeight;
						}
					}
				}
			}
		}
	
		
		protected override void StartEnter()
		{
			GUIButton.SafeEnable(_btnBack, true);
			_mouseHasBeenUp = false;
		}
		
		protected override bool UpdateEnter(float deltaTime)
		{
			this.Visible = true;
			return true;
		}
		
		protected override void StartExit()
		{
			GUIButton.SafeEnable(_btnBack, false);
		}
		
		protected override void DoReset()
		{
			
		}
		

		public override void ActiveUpdate(float deltaTime)
		{
			if(_mouseHasBeenUp)
			{

			}
			else if(!Input.GetMouseButton(0))
			{
				_mouseHasBeenUp = true;
			}
		}

		public void Back_OnPress(GUIEntity guiEntity)
		{
			GameControl.SetState(GameControl.eState.OnTitleScreen);
			GameControl.PlaySFXTone(0);
		}

		public void Play_OnPress(GUIEntity guiEntity)
		{
			GameControl.SetState(GameControl.eState.OnTitleScreen);
		}
	}
}


