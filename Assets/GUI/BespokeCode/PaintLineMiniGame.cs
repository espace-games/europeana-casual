﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class PaintLineMiniGame : GUIEntity
	{
		public class ClickResults
		{
			private int _badClickCount = 0;
			private int _okClickCount = 0;
			private int _goodClickCount = 0;

			public int BadCount
			{
				get { return _badClickCount; }
			}

			public int OKCount
			{
				get { return _okClickCount; }
			}

			public int GoodCount
			{
				get { return _goodClickCount; }
			}

			public int TotalClicks
			{
				get { return BadCount + OKCount + GoodCount; }
			}

			public ClickResults(int badCount, int okCount, int goodCount)
			{
				_badClickCount = badCount;
				_okClickCount = okCount;
				_goodClickCount = goodCount;
			}
		}

		private GUIImage _holder = null;
		private GUIButton[] _clickZones = null;
		private int _lastRandom = -1;
		private bool _active = false;
		private float _timer = 0.0f;
		//private float _clickProgress = 0.0f;
		private int[] _clickCounts = null;

		//private string[] _resultsPhrases = {"Philistine!", "Terrible!", "Admissable!", "Delightful!", "Exquisite!"};
		private static string[] RESULTS_PHRASES = {"Bad!", "Ok!", "Wonderful!"};
		private static float[] RESULTS_TIMES = {GameControl.TIME_WORST, GameControl.TIME_AVERAGE, GameControl.TIME_BEST};
		private static Color[] RESULTS_COLOURS = { Color.red, Color.white, Color.green };
		private static float NO_PROGRESS_TIME = 2.5f;
		//public static float[] PROGRESS_AMOUNTS = {0.25f, 0.5f, 1.0f};

		private void ClearClickCount()
		{
			if(_clickCounts == null)
			{
				_clickCounts = new int[RESULTS_TIMES.Length];
			}

			for(int i = 0; i < _clickCounts.Length; ++i)
			{
				_clickCounts[i] = 0;
			}
		}

		public PaintLineMiniGame()
		{
			_holder = (GUIImage)AddChild(new GUIImage(0.0f, 0.0f, "button_grid.png"));

			ClearClickCount();

			_clickZones = new GUIButton[4];

			float size = 74.0f;
			_clickZones[0] = (GUIButton)AddChild(new GUIButton(0.0f, -size, "button_up_up.png", null, null));
			_clickZones[1] = (GUIButton)AddChild(new GUIButton(size, 0.0f, "button_right_up.png", null, null));
			_clickZones[2] = (GUIButton)AddChild(new GUIButton(0.0f, size, "button_down_up.png", null, null));
			_clickZones[3] = (GUIButton)AddChild(new GUIButton(-size, 0.0f, "button_left_up.png", null, null));

			for(int i = 0; i < _clickZones.Length; ++i)
			{
				if(_clickZones[i] != null)
				{
					_clickZones[i].PixelPerfectCollision = true;
					_clickZones[i].OnFirstTouchDelegate = OnZonePressDelegate;
				}
			}


			DisableAll();
			EnableRandom();
		}

		public void Start()
		{
			DisableAll();
			EnableRandom();

			this.Visible = true;
			_timer = 0.0f;
			//_clickProgress = 0;
			ClearClickCount();
			_active = true;

		}

		public ClickResults Stop()
		{
			DisableAll();

			this.Visible = false;

			_active = false;

			//return _clickProgress;


			int badCount = 0;
			int okCount = 0;
			int goodCount = 0;

			if(_clickCounts != null && _clickCounts.Length >= 3)
			{
				badCount = _clickCounts[0];
				okCount = _clickCounts[1];
				goodCount = _clickCounts[2];

			}

			return new ClickResults(badCount, okCount, goodCount);
		}

		protected override void Process(float deltaTime)
		{
			if(_active)
			{
				_timer += deltaTime;
			}
		}

		public void OnZonePressDelegate(GUIEntity guiEntity, float x, float y)
		{
			if(RESULTS_TIMES != null && RESULTS_PHRASES != null && RESULTS_TIMES.Length == RESULTS_PHRASES.Length && RESULTS_TIMES.Length > 0)
			{
				int resultIndex = RESULTS_TIMES.Length-1;

				while(_timer > RESULTS_TIMES[resultIndex] && resultIndex > 0)
				{
					resultIndex--;
				}

				Vector2 position = new Vector2(this.X, this.Y);
				Vector2 direction = ClickZoneToVelocity();


				int points = GameControl.PAINT_LINE_MINI_GAME_SCORES[resultIndex] * GameControl.GetCurrentLevel();
				GameControl.AddRoundPoints(points, position);

				GameUI gui = GameControl.GetGUI();
				if(gui != null)
				{
					//gui.ShowFeedBackText(RESULTS_PHRASES[resultIndex], 3.0f, RESULTS_COLOURS[resultIndex], position + direction * 80.0f, ClickZoneToRotation(), direction * 200.0f);
					gui.ShowFeedBackText(points, resultIndex == 0 ? FlagParticles.eRating.Bad : (resultIndex == 2 ? FlagParticles.eRating.Wonderful : FlagParticles.eRating.Good), position);
				}

				//_clickProgress += ClickSuccess(_timer, resultIndex);
				if(_clickCounts != null)
				{
					_clickCounts[resultIndex]++;
				}

				GameControl.PlaySFXTone(resultIndex);
			}

			_timer = 0.0f;



			EnableRandom();
		}

		/*private float ClickSuccess(float timer, int resultIndex)
		{
			if(timer >= NO_PROGRESS_TIME || resultIndex < 0 || resultIndex >= PROGRESS_AMOUNTS.Length)
			{
				return 0.0f;
			}
			return PROGRESS_AMOUNTS[resultIndex];
		}*/

		private void DisableAll()
		{
			if(_clickZones != null)
			{
				for(int i = 0; i < _clickZones.Length; ++i)
				{
					if(_clickZones[i] != null)
					{
						_clickZones[i].Enabled = false;
					}
				}
			}
		}

		private void EnableRandom()
		{
			DisableAll();

			if(_clickZones != null && _clickZones.Length > 0)
			{
				int random = Random.Range(0, _clickZones.Length-1);
				if(random == _lastRandom)
				{
					random = (random + 2)%_clickZones.Length;
				}

				_lastRandom = random;

				GUIEntity.SafeEnable(_clickZones[_lastRandom], true);
			}
		}

		private Vector2 ClickZoneToVelocity()
		{
			switch(_lastRandom)
			{
			case 0:
				return new Vector2(-1.0f, -1.0f);
			case 1:
				return new Vector2(1.0f, -1.0f);
			case 2:
				return new Vector2(1.0f, 1.0f);
			case 3:
				return new Vector2(-1.0f, 1.0f);
			default:
				break;
			}
			return Vector2.zero;
		}

		private float ClickZoneToRotation()
		{
			switch(_lastRandom)
			{
			case 0:
				return -45.0f;
			case 1:
				return 45.0f;
			case 2:
				return 135.0f;
			case 3:
				return 225.0f;
			default:
				break;
			}
			return 0.0f;
		}
	}


}
