﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public abstract class Page : GUIEntity
	{
		public static float BOX_BORDER = 40.0f;
		public static float INNER_BORDER = 10.0f;
		public static string STR_UNKNOWN = "Unknown";


		public static string NullCheck(string str)
		{
			return (str == null ? STR_UNKNOWN : str);
		}

		protected enum eState
		{
			Hidden = 0,
			Entering,
			OnScreen,
			Exiting
		}

		private eState _state = eState.Hidden;

		private GUIAlphaEffect _enterEffect = null;

		public static float STROKE_SIZE = 1.0f;
		public static Color STROKE_COLOUR = new Color(0.0f, 0.0f, 0.0f, 0.9f);

		protected Page()
		{
			_enterEffect = new GUIAlphaEffect(this, 0.5f, GUIEffect.eAnim.SmoothStep, 0.0f, 1.0f);
			_enterEffect.SetToStart();
			this.Visible = false;
		}

		public void Enter()
		{
			if(_enterEffect != null)
			{
				_enterEffect.Play(true, true);
			}
			this.Visible = true;
			StartEnter();
			_state = eState.Entering;
		}

		public void Exit()
		{
			if(_enterEffect != null)
			{
				_enterEffect.Play(false, true);
			}
			StartExit();
			_state = eState.Exiting;
		}

		public void Reset()
		{
			DoReset();
			_state = eState.Hidden;
		}

		protected override void Process (float deltaTime)
		{
			GUIEffect.SafeUpdate(_enterEffect, deltaTime);

			switch(_state)
			{
			case eState.Entering:
				if(UpdateEnter(deltaTime))
				{
					_state = eState.OnScreen;
				}
				break;
			case eState.Exiting:
				if(UpdateExit(deltaTime))
				{
					Reset();
				}
				break;
			default:
				break;
			}
		}

		protected GUILabel CreateTitle(float x, float y, string title)
		{
			return CreateTitle(this, x, y, title);
		}

		protected GUILabel CreateTitle(GUIEntity parent, float x, float y, string title)
		{
			if(parent != null)
			{
				GUILabel lbl = (SGGUI.GUILabel)parent.AddChild(new SGGUI.GUILabel(x, y, title));
				lbl.Style.fontSize = 64;

				GameUI gui = GameControl.GetGUI();
				if(gui != null && gui.fontLarge != null)
				{
					lbl.Style.font = gui.fontLarge;
				}
				lbl.Stroke = STROKE_SIZE;
				lbl.StrokeColour = STROKE_COLOUR;
				lbl.Docking = SGGUI.GUIEntity.eDock.MiddleMiddle;
				lbl.Style.alignment = TextAnchor.MiddleCenter;
				//lbl.UseParentAlpha = false;
				lbl.Style.fontStyle = FontStyle.Bold;
				lbl.DropShadow = true;

				return lbl;
			}
			return null;
		}

		protected GUILabel CreateSubTitle(float x, float y, string title)
		{
			return CreateSubTitle(this, x, y, title);
		}

		protected GUILabel CreateSubTitle(GUIEntity parent, float x, float y, string title)
		{
			if(parent != null)
			{
				GUILabel lbl = (SGGUI.GUILabel)parent.AddChild(new SGGUI.GUILabel(x, y, title));
				lbl.Style.fontSize = 48;

				GameUI gui = GameControl.GetGUI();
				if(gui != null && gui.fontMedium != null)
				{
					lbl.Style.font = gui.fontMedium;
				}

				lbl.Stroke = STROKE_SIZE;
				lbl.StrokeColour = STROKE_COLOUR;
				lbl.Docking = SGGUI.GUIEntity.eDock.MiddleMiddle;
				lbl.Style.alignment = TextAnchor.MiddleCenter;
				//lbl.UseParentAlpha = false;
				lbl.Style.fontStyle = FontStyle.Bold;
				lbl.DropShadow = true;
				
				return lbl;
			}
			return null;
		}

		protected GUILabel CreateNormalText(float x, float y, string title)
		{
			return CreateNormalText(this, x, y, title);
		}
		
		protected GUILabel CreateNormalText(GUIEntity parent, float x, float y, string title)
		{
			if(parent != null)
			{
				GUILabel lbl = (SGGUI.GUILabel)parent.AddChild(new SGGUI.GUILabel(x, y, title));
				lbl.Style.fontSize = 32;

				GameUI gui = GameControl.GetGUI();
				if(gui != null && gui.fontSmall != null)
				{
					lbl.Style.font = gui.fontSmall;
				}

				lbl.Stroke = STROKE_SIZE;
				lbl.StrokeColour = STROKE_COLOUR;
				lbl.Docking = SGGUI.GUIEntity.eDock.MiddleMiddle;
				lbl.Style.alignment = TextAnchor.MiddleCenter;
				//lbl.UseParentAlpha = false;
				lbl.DropShadow = true;
				
				return lbl;
			}
			return null;
		}

		protected abstract void StartEnter();
		protected abstract bool UpdateEnter(float deltaTime);
		protected abstract void StartExit();
		protected virtual bool UpdateExit(float deltaTime)
		{
			if(_enterEffect == null || !_enterEffect.IsPlaying)
			{
				this.Visible = false;
				return true;
			}
			return false;
		}

		public abstract void ActiveUpdate(float deltaTime);

		protected abstract void DoReset();

	}
}
