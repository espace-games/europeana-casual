﻿using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class TitlePage : Page
	{
		public static TitlePage Create(GUIEntity parent)
		{
			TitlePage page = null;
			if(parent != null)
			{
				page = (TitlePage)parent.AddChild(new TitlePage());
				page.Reset();
			}
			return page;
		}

		private GUIBox _border = null;
		private GUIImage _imgTitle = null;
		private GUIImage _imgClickToBegin = null;
		private GUIEffect _effClickToBegin = null;
		private GUIButton _btnInformation = null;
		private GUIButton _btnHelp = null;
		private GUILabel _lblCopyright = null;

		private bool _mouseHasBeenUp = false;
		private bool _informationButtonTouched = false;
		private bool _helpButtonTouched = false;

		protected TitlePage()
		{
			_border = (SGGUI.GUIBox)AddChild(new SGGUI.GUIBox(1280.0f - (BOX_BORDER * 2.0f), 800.0f - (BOX_BORDER * 2.0f), new Color(0.0f, 0.0f, 0.0f, 0.0f)));
			_border.BorderSize = 4.0f;
			_border.IgnoreAutoScaling = true;

			_imgTitle = (SGGUI.GUIImage)AddChild(new SGGUI.GUIImage(0.0f, 0.0f, "game_logo.png"));
			_imgTitle.IgnoreAutoScaling = true;
			_imgClickToBegin = (SGGUI.GUIImage)AddChild(new SGGUI.GUIImage(0.0f, 300.0f, "click_to_begin.png"));
			_imgClickToBegin.IgnoreAutoScaling = true;
			_effClickToBegin = new GUIAlphaEffect(_imgClickToBegin, 1.0f, GUIEffect.eAnim.SmoothStep, 0.0f, 1.0f);
			_effClickToBegin.SetToStart();

			_btnInformation = (SGGUI.GUIButton)AddChild(new SGGUI.GUIButton("button_information_up.png", "button_information_down.png", "button_information_up.png"));
			_btnInformation.Docking = eDock.BottomLeft;
			_btnInformation.OnFirstTouchDelegate = Information_OnFirstTouch;
			_btnInformation.OnPressDelegate = Information_OnPress;
			_btnInformation.IgnoreAutoScaling = true;
			_btnInformation.X = BOX_BORDER + (_btnInformation.Width * 0.5f) + INNER_BORDER;
			_btnInformation.Y = -BOX_BORDER - (_btnInformation.Height * 0.5f) - INNER_BORDER;

			_btnHelp = (SGGUI.GUIButton)AddChild(new SGGUI.GUIButton("button_help_up.png", "button_help_down.png", "button_help_up.png"));
			_btnHelp.Docking = eDock.BottomRight;
			_btnHelp.OnFirstTouchDelegate = Help_OnFirstTouch;
			_btnHelp.OnPressDelegate = Help_OnPress;
			_btnHelp.IgnoreAutoScaling = true;
			_btnHelp.X = BOX_BORDER + (_btnHelp.Width * 0.5f) + INNER_BORDER;
			_btnHelp.Y = -BOX_BORDER - (_btnHelp.Height * 0.5f) - INNER_BORDER;

			_lblCopyright = (SGGUI.GUILabel)AddChild(new SGGUI.GUILabel("Copyright © 2015 Serious Games International Ltd"));
			_lblCopyright.Style.alignment = TextAnchor.MiddleCenter;
			_lblCopyright.IgnoreAutoScaling = true;
			_lblCopyright.Docking = eDock.BottomMiddle;

			FontManager.ApplyFont_OpenSansLight12(_lblCopyright);
		}
		
		protected override void StartEnter()
		{
			_mouseHasBeenUp = false;
			_informationButtonTouched = false;
			_helpButtonTouched = false;

			if(_effClickToBegin != null)
			{
				_effClickToBegin.SetToStart();
				_effClickToBegin.Play(true, false);
			}

			GUIButton.SafeEnable(_btnInformation, true);
			GUIButton.SafeEnable(_btnHelp, true);
		}
		
		protected override bool UpdateEnter(float deltaTime)
		{
			this.Visible = true;
			return true;
		}
		
		protected override void StartExit()
		{
			GUIButton.SafeEnable(_btnInformation, false);
			GUIButton.SafeEnable(_btnHelp, false);
		}

		protected override void DoReset()
		{

		}

		protected override void Draw(float parentAlpha)
		{
			float screenWidth = (float)Screen.width;
			float screenHeight = (float)Screen.height;

			if(_border != null)
			{
				_border.Autosize((screenWidth - (2.0f * BOX_BORDER)), (screenHeight - (2.0f * BOX_BORDER)));
			}

			if(_btnInformation != null)
			{
				float scale = Mathf.Min ((1.0f / GUIScreen.GUIScale()), 0.5f);
				_btnInformation.ScaleX = _btnInformation.ScaleY = scale;
				_btnInformation.X = BOX_BORDER + (_btnInformation.Width * 0.5f * scale) + INNER_BORDER;
				_btnInformation.Y = -BOX_BORDER - (_btnInformation.Height * 0.5f * scale) - INNER_BORDER;
			}

			if(_btnHelp != null)
			{
				float scale = Mathf.Min ((1.0f / GUIScreen.GUIScale()), 0.5f);
				_btnHelp.ScaleX = _btnHelp.ScaleY = scale;
				_btnHelp.X = -BOX_BORDER - (_btnHelp.Width * 0.5f * scale) - INNER_BORDER;
				_btnHelp.Y = -BOX_BORDER - (_btnHelp.Height * 0.5f * scale) - INNER_BORDER;;
			}

			if(_imgTitle != null)
			{
				_imgTitle.Y = -screenHeight * 0.25f;

				float scale = ((screenHeight/5.0f) / _imgTitle.Height);

				float maxSize = Mathf.Max (screenWidth - (2.0f * BOX_BORDER) - (2.0f * INNER_BORDER), 1.0f);

				if((_imgTitle.Width * scale) > maxSize)
				{
					scale *= maxSize / (_imgTitle.Width * scale);
				}

				_imgTitle.ScaleX = _imgTitle.ScaleY = scale;


				float yLimit = (-screenHeight * 0.5f) + BOX_BORDER + INNER_BORDER + (_imgTitle.Height * scale * 0.5f);

				if(_imgTitle.Y < yLimit)
				{
					_imgTitle.Y = yLimit;
				}


			}

			if(_imgClickToBegin != null)
			{
				_imgClickToBegin.Y = screenHeight * 0.25f;

				float scale = ((screenHeight/5.0f) / _imgClickToBegin.Height);
				
				float maxSize = Mathf.Max (screenWidth - (2.0f * BOX_BORDER) - (2.0f * INNER_BORDER), 1.0f);
				
				if((_imgClickToBegin.Width * scale) > maxSize)
				{
					scale *= maxSize / (_imgClickToBegin.Width * scale);
				}
				
				_imgClickToBegin.ScaleX = _imgClickToBegin.ScaleY = scale;

				float yLimit = (screenHeight * 0.5f) - BOX_BORDER - INNER_BORDER - (_imgClickToBegin.Height * scale * 0.5f);
				
				if(_imgClickToBegin.Y > yLimit)
				{
					_imgClickToBegin.Y = yLimit;
				}
			}

			if(_lblCopyright != null)
			{
				_lblCopyright.Y = -BOX_BORDER * 0.5f;

				float scale = 1.0f;
				float maxSize = Mathf.Max (screenWidth - (2.0f * BOX_BORDER), 1.0f);
				
				if((_lblCopyright.Width * scale) > maxSize)
				{
					scale *= maxSize / (_lblCopyright.Width * scale);
				}
				_lblCopyright.ScaleX = _lblCopyright.ScaleY = scale;
			}
		}

		public override void ActiveUpdate(float deltaTime)
		{
			if(_effClickToBegin != null)
			{
				_effClickToBegin.Update(deltaTime);
				if(!_effClickToBegin.IsPlaying)
				{
					_effClickToBegin.Play(!_effClickToBegin.IsPlayingForward, false);
				}

			}

			if(_mouseHasBeenUp)
			{
				if(!_informationButtonTouched && !_helpButtonTouched)
				{
					if(Input.GetMouseButtonDown(0))
					{
						//GameControl.SetState(GameControl.eState.PlayingGame);
						GameControl.SetState(GameControl.eState.SelectingSource);
					}
				}
				_informationButtonTouched = false;
				_helpButtonTouched = false;
			}
			else if(!Input.GetMouseButton(0))
			{
				_mouseHasBeenUp = true;
			}
				
		}

		public void Information_OnPress(GUIEntity guiEntity)
		{
			GameUI gui = GameControl.GetGUI();
			if(gui)
			{
				gui.ShowPage(GameUI.ePages.Information);
			}

			GameControl.PlaySFXTone(2);
		}
		
		public void Information_OnFirstTouch(GUIEntity guiEntity, float x, float y)
		{
			_informationButtonTouched = true;
		}

		public void Help_OnPress(GUIEntity guiEntity)
		{
			GameUI gui = GameControl.GetGUI();
			if(gui)
			{
				gui.ShowPage(GameUI.ePages.Instructions);
			}
			
			GameControl.PlaySFXTone(2);
		}

		public void Help_OnFirstTouch(GUIEntity guiEntity, float x, float y)
		{
			_helpButtonTouched = true;
		}
	}
}

