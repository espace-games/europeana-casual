using System;

namespace SGGUI
{
	public class GUITable : GUIUserEditableEntity
	{
		
		
		
		private UnityEngine.GUIStyle _style = null;
		private int _characterLimit = int.MaxValue;
		
		private String[,] _cellText = null;
		private String[,] _oldCellText = null;
		private bool[,] _locked = null;
		private float _cellWidth = 64.0f;
		private float _cellHeight = 32.0f;
		
		public UnityEngine.GUIStyle Style
		{
			get { return _style;}
			set { _style = value;}
		}
		
		//Character limit seem to break new line input
		
		public int CharacterLimit
		{
			get { return _characterLimit; }
			set { _characterLimit = value; }
		}
		
		public int RowCount
		{
			get { return (_cellText != null ? _cellText.GetLength(0) : 0); }
		}
		
		public int ColumnCount
		{
			get { return (_cellText != null ? _cellText.GetLength(1) : 0); }
		}
		
		public bool DoAllCellsHaveText()
		{
			if(_cellText != null)
			{
				for(int i = 0; i < _cellText.GetLength(0); ++i)
				{
					for(int j = 0; j < _cellText.GetLength(1); ++j)
					{
						String text = _cellText[i, j];
						if(text == null || text.Length <= 0)
						{
							return false;	
						}
					}
				}
			}
			return true;
		}
		
		public bool GetCellLocked(int row, int column)
		{
			return ((_locked != null && row >= 0 && row < _locked.GetLength(0) && column >= 0 && column < _locked.GetLength(1)) ? _locked[row, column] : false);
		}
		
		public void SetCellLocked(int row, int column, bool locked)
		{
			if(_locked != null && row >= 0 && row < _locked.GetLength(0) && column >= 0 && column < _locked.GetLength(1))
			{
				_locked[row, column] = locked;
			}
		}
		
		public string GetTextInCell(int row, int column)
		{
			if(_cellText != null && row >= 0 && row < _cellText.GetLength(0) && column >= 0 && column < _cellText.GetLength(1))
			{
				return _cellText[row, column];	
			}
			return null;
		}
		
		public void SetTextInCell(int row, int column, string text)
		{
			if(_cellText != null && row >= 0 && row < _cellText.GetLength(0) && column >= 0 && column < _cellText.GetLength(1))
			{
				_cellText[row, column] = text;
			}
		}
		
		
		public string DataAsCSV()
		{
			string  csv = "";
			if(_cellText != null)
			{
				
				for(int row = 0; row < _cellText.GetLength(0); ++row)
				{
					csv += "{";
					for(int column = 0; column < _cellText.GetLength(1); ++column)
					{
						csv += (_cellText[row, column] != null ? ((column > 0 ) ? "," : "") + _cellText[row, column] : ((column > 0 ) ? "," : "") + "");	
					}
					
					csv += "}";
				}
			}
			return csv;
		}
		
		public GUITable (int rows, int columns, float cellWidth, float cellHeight, string defaultValue)
		{
			Initialise(rows, columns, cellWidth, cellHeight, defaultValue);
		}
		
		public GUITable (float x, float y, int rows, int columns, float cellWidth, float cellHeight, string defaultValue)
			: base(x, y)
		{
			Initialise(rows, columns, cellWidth, cellHeight, defaultValue);
		}
		
		protected override void Draw(float parentAlpha)
		{
			bool dataChanged = false;
			
			if(_cellText != null)
			{	
				if(_style != null)
				{
					_style.normal.textColor = UnityEngine.GUI.color;
					
					for(int row = 0; row < _cellText.GetLength(0); ++row)
					{
						for(int column = 0; column < _cellText.GetLength(1); ++column)
						{
							if(Enabled && !GetCellLocked(row, column))
							{
								_cellText[row, column] = UnityEngine.GUI.TextArea(new UnityEngine.Rect(-PivotX + ((float)column * _cellWidth), -PivotY + ((float)row * _cellHeight), _cellWidth, _cellHeight), _cellText[row, column], _characterLimit, _style);
								if(_cellText[row, column] != _oldCellText[row, column])
								{
									dataChanged = true;
								}
								_oldCellText[row, column] = _cellText[row, column];
							}
							else
							{
								UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + ((float)column * _cellWidth), -PivotY + ((float)row * _cellHeight), _cellWidth, _cellHeight), _cellText[row, column], _style);
							}
						}
					}
				}
				else
				{
					for(int row = 0; row < _cellText.GetLength(0); ++row)
					{
						for(int column = 0; column < _cellText.GetLength(1); ++column)
						{
							if(Enabled && !GetCellLocked(row, column))
							{
								_cellText[row, column] = UnityEngine.GUI.TextArea(new UnityEngine.Rect(-PivotX + ((float)column * _cellWidth), -PivotY + ((float)row * _cellHeight), _cellWidth, _cellHeight), _cellText[row, column], _characterLimit);
								if(_cellText[row, column] != _oldCellText[row, column])
								{
									dataChanged = true;
								}
								_oldCellText[row, column] = _cellText[row, column];
							}
							else
							{
								UnityEngine.GUI.TextArea(new UnityEngine.Rect(-PivotX + ((float)column * _cellWidth), -PivotY + ((float)row * _cellHeight), _cellWidth, _cellHeight), _cellText[row, column]);
							}
						}
					}
				}
			
			}
			
			if(dataChanged)
			{
				_dataChangedFlag = true;
			}
		}
		
		private void Initialise(int rows, int columns, float cellWidth, float cellHeight, string defaultValue)
		{
			_cellWidth = cellWidth;
			_cellHeight = cellHeight;
			
			if(defaultValue == null)
			{
				defaultValue = "";	
			}
			
			_cellText = new string[rows, columns];
			_oldCellText = new string[rows, columns];
			_locked = new bool[rows, columns];
			
			for(int i = 0; i < rows; ++i)
			{
				for(int j = 0; j < columns; ++j)
				{
					_cellText[i, j] = _oldCellText[i, j] = defaultValue;
					_locked[i, j] = false;
				}
			}
			
			Width = (float)rows * _cellWidth;
			Height = (float)columns * _cellHeight;;
			
			_style = new UnityEngine.GUIStyle();
			_style.fontSize = 16;
			_style.fontStyle = UnityEngine.FontStyle.Normal;
			_style.alignment = UnityEngine.TextAnchor.MiddleCenter;
			_style.wordWrap = true;
			_style.normal.textColor = new UnityEngine.Color(R, G, B, A);
		}
	}
}

