using System;
using System.IO;
using System.Collections.Generic;

namespace SGGUI
{
	public class GUIInputSuspendHelper
	{
		private GUIEntity _entity = null;
		private bool _enabled = false;
		private GUIInputSuspendHelper[] _childInputSuspend = null;
			
		public GUIInputSuspendHelper(GUIEntity entity)
		{
			_entity = entity;
			if(_entity != null)
			{
				_enabled = _entity.Enabled;
				if(_entity.ChildCount > 0)
				{
					_childInputSuspend = new GUIInputSuspendHelper[_entity.ChildCount];
					for(int i = 0; i < _entity.ChildCount; ++ i)
					{
						_childInputSuspend[i] = new GUIInputSuspendHelper(_entity.GetChildByIndex(i));
					}
				}
				
				_entity.Enabled = false;
			}
		}
			
		public void ApplyOrigionalState()
		{
			if(_entity != null)
			{
			
				if(!_entity.Enabled)
				{
					_entity.Enabled = _enabled;
				}
				
				if(_childInputSuspend != null)
				{
					for(int i = 0; i < _childInputSuspend.Length; ++i)
					{
						if(_childInputSuspend[i] != null)
						{
							_childInputSuspend[i].ApplyOrigionalState();
						}
					}
					_childInputSuspend = null;
				}
			}
		}
	}
}

