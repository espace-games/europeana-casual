using System;
using System.IO;
using System.Collections.Generic;

using UnityEngine;

namespace SGGUI
{
	public class GUIEntity
	{
		public enum eDock
		{
			MiddleMiddle = 0,
			TopLeft,
			TopMiddle,
			TopRight,
			MiddleRight,
			BottomRight,
			BottomMiddle,
			BottomLeft,
			MiddleLeft			
		}
		
		public static bool SafeEnable(GUIEntity entity, bool enable)
		{
			if(entity != null)
			{
				entity.Enabled = enable;
				return true;
			}
			return false;
		}
		
		public static bool SafeSetVisible(GUIEntity entity, bool visible)
		{
			if(entity != null)
			{
				entity.Visible = visible;
				return true;
			}
			return false;
		}
		
		public static Texture2D LoadImage(string imageFilename)
		{
			return (imageFilename != null) ? Resources.Load(Path.GetFileNameWithoutExtension(imageFilename)) as Texture2D : null;
		}
		
		private static float TOUCH_SIZE = 1.0f;
		
		protected List<GUIEntity> _children = null;
		protected float _x = 0.0f;
		protected float _y = 0.0f;
		protected float _rotation = 0.0f;
		protected float _width = 0.0f;
		protected float _height = 0.0f;
		protected float _pivotX = 0.0f;
		protected float _pivotY = 0.0f;
		protected float _scaleX = 1.0f;
		protected float _scaleY = 1.0f;
		protected float _r = 1.0f;
		protected float _g = 1.0f;
		protected float _b = 1.0f;
		protected float _a = 1.0f;
		protected bool _useParentAlpha = true;
		protected Matrix4x4 _lastMatrix = Matrix4x4.identity;
		
		protected bool _enabled = true;
		protected bool _visible = true;
		
		protected bool _clipChildren = false;
		
		protected Vector2 _scrollPosition = Vector2.zero;
		
		
		protected GUIEntity _parent = null;
		
		private static Texture2D BLANK_PIXEL = null;

		private eDock _docking = eDock.MiddleMiddle;

		private float _extraAlphaChannel = -1.0f;

		private bool _ignoreAutoScaling = false;

		public bool IgnoreAutoScaling
		{
			get { return _ignoreAutoScaling; }
			set { _ignoreAutoScaling = value; }
		}

		protected float ExtraAlphaChannel
		{
			get { return _extraAlphaChannel; }
			set { _extraAlphaChannel = value; }
		}
		
		public float X
		{
			get { return _x;}
			set { _x = value;}
		}
		
		public float Y
		{
			get { return _y; }
			set { _y = value; }
		}
		
		public float Rotation
		{
			get { return _rotation; }
			set { _rotation = value; }
		}
		
		public float Width
		{
			get { return _width;}
			set { _width = value; }
		}
		
		public float Height
		{
			get { return _height;}
			set { _height = value; }
		}
		
		public float PivotX
		{
			get { return _pivotX;}
			set { _pivotX = value;}
		}
		
		public float PivotY
		{
			get { return _pivotY;}
			set { _pivotY = value;}
		}
		
		public float ScaleX
		{
			get { return _scaleX;}
			set { _scaleX = value;}
		}
		
		public float ScaleY
		{
			get { return _scaleY;}
			set { _scaleY = value;}
		}
		
		public virtual float R
		{
			get { return _r;}
			set { _r = value;}
		}
		
		public virtual float G
		{
			get { return _g;}
			set { _g = value;}
		}
		
		public virtual float B
		{
			get { return _b;}
			set { _b = value;}
		}
		
		public virtual float A
		{
			get { return _a;}
			set { _a = value;}
		}
		
		public Color Colour
		{
			get { return new Color(_r, _g, _b, _a); }
			set { _r = value.r; _g = value.g; _b = value.b; _a = value.a; }
		}
		
		public bool UseParentAlpha
		{
			get { return _useParentAlpha;}
			set { _useParentAlpha = value;}
		}
		
		public bool Enabled
		{
			get { return _enabled; }
			set { _enabled = value; }
		}
		
		public bool Visible
		{
			get { return _visible; }
			set { _visible = value; VisibilityChanged(_visible); }
		}
		
		public int ChildCount
		{
			get { return (_children != null ? _children.Count : 0); }	
		}
		
		public GUIEntity Parent
		{
			get { return _parent; }	
		}
		
		public eDock Docking
		{
			get { return _docking; }
			set { _docking = value; }
		}
		
		
		public GUIEntity ()
		{
			
		}
		
		public GUIEntity (float x, float y)
		{
			_x = x;
			_y = y;
		}
		
		
		public void Render(UnityEngine.Matrix4x4 unscaledGUIMatrix)
		{
			if(Visible && ScaleX != 0.0f && ScaleY != 0.0f)
			{
				Matrix4x4 matrixCurrent = GUI.matrix;
				
				
				Vector2 translation = new Vector2(0.0f, 0.0f);
				
				float scale = IgnoreAutoScaling ? 1.0f : GUIScreen.GUIScale();
				
				float halfScreenWidth = ((float)Screen.width) * 0.5f * scale;
				float halfScreenHeight = ((float)Screen.height) * 0.5f * scale;
				
				switch(_docking)
				{
				case eDock.MiddleMiddle:
				default:
					translation.x = _x;
					translation.y = _y;
					break;
				case eDock.TopLeft:
					translation.x = -halfScreenWidth + _x;
					translation.y = -halfScreenHeight + _y;
					break;
				case eDock.TopMiddle:
					translation.x = _x;
					translation.y = -halfScreenHeight + _y;
					break;
				case eDock.TopRight:
					translation.x = halfScreenWidth + _x;
					translation.y = -halfScreenHeight + _y;
					break;
				case eDock.MiddleRight:
					translation.x = halfScreenWidth + _x;
					translation.y = _y;
					break;
				case eDock.BottomRight:
					translation.x = halfScreenWidth + _x;
					translation.y = halfScreenHeight + _y;
					break;
				case eDock.BottomMiddle:
					translation.x = _x;
					translation.y = halfScreenHeight + _y;
					break;
				case eDock.BottomLeft:
					translation.x = -halfScreenWidth + _x;
					translation.y = halfScreenHeight + _y;
					break;
				case eDock.MiddleLeft:
					translation.x = -halfScreenWidth + _x;
					translation.y = _y;
					break;		
				}
				

				if(IgnoreAutoScaling)
				{
					translation.x += halfScreenWidth;
					translation.y += halfScreenHeight;
				}



				GUI.matrix = (IgnoreAutoScaling ? unscaledGUIMatrix : matrixCurrent) * Matrix4x4.TRS(translation, Quaternion.Euler(new Vector3(0.0f, 0.0f, _rotation)), new Vector3(ScaleX, ScaleY, 1.0f));
		
				_lastMatrix = GUI.matrix;

			
				Color colourCurrent = GUI.color;
				//DrawBoundingBox(this);


				Color normalColour = new Color(R, G, B, (UseParentAlpha ? A * colourCurrent.a : A));

				if(ExtraAlphaChannel >= 0.0f)
				{
					GUI.color = new Color(R, G, B, (UseParentAlpha ? A * ExtraAlphaChannel * colourCurrent.a : (A * ExtraAlphaChannel)));
				}
				else
				{
					GUI.color = normalColour;
				}




				Draw(colourCurrent.a);

				if(ExtraAlphaChannel >= 0.0f)
				{
					GUI.color = normalColour;
				}
				
				if(_children != null)
				{
					foreach(GUIEntity child in _children)
					{
						child.Render(unscaledGUIMatrix);	
					}
				}
			
				GUI.matrix = matrixCurrent;
				GUI.color = colourCurrent;
			}
		}

		private static void DrawBoundingBox(GUIEntity entity)
		{
			if(entity != null)
			{
				GUI.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
				DrawBox(-entity.PivotX, -entity.PivotY, entity.Width, entity.Height);

				GUI.color = Color.green;
				float pointSize = 10.0f;

				DrawBox(-pointSize * 0.5f, -pointSize * 0.5f, pointSize, pointSize);
			}
		}
		
		public void Update(float deltaTime)
		{
			Process(deltaTime);
			if(_children != null)
			{
				foreach(GUIEntity child in _children)
				{
					child.Update(deltaTime);	
				}
			}
		}
		
		protected virtual void Draw(float parentAlpha)
		{
			
		}
		
		protected virtual void Process(float deltaTime)
		{
			
		}
		
		protected virtual void VisibilityChanged(bool isVisible)
		{
			
		}
		
		public GUIEntity AddChild(GUIEntity child)
		{
			if(child != null)
			{
				if(_children == null)
				{
					_children = new List<GUIEntity>();
				}
				_children.Add(child);
				child._parent = this;
			}
			return child;
		}
		
		public bool RemoveChild(GUIEntity child)
		{
			if(child != null && _children != null && _children.Count > 0)
			{
				return _children.Remove(child);
			}
			return false;
		}
		
		
		
		protected bool DrawTexture(Texture texture)
		{
			if(texture != null)
			{
				GUI.DrawTexture(new Rect(-PivotX, -PivotY, Width, Height), texture);
				return true;
			}
			return false;
		}
		
		protected static Texture2D BlankPixel()
		{
			if(BLANK_PIXEL == null)
			{
				BLANK_PIXEL = new Texture2D(1, 1);
				BLANK_PIXEL.SetPixel (0, 0, Color.white);
				BLANK_PIXEL.Apply();
			}
			
			return BLANK_PIXEL;
		}
		
		public static float AngleBetween(Vector2 a, Vector2 b)
		{
			float angle = Mathf.Atan2(b.y, b.x) - Mathf.Atan2(a.y, a.x);
			
			return Mathf.Rad2Deg * angle;
		}
		
		public static void DrawBox(float left, float top, float width, float height)
		{
			GUI.DrawTexture(new Rect(left, top, width, height), BlankPixel());
		}
		
		public static void DrawLine(Vector2 start, Vector2 end, float thickness)
		{
			if(thickness > 0.0f)
			{
				Vector2 direction = end - start;
				
				float length = direction.magnitude;
				if(length > 0.0f)
				{
					direction /= length;
					
					Matrix4x4 matrixCurrent = GUI.matrix;
					
					GUI.matrix = matrixCurrent * Matrix4x4.TRS(start, Quaternion.Euler(new Vector3(0.0f, 0.0f, AngleBetween(Vector2.right, direction))), Vector3.one);
					
					GUI.DrawTexture(new Rect(0.0f, -thickness* 0.5f, length, thickness), BlankPixel());
					
					GUI.matrix = matrixCurrent;
				}
				else
				{
					GUI.DrawTexture(new Rect(-thickness* 0.5f, -thickness* 0.5f, thickness, thickness), BlankPixel());
				}
			}
		}
		
		public bool Autosize(Texture texture)
		{
			if(texture != null)
			{
				Width = texture.width;
				Height = texture.height;
				PivotX = Width * 0.5f;
				PivotY = Height * 0.5f;
				return true;
			}
			return false;
		}

		public bool Autosize(float width, float height)
		{
			Width = width;
			Height = height;
			PivotX = Width * 0.5f;
			PivotY = Height * 0.5f;
			return true;
		}
		
		public bool TouchDown(float x, float y)
		{
			if(Visible)
			{
				//Ensure top most item gets priority!
				if(_children != null)
				{
					for(int i = _children.Count -1; i >= 0; --i)
					{
						if(_children[i].TouchDown(x, y))
						{
							return true;	
						}
					}
				}
			
				if(this is GUITouchInput && Enabled)
				{
					return ((GUITouchInput)this).OnTouchDown(x, y);
				}
			}
			return false;
		}
		
		public bool TouchMove(float x, float y)
		{
			if(Visible)
			{
				if(this is GUITouchInput && Enabled)
				{
					((GUITouchInput)this).OnTouchMove(x, y);	
				}
				if(_children != null)
				{
					foreach(GUIEntity child in _children)
					{
						child.TouchMove(x, y);
					}
				}
			}
			return false;
		}
		
		public bool TouchUp(float x, float y)
		{
			if(Visible)
			{
				if(this is GUITouchInput && Enabled)
				{
					((GUITouchInput)this).OnTouchUp(x, y);	
				}
				if(_children != null)
				{
					foreach(GUIEntity child in _children)
					{
						child.TouchUp(x, y);
					}
				}
			}
			return false;
		}
		
		public virtual bool CollisionTest(float x, float y)
		{
			return PointBoxIntersectionTest(new Vector2(_x, _y));	
		}
		
		protected bool PointBoxIntersectionTest(Vector2 point)
		{
			if(Width > 0.0f && Height > 0.0f)
			{
				return !( ((point.x + TOUCH_SIZE) < -PivotX) || (point.x > (-PivotX + Width)) || ((point.y + TOUCH_SIZE) < -PivotY) || (point.y > (-PivotY + Height)) );
			}
			return false;
		}
		
		protected bool PixelPerfectIntersectionTest(Vector2 point, Texture2D texture)
		{
			if(texture != null)
			{
				int texelX = (int)(point.x + 0.5f);
				int texelY = (int)(point.y + 0.5f);
				
				if(texelX >= 0 && texelX < texture.width && texelY >= 0 && texelY < texture.height)
				{
					return (texture.GetPixel(texelX, texture.height - texelY).a > 0.0f);
				}
			}
			return false;
		}
		
		public GUIEntity GetChildByIndex(int index)
		{
			return ((_children != null && index >= 0 && index < _children.Count) ? _children[index] : null);
		}
	}
}

