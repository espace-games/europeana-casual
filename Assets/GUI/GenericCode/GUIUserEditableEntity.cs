using System;

namespace SGGUI
{	
	public abstract class GUIUserEditableEntity : GUIEntity
	{
		protected bool _dataChangedFlag = false;
		
		public static bool DataChanged(GUIUserEditableEntity entity, bool clearFlag)
		{
			return (entity != null ? entity.HasDataChanged(clearFlag) : false);
		}
		
		
		public bool HasDataChanged(bool clearFlag)
		{
			bool dataChanged = _dataChangedFlag;
			if(clearFlag)
			{
				_dataChangedFlag = false;
			}
			return dataChanged;
		}
		
		
		public GUIUserEditableEntity ()
		{
			
		}
		
		public GUIUserEditableEntity (float x, float y)
			: base(x, y)
		{
			
		}
		
	}
}

