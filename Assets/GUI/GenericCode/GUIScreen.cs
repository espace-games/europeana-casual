using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

namespace SGGUI
{
	public class GUIScreen
	{
		public static float REF_WIDTH = 1280.0f;
		public static float REF_HEIGHT = 800.0f;
		public static float REF_HALFWIDTH = REF_WIDTH * 0.5f;
		public static float REF_HALFHEIGHT = REF_HEIGHT * 0.5f;
		public static float REF_ASPECTRATIO = REF_WIDTH/REF_HEIGHT;
		
		private GUIEntity _root =  null;
		
		private UnityEngine.Texture2D _borderTexture = null;
		
		public static float GUIScale()
		{
			return (((float)Screen.width / (float)Screen.height) <= GUIScreen.REF_ASPECTRATIO) ? GUIScreen.REF_WIDTH / (float)Screen.width : GUIScreen.REF_HEIGHT / (float)Screen.height;
		}
		
		public GUIEntity Root
		{
			get { return _root; }	
		}
		
		
		public GUIScreen()
		{
			_root = new GUIEntity();
			
			_borderTexture = GUIEntity.LoadImage("fade.jpg");
		}
		
		public GUIEntity AddChild(GUIEntity child)
		{
			if(child != null && _root != null)
			{
				_root.AddChild(child);
				return child;
			}
			return null;
		}

		public bool RemoveChild(GUIEntity child)
		{
			if(child != null && _root != null)
			{
				return _root.RemoveChild(child);
			}
			return false;
		}
		
		
		public void Render(float renderWidth, float renderHeight, float screenWidth, float screenHeight)
		{	
			if(_root != null)
			{
				UnityEngine.Matrix4x4 matrixCurrent = UnityEngine.GUI.matrix;
				
				float scaleX = renderWidth / REF_WIDTH;
				float scaleY = renderHeight / REF_HEIGHT;
				
				UnityEngine.GUI.matrix =  UnityEngine.Matrix4x4.TRS(new UnityEngine.Vector2(screenWidth * 0.5f, screenHeight * 0.5f), UnityEngine.Quaternion.Euler(new UnityEngine.Vector3(0.0f, 0.0f, 0.0f)), new UnityEngine.Vector3(scaleX, scaleY, 1.0f));
				
				_root.Render(matrixCurrent);
				
				UnityEngine.GUI.matrix = matrixCurrent;
			}
			
			/*UnityEngine.GUI.color = UnityEngine.Color.black;
			if(renderHeight < screenHeight)
			{
				float height = (screenHeight - renderHeight) * 0.5f;
				UnityEngine.GUI.DrawTexture(new UnityEngine.Rect(0, 0, screenWidth, height), _borderTexture);
				UnityEngine.GUI.DrawTexture(new UnityEngine.Rect(0, screenHeight - height, screenWidth, height), _borderTexture);
			}
			else if(renderWidth < screenWidth)
			{
				float width = (screenWidth - renderWidth) * 0.5f;
				UnityEngine.GUI.DrawTexture(new UnityEngine.Rect(0, 0, width, screenHeight), _borderTexture);
				UnityEngine.GUI.DrawTexture(new UnityEngine.Rect(screenWidth - width, 0, width, screenHeight), _borderTexture);
			}*/
		}
		
		public void Update(float deltaTime)
		{
			if(_root != null)
			{
				_root.Update(deltaTime);	
			}
		}
		
		public bool TouchDown(float x, float y)
		{
			return (_root != null ? _root.TouchDown(x, y) : false);
		}
		
		public bool TouchMove(float x, float y)
		{
			return (_root != null ? _root.TouchMove(x, y) : false);
		}
		
		public bool TouchUp(float x, float y)
		{
			return (_root != null ? _root.TouchUp(x, y) : false);
		}
	}
}

