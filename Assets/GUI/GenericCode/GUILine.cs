using System;

using UnityEngine;

namespace SGGUI
{
	public class GUILine : GUIEntity
	{
		protected float _thickness = 4.0f;
		
		protected UnityEngine.Vector2[] _points = null;
		
		public float Thickness
		{
			get { return _thickness; }
			set { _thickness = value; }
		}
		
		public Vector2[] Points
		{
			get { return _points; }
			set { _points = value; }
		}
		
		public GUILine()
		{
			Initialise();
		}
		
		public GUILine (float x, float y)
			: base(x, y)
		{
			Initialise();
		}
	
		
		protected override void Draw(float parentAlpha)
		{
			if(Thickness > 0.0f && _points != null && _points.Length > 1)
			{
				
				
				//Debuging
				/*
				GUI.color = new Color(1.0f, 1.0f, 1.0f, 0.5f);
				float boxSize = 150.0f;
				float boxRadius = boxSize * 0.5f;
				GUI.DrawTexture(new Rect(-boxRadius, -boxRadius, boxSize, boxSize), BlankPixel());
				
				GUI.color = Color.yellow;
				
				GUIEntity.DrawLine(new Vector2(-boxRadius, boxRadius), new Vector2(boxRadius, boxRadius), Thickness);
				GUIEntity.DrawLine(new Vector2(-boxRadius, -boxRadius), new Vector2(boxRadius, -boxRadius), Thickness);
				GUIEntity.DrawLine(new Vector2(-boxRadius, -boxRadius), new Vector2(-boxRadius, boxRadius), Thickness);
				GUIEntity.DrawLine(new Vector2(boxRadius, -boxRadius), new Vector2(boxRadius, boxRadius), Thickness);
				
				
				GUI.color = Color.red;
				*/
				
				for( int i = 1; i < _points.Length; ++i)
				{
					GUIEntity.DrawLine(_points[i - 1], _points[i], Thickness);
				}
			}
		}
		
		
		
		private void Initialise()
		{
			
			

		}
	}
}

