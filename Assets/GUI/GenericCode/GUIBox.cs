﻿using UnityEngine;
using System.Collections;
using System;

namespace SGGUI
{
	public class GUIBox : GUIEntity
	{
		protected float _borderSize = 0.0f;
		protected Color _borderColour = new Color(1.0f, 1.0f, 1.0f, 1.0f);

		public float BorderSize
		{
			get { return _borderSize; }
			set { _borderSize = value; }
		}

		public Color BorderColour
		{
			get { return _borderColour; }
			set { _borderColour = value; }
		}

		public GUIBox (float width, float height)
		{
			Initialise(width, height, Color.white);
		}
		
		public GUIBox (float x, float y, float width, float height)
			: base(x, y)
		{
			Initialise(width, height, Color.white);
		}

		public GUIBox (float width, float height, Color colour)
		{
			Initialise(width, height, colour);
		}
		
		public GUIBox (float x, float y, float width, float height, Color colour)
			: base(x, y)
		{
			Initialise(width, height, colour);
		}
		
		protected override void Draw(float parentAlpha)
		{
			DrawBox(-PivotX, -PivotY, Width, Height);

			if(_borderSize > 0.0f)
			{
				Color currentGUIColor = GUI.color;

				GUI.color = new Color(_borderColour.r, _borderColour.g, _borderColour.b, UseParentAlpha ? (_borderColour.a * this.A * parentAlpha) : _borderColour.a * this.A);
				//Top
				DrawBox(-PivotX, -PivotY, Width, _borderSize);
				//Bottom
				DrawBox(-PivotX, -PivotY + Height - _borderSize, Width, _borderSize);
				//Left
				DrawBox(-PivotX, -PivotY + _borderSize, _borderSize, _height - (_borderSize * 2.0f));
				//Left
				DrawBox(-PivotX + Width - _borderSize, -PivotY + _borderSize, _borderSize, _height - (_borderSize * 2.0f));

				GUI.color = currentGUIColor;
			}
		}
		
		private void Initialise(float width, float height, Color colour)
		{
			this.Colour = new Color(colour.r, colour.g, colour.b);
			ExtraAlphaChannel = colour.a;
			Autosize(width, height);
		}
	}
}
