using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class GUISlideEffect: GUIEffect
	{
		float _aX = 0.0f;
		float _bX = 0.0f;
		float _aY = 0.0f;
		float _bY = 0.0f;
		
		public float AX
		{
			get { return _aX; }
			set { _aX = value; }
		}
		
		public float BX
		{
			get { return _bX; }
			set { _bX = value; }
		}
		
		public float AY
		{
			get { return _aY; }
			set { _aY = value; }
		}
		
		public float BY
		{
			get { return _bY; }
			set { _bY = value; }
		}
		
		public GUISlideEffect (GUIEntity target, float duration, eAnim anim, float aX, float bX, float aY, float bY)
			:base(target, duration, anim)
		{
			_aX = aX;
			_bX = bX;
			_aY = aY;
			_bY = bY;
		}
		
		protected override void ApplyEffect(GUIEntity target, eAnim anim, float t)
		{
			if(target != null)
			{
				target.X = GUIEffect.Animate(anim, _aX, _bX, t);
				target.Y = GUIEffect.Animate(anim, _aY, _bY, t);
			}
		}
	}
}
