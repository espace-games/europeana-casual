using UnityEngine;
using System.Collections.Generic;

namespace SGGUI
{
	public delegate void EffectComplete(bool forward);
	
	public abstract class GUIEffect
	{
		public static void SafeUpdate(GUIEffect effect, float deltaTime)
		{
			if(effect != null)
			{
				effect.Update(deltaTime);	
			}
		}
		
		public enum eAnim
		{
			Lerp = 0,
			SmoothStep,
			Square,
			InverseSquare,
			SinWave,
			CosWave,
		}
		
		private List<GUIEffect> _subEffects = null;
		
		public static float Animate(eAnim anim, float a, float b, float t)
		{
			t = (t < 0.0f ? 0.0f : (t > 1.0f ? 1.0f : t));
			
			int squareInterations = 10;
			float speed = 10.0f;
			
			switch(anim)
			{
			case eAnim.Lerp:
				return  a + (b - a) * t;
			case eAnim.SmoothStep:
				return a + (b - a) * (t * t * (3.0f - 2.0f * t));
			case eAnim.Square:
				return a + (b - a) * (Mathf.Pow(t, squareInterations));
			case eAnim.InverseSquare:
			{
				float lerp = t;
				for(int i = 0; i < squareInterations; ++i)
				{
 					lerp = 1.0f - (1.0f - lerp) * (1.0f - lerp);
				}
				return a + (b - a) * lerp;

 			}
			case eAnim.SinWave:
				return a + (b - a) * Mathf.Sin(t * speed);
			case eAnim.CosWave:
				return a + (b - a) * Mathf.Cos(t * speed);
			default:
				break;
			}
			
			return a;
		}
		
		private GUIEntity _target = null;
		
		private float _duration = 1.0f;
		private float _timeCurrent = 0.0f;
		private eAnim _anim;
		private bool _playForward = true;
		private bool _isPlaying = false;
		
		private EffectComplete _delegateEffectComplete = null;
		
		public bool IsPlayingForward
		{
			get { return _playForward; }	
		}
		
		public bool IsPlaying
		{
			get { return _isPlaying; }	
		}
		
		public EffectComplete EffectCompleteDelegate
		{
			set { _delegateEffectComplete = value; }	
		}
		
		public GUIEffect (GUIEntity target, float duration, eAnim anim)
		{
			_target = target;
			_duration = duration;
			_anim = anim;
		}
		
		public void Play(bool forward, bool leaveTime)
		{
			_playForward = forward;
			if(!leaveTime)
			{
				_timeCurrent = (_playForward ? 0.0f : _duration);	
			}
			_isPlaying = true;
			
			if(_subEffects != null)
			{
				foreach(GUIEffect subEffect in _subEffects)
				{
					subEffect.Play(forward, leaveTime);
				}
			}
		}
		
		public void Stop()
		{
			_isPlaying = false;
			
			if(_subEffects != null)
			{
				foreach(GUIEffect subEffect in _subEffects)
				{
					subEffect.Stop();
				}
			}
		}
		
		public void SetToStart()
		{
			_timeCurrent = 0.0f;
			ApplyEffect(_target, _anim, 0.0f);
			
			if(_subEffects != null)
			{
				foreach(GUIEffect subEffect in _subEffects)
				{
					subEffect.SetToStart();
				}
			}
		}
		
		public void SetToEnd()
		{
			_timeCurrent = _duration;
			ApplyEffect(_target, _anim, 1.0f);
			
			if(_subEffects != null)
			{
				foreach(GUIEffect subEffect in _subEffects)
				{
					subEffect.SetToEnd();
				}
			}
		}
		
		public void Update(float deltaTime)
		{
			if(_isPlaying)
			{
				_timeCurrent = Mathf.Clamp(_timeCurrent + (_playForward ? deltaTime : -deltaTime), 0.0f, _duration);
			
				ApplyEffect(_target, _anim, _duration > 0.0f ? (_timeCurrent/_duration) : 0.0f);
				
				if(_subEffects != null)
				{
					foreach(GUIEffect subEffect in _subEffects)
					{
						subEffect.Update(deltaTime);
					}
				}
			
				_isPlaying = !(_playForward ? (_timeCurrent >= _duration) : (_timeCurrent <= 0.0f));
				if(!IsPlaying && _delegateEffectComplete != null)
				{
					_delegateEffectComplete(_playForward);	
				}
			}
		}
		
		protected abstract void ApplyEffect(GUIEntity target, eAnim anim, float t);
		
		public void AddSubEffect(GUIEffect subEffect)
		{
			if(subEffect != null)
			{
				if(_subEffects == null)
				{
					_subEffects = new List<GUIEffect>();
				}
				
				if(subEffect._duration > _duration)
				{
					subEffect._duration = _duration;
				}
				subEffect._timeCurrent = _timeCurrent;
				subEffect._isPlaying = _isPlaying;
				
				_subEffects.Add(subEffect);
			}
		}
	}
}

