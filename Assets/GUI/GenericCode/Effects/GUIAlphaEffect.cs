using UnityEngine;
using System.Collections;

namespace SGGUI
{
	public class GUIAlphaEffect: GUIEffect
	{
		float _a = 0.0f;
		float _b = 0.0f;
		
		public GUIAlphaEffect (GUIEntity target, float duration, eAnim anim, float a, float b)
			:base(target, duration, anim)
		{
			_a = a;
			_b = b;
		}
		
		protected override void ApplyEffect(GUIEntity target, eAnim anim, float t)
		{
			if(target != null)
			{
				target.A = GUIEffect.Animate(anim, _a, _b, t);
			}
		}
	}
}
