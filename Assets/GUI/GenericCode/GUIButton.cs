using System;

namespace SGGUI
{
	public class GUIButton : GUIEntity, GUITouchInput
	{
		protected UnityEngine.Texture _textureNormal = null;
		protected UnityEngine.Texture _textureTouched = null;
		protected UnityEngine.Texture _textureDisabled = null;
		protected bool _isTouched = false;
		protected bool _wasTouched = false;
		protected bool _pixelPerfectCollision = false;
		protected OnPress _delegateOnPress = null;
		protected OnFirstTouch _delegateOnFirstTouch = null;
		protected bool _toggleButton = false;
		protected bool _isToggled = false;
		
		public bool ToggleButton
		{
			get { return _toggleButton; }
			set { _toggleButton = value; }
		}
		
		public bool Toggled
		{
			get { return (_toggleButton && _isToggled); }
			set { _isToggled = (_toggleButton ? value : false); }
		}
		
		public bool PixelPerfectCollision
		{
			get { return _pixelPerfectCollision; }
			set { _pixelPerfectCollision = value; }
		}
		
		public UnityEngine.Texture TextureNormal
		{
			set { _textureNormal = value; }
		}
		
		public UnityEngine.Texture TextureTouched
		{
			set { _textureTouched = value; }
		}
		
		public UnityEngine.Texture TextureDisabled
		{
			set { _textureDisabled = value; }
		}
		
		public OnPress OnPressDelegate
		{
			set { _delegateOnPress = value; }	
		}
		
		public OnFirstTouch OnFirstTouchDelegate
		{
			set { _delegateOnFirstTouch = value; }	
		}
		
		public GUIButton (string imageNormalFilename, string imageTouchedFilename, string imageDisabledFilename)
		{
			Initialise(GUIEntity.LoadImage(imageNormalFilename), GUIEntity.LoadImage(imageTouchedFilename), GUIEntity.LoadImage(imageDisabledFilename));
		}
		
		public GUIButton (float x, float y, string imageNormalFilename, string imageTouchedFilename, string imageDisabledFilename)
			: base(x, y)
		{
			Initialise(GUIEntity.LoadImage(imageNormalFilename), GUIEntity.LoadImage(imageTouchedFilename), GUIEntity.LoadImage(imageDisabledFilename));
		}
		
		public GUIButton (UnityEngine.Texture textureNormal, UnityEngine.Texture textureTouched, UnityEngine.Texture textureDisabled)
		{
			Initialise(textureNormal, textureTouched, textureDisabled);
		}
		
		public GUIButton (float x, float y, UnityEngine.Texture textureNormal, UnityEngine.Texture textureTouched, UnityEngine.Texture textureDisabled)
			: base(x, y)
		{
			Initialise(textureNormal, textureTouched, textureDisabled);
		}
		
		private void Initialise(UnityEngine.Texture textureNormal, UnityEngine.Texture textureTouched, UnityEngine.Texture textureDisabled)
		{
			_textureNormal = textureNormal;
			_textureTouched = textureTouched;
			
			_textureDisabled = textureDisabled;
			
			if(!Autosize(_textureNormal))
			{
				Autosize(_textureTouched);	
			}
		}
		
		protected override void Draw(float parentAlpha)
		{
			DrawTexture(Enabled ?  ((Toggled ? !_isTouched : _isTouched) ? _textureTouched : _textureNormal) : _textureDisabled);
		}
		
		public bool OnTouchDown(float x, float y)
		{
			_wasTouched = _isTouched = CollisionTest(x, y);
			
			if(_isTouched && _delegateOnFirstTouch != null)
			{
				_delegateOnFirstTouch(this, x, y);	
			}

			return _isTouched;
		}
		
		public bool OnTouchMove(float x, float y)
		{
			if(_wasTouched)
			{
				_isTouched = CollisionTest(x, y);
			}
			return _isTouched;
		}
		
		public bool OnTouchUp(float x, float y)
		{
			_wasTouched = false;
			if(_isTouched)
			{
				_isTouched = false;
				
				if(_toggleButton)
				{
					_isToggled = !_isToggled;	
				}
				if(_delegateOnPress != null)
				{
					_delegateOnPress(this);	
				}
				return true;
			}
			return false;
		}
		
		public override bool CollisionTest(float x, float y)
		{
			UnityEngine.Vector2 point = _lastMatrix.inverse.MultiplyPoint3x4(new UnityEngine.Vector3(x, y, 0.0f));
			bool boxCollision = PointBoxIntersectionTest(point);
			
			if(_textureNormal != null && (_textureNormal is UnityEngine.Texture2D) && boxCollision && PixelPerfectCollision)
			{
				//UnityEngine.Debug.Log("point: " + (point.x + _pivotX) + ", " + (point.y + _pivotY));
				return PixelPerfectIntersectionTest(new UnityEngine.Vector2(point.x + _pivotX, point.y + _pivotY), (UnityEngine.Texture2D)_textureNormal);
			}
			
			return boxCollision;
		}
	}
}

