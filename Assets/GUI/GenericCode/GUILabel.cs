using System;

namespace SGGUI
{
	public class GUILabel : GUIEntity
	{
		private static float DROP_SHADOW_DISTANCE = 5.0f;
		private static float DROP_SHADOW_ALPHA = 0.5f;

		private string _text = null;
		private UnityEngine.GUIStyle _style = null;
		private float _stroke = 0.0f;
		private float _strokeR = 0.0f;
		private float _strokeG = 0.0f;
		private float _strokeB = 0.0f;
		private float _strokeA = 0.0f;
		private bool _dropShadow = false;

		public void Autosize()
		{
			UnityEngine.Vector2 size = this.Style.CalcSize(new UnityEngine.GUIContent(this.Text));
			Autosize(size.x, size.y);
		}


		public static void SafeChangeFont(GUILabel label, UnityEngine.Font font)
		{
			if(label != null && font != null)
			{
				label.Style.font = font;
			}
		}

		public static void SafeEnableDropShadow(GUILabel label, bool enable)
		{
			if(label != null)
			{
				label.DropShadow = enable;
			}
		}

		public static void SafeSetText(GUILabel label, string text)
		{
			if(label != null)
			{
				label.Text = text;
			}
		}
		
		public UnityEngine.GUIStyle Style
		{
			get { return _style;}
			set { _style = value;}
		}
		
		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}
		
		public float Stroke
		{
			get { return _stroke; }
			set { _stroke = value; }
		}
		
		public UnityEngine.Color StrokeColour
		{
			get { return new UnityEngine.Color(_strokeR, _strokeG, _strokeB, _strokeA); }
			set { _strokeR = value.r; _strokeG = value.g; _strokeB = value.b; _strokeA = value.a; }
		}

		public bool DropShadow
		{
			get { return _dropShadow; }
			set { _dropShadow = value; }
		}
		
		public GUILabel (string text)
		{
			Initialise(text);
		}
		
		public GUILabel (float x, float y, string text)
			: base(x, y)
		{
			Initialise(text);
		}
		
		protected override void Draw(float parentAlpha)
		{
			if(_text != null)
			{
				UnityEngine.Color currentColour = UnityEngine.GUI.color;
				if(_style != null)
				{

					if(DropShadow)
					{
						_style.normal.textColor = new UnityEngine.Color(0.0f, 0.0f, 0.0f, DROP_SHADOW_ALPHA * 0.5f);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + (DROP_SHADOW_DISTANCE * 0.5f), -PivotY + (DROP_SHADOW_DISTANCE * 0.5f), Width, Height), _text, _style);
						_style.normal.textColor = new UnityEngine.Color(0.0f, 0.0f, 0.0f, DROP_SHADOW_ALPHA);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + DROP_SHADOW_DISTANCE, -PivotY + DROP_SHADOW_DISTANCE, Width, Height), _text, _style);
					}

					if(_stroke > 0.0f && _strokeA > 0.0f)
					{
						
						_style.normal.textColor = UnityEngine.GUI.color = new UnityEngine.Color(_strokeR, _strokeG, _strokeB, currentColour.a * _strokeA);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX - _stroke, -PivotY - _stroke, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX, -PivotY - _stroke, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + _stroke, -PivotY - _stroke, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + _stroke, -PivotY, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + _stroke, -PivotY + _stroke, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX, -PivotY + _stroke, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX - _stroke, -PivotY + _stroke, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX - _stroke, -PivotY, Width, Height), _text, _style);
					}
					_style.normal.textColor = UnityEngine.GUI.color = currentColour;
					
					UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX, -PivotY, Width, Height), _text, _style);
				}
				else
				{
					if(DropShadow)
					{
						UnityEngine.GUI.color = new UnityEngine.Color(0.0f, 0.0f, 0.0f, DROP_SHADOW_ALPHA * 0.5f);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + (DROP_SHADOW_DISTANCE * 0.5f), -PivotY + (DROP_SHADOW_DISTANCE * 0.5f), Width, Height), _text);
						UnityEngine.GUI.color = new UnityEngine.Color(0.0f, 0.0f, 0.0f, DROP_SHADOW_ALPHA);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + DROP_SHADOW_DISTANCE, -PivotY + DROP_SHADOW_DISTANCE, Width, Height), _text);
						UnityEngine.GUI.color = currentColour;
					}

					if(_stroke > 0.0f && _strokeA > 0.0f)
					{
						UnityEngine.GUI.color = new UnityEngine.Color(_strokeR, _strokeG, _strokeB, currentColour.a * _strokeA);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX - _stroke, -PivotY - _stroke, Width, Height), _text);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX, -PivotY - _stroke, Width, Height), _text);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + _stroke, -PivotY - _stroke, Width, Height), _text);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + _stroke, -PivotY, Width, Height), _text);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX + _stroke, -PivotY + _stroke, Width, Height), _text);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX, -PivotY + _stroke, Width, Height), _text, _style);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX - _stroke, -PivotY + _stroke, Width, Height), _text);
						UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX - _stroke, -PivotY, Width, Height), _text);
						UnityEngine.GUI.color = currentColour;
					}
					
					
					
					UnityEngine.GUI.Label(new UnityEngine.Rect(-PivotX, -PivotY, Width, Height), _text);
				}
			}
		}
		
		private void Initialise(string text)
		{
			_text = text;

			Autosize(100.0f, 100.0f);
			
			_style = new UnityEngine.GUIStyle();
			_style.fontSize = 16;
			_style.fontStyle = UnityEngine.FontStyle.Normal;
			_style.alignment = UnityEngine.TextAnchor.UpperLeft;
			_style.normal.textColor = new UnityEngine.Color(R, G, B, A);
		}
	}
}

