using System;

namespace SGGUI
{
	
	public delegate void OnPress(GUIEntity guiEntity);
	
	public delegate void OnFirstTouch(GUIEntity guiEntity, float x, float y);
	
	public interface GUITouchInput
	{
		bool OnTouchDown(float x, float y);
		
		bool OnTouchMove(float x, float y);
		
		bool OnTouchUp(float x, float y);
		
		bool PixelPerfectCollision
		{
			get;
			set;
		}
		
		/*OnPress OnPressDelegate
		{
			set;	
		}*/
	}
}

