using System;

namespace SGGUI
{
	public class GUIImage : GUIEntity
	{
		protected UnityEngine.Texture _texture = null;
		
		public UnityEngine.Texture ImgTexture
		{
			get { return _texture; }
			set { _texture = value; }
		}
		
		public GUIImage (string imageFilename)
		{
			Initialise(GUIEntity.LoadImage(imageFilename));
		}
		
		public GUIImage (float x, float y, string imageFilename)
			: base(x, y)
		{
			Initialise(GUIEntity.LoadImage(imageFilename));
		}
		
		public GUIImage (UnityEngine.Texture texture)
		{
			Initialise(texture);
		}
		
		public GUIImage (float x, float y, UnityEngine.Texture texture)
			: base(x, y)
		{
			Initialise(texture);
		}
		
		protected override void Draw(float parentAlpha)
		{
			DrawTexture(_texture);
		}
		
		private void Initialise(UnityEngine.Texture texture)
		{
			_texture = texture;
			Autosize(_texture);
		}
	}
}

