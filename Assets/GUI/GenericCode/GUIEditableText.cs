using System;

namespace SGGUI
{
	public class GUIEditableText : GUIUserEditableEntity
	{
		private string _text = null;
		private UnityEngine.GUIStyle _style = null;
		private int _characterLimit = 0;
		private bool _retainFocus = false;

		public void Autosize()
		{
			UnityEngine.Vector2 size = this.Style.CalcSize(new UnityEngine.GUIContent(this.Text));
			Autosize(size.x, size.y);
		}
		
		public UnityEngine.GUIStyle Style
		{
			get { return _style;}
			set { _style = value;}
		}
		
		public string Text
		{
			get { return _text; }
			set { _text = value; }
		}
		
		public bool RetainFocus
		{
			get { return _retainFocus; }
			set { _retainFocus = value; }
		}
		
		//Character limit seem to break new line input

		public int CharacterLimit
		{
			get { return _characterLimit; }
			set { _characterLimit = value; }
		}
		
		
		public GUIEditableText (string text)
		{
			Initialise(text);
		}
		
		public GUIEditableText (float x, float y, string text)
			: base(x, y)
		{
			Initialise(text);
		}
		
		protected override void Draw(float parentAlpha)
		{
			string oldText = _text;
			if(_text != null)
			{	
				if(_retainFocus)
				{
					UnityEngine.GUI.SetNextControlName("InputBox");
				}
				
				if(_style != null)
				{
					_style.normal.textColor = UnityEngine.GUI.color;
					if(_characterLimit > 0)
					{
						//Character limit seem to break new line input
						_text = UnityEngine.GUI.TextArea(new UnityEngine.Rect(-PivotX, -PivotY, Width, Height), _text, _characterLimit, _style);
					}
					else
					{
						_text = UnityEngine.GUI.TextArea(new UnityEngine.Rect(-PivotX, -PivotY, Width, Height), _text, _style);
					}
				}
				else
				{
					if(_characterLimit > 0)
					{
						//Character limit seem to break new line input
						_text = UnityEngine.GUI.TextArea(new UnityEngine.Rect(-PivotX, -PivotY, Width, Height), _text, _characterLimit);
					}
					else
					{
						_text = UnityEngine.GUI.TextArea(new UnityEngine.Rect(-PivotX, -PivotY, Width, Height), _text);
					}
				}
				if(_retainFocus)
				{
					UnityEngine.GUI.FocusControl("InputBox");
				}
				
				if(oldText != _text)
				{
					_dataChangedFlag = true;
				}
			}
		}
		
		private void Initialise(string text)
		{
			_text = text;
			
			Width = 100;
			Height = 100;
			
			_style = new UnityEngine.GUIStyle();
			_style.fontSize = 16;
			_style.fontStyle = UnityEngine.FontStyle.Normal;
			_style.alignment = UnityEngine.TextAnchor.UpperLeft;
			_style.wordWrap = true;
			_style.normal.textColor = new UnityEngine.Color(R, G, B, A);
		}
	}
}

